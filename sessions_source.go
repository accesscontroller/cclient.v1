package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalSessionsSourceContext loads and returns one storage.ExternalSessionsSourceV1.
func (c *CClient) GetExternalSessionsSource(sessionsSourceName storage.ResourceNameT) (
	*storage.ExternalSessionsSourceV1, error) {
	url := fmt.Sprintf(externalSessionsSourceV1Path, c.urlPrefix, sessionsSourceName)

	// Load.
	pl, err := c.getRead(url, nil)
	if err != nil {
		return nil, err
	}

	// Parse.
	var res storage.ExternalSessionsSourceV1

	if err := json.Unmarshal(pl, &res); err != nil {
		return nil, err
	}

	return &res, nil
}

// GetExternalSessionsSourceContext loads and returns filtered storage.ExternalSessionsSourceV1s.
func (c *CClient) GetExternalSessionsSources(filter []storage.ExternalSessionsSourceFilterV1) (
	[]storage.ExternalSessionsSourceV1, error) {
	url := fmt.Sprintf(externalSessionsSourceListV1Path, c.urlPrefix)

	// Load.
	pl, err := c.getRead(url, filter)
	if err != nil {
		return nil, err
	}

	// Parse.
	var res []storage.ExternalSessionsSourceV1

	if err := json.Unmarshal(pl, &res); err != nil {
		return nil, err
	}

	return res, nil
}

// checkExternalSessionsSourceV1sFound checks that for all the names there are ExternalSessionsSourceV1s.
func checkExternalSessionsSourceV1sFound(
	names []storage.ResourceNameT, sources []storage.ExternalSessionsSourceV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range sources {
			if n == sources[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: ExternalSessionsSourceV1s %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetExternalSessionsSourcesByName loads storage.ExternalSessionsSourceV1s using their names.
func (c *CClient) GetExternalSessionsSourcesByNames(names []storage.ResourceNameT) ([]storage.ExternalSessionsSourceV1, error) {
	// Filter.
	var filters = make([]storage.ExternalSessionsSourceFilterV1, 0, len(names))

	for _, n := range names {
		filters = append(filters, storage.ExternalSessionsSourceFilterV1{Name: n})
	}

	// Request.
	sources, err := c.GetExternalSessionsSources(filters)
	if err != nil {
		return nil, err
	}

	if err := checkExternalSessionsSourceV1sFound(names, sources); err != nil {
		return nil, err
	}

	return sources, nil
}

// CreateExternalSessionsSource creates an storage.ExternalSessionsSourceV1.
func (c *CClient) CreateExternalSessionsSource(source *storage.ExternalSessionsSourceV1,
	loadCreated bool) (*storage.ExternalSessionsSourceV1, error) {
	_url := fmt.Sprintf(externalSessionsSourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, source.Metadata.Name, loadCreated)

	resp, err := c.postRead(_url, source)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result storage.ExternalSessionsSourceV1

	if err := json.Unmarshal(resp, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// CreateExternalSessionsSources creates given storage.ExternalSessionsSourceV1s.
func (c *CClient) CreateExternalSessionsSources(sources []storage.ExternalSessionsSourceV1,
	loadCreated bool) ([]storage.ExternalSessionsSourceV1, error) {
	_url := fmt.Sprintf(externalSessionsSourceListV1Path+"?get_operation_result=%t",
		c.urlPrefix, loadCreated)

	resp, err := c.postRead(_url, sources)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result []storage.ExternalSessionsSourceV1

	if err := json.Unmarshal(resp, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// DeleteExternalSessionsSource deletes one storage.ExternalSessionsSourceV1.
func (c *CClient) DeleteExternalSessionsSource(source *storage.ExternalSessionsSourceV1) error {
	_url := fmt.Sprintf(externalSessionsSourceV1Path, c.urlPrefix, source.Metadata.Name)

	if err := c.delete(_url, source); err != nil {
		return err
	}

	return nil
}

// UpdateExternalSessionsSource updates one storage.ExternalSessionsSourceV1.
func (c *CClient) UpdateExternalSessionsSource(oldName storage.ResourceNameT,
	source *storage.ExternalSessionsSourceV1, loadUpdated bool) (*storage.ExternalSessionsSourceV1, error) {
	_url := fmt.Sprintf(externalSessionsSourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, oldName, loadUpdated)

	resp, err := c.patchRead(_url, source)
	if err != nil {
		return nil, err
	}

	if !loadUpdated {
		return nil, nil
	}

	var updated storage.ExternalSessionsSourceV1

	if err := json.Unmarshal(resp, &updated); err != nil {
		return nil, err
	}

	return &updated, nil
}
