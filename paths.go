package cclient

const (
	apiV1Suffix = "/v1"
)

// nolint: gochecknoglobals
var (
	// WebResourceCategoryListV1Path - ALL web resource categories.
	webResourceCategoryListV1Path = "%s" + apiV1Suffix + "/WebResourceCategories"
	webResourceCategoryV1Path     = webResourceCategoryListV1Path + "/%s"

	webResourceCategoryWebResourceListV1Path = webResourceCategoryV1Path + "/WebResources"
	webResourceCategoryWebResourceV1Path     = webResourceCategoryWebResourceListV1Path + "/%s"

	// Web Resources.
	webResourceListV1Path = "%s" + apiV1Suffix + "/WebResources"
	webResourceV1Path     = webResourceListV1Path + "/%s"

	// Access Groups.
	accessGroupListV1Path = "%s" + apiV1Suffix + "/AccessGroups"
	accessGroupV1Path     = accessGroupListV1Path + "/%s"

	// Access Group 2 External Groups path.
	accessGroupV12ExternalGroupsPath = accessGroupV1Path + "/ExternalGroups"

	// Access Group 2 Web Resource Categories path.
	accessGroupV12WebResourceCategoriesPath = accessGroupV1Path + "/WebResourceCategories"

	// External Users and Groups' sources.
	externalUsersGroupsSourceListV1Path = "%s" + apiV1Suffix + "/ExternalUsersGroupsSources"
	externalUsersGroupsSourceV1Path     = externalUsersGroupsSourceListV1Path + "/%s"

	// External Sessions' sources.
	externalSessionsSourceListV1Path = "%s" + apiV1Suffix + "/ExternalSessionsSources"
	externalSessionsSourceV1Path     = externalSessionsSourceListV1Path + "/%s"

	// External Users.
	externalUserListV1Path = externalUsersGroupsSourceV1Path + "/ExternalUsers"
	externalUserV1Path     = externalUserListV1Path + "/%s"

	// External User to External Groups.
	externalUserV12ExternalGroupsPath = externalUserV1Path + "/ExternalGroups"

	// External Group Lists.
	externalGroupListV1Path = externalUsersGroupsSourceV1Path + "/ExternalGroups"
	externalGroupV1Path     = externalGroupListV1Path + "/%s"

	// External Group to External Users.
	externalGroupV12ExternalUsersPath = externalGroupV1Path + "/ExternalUsers"

	// External Sessions.
	externalUserSessionListV1Path = externalSessionsSourceV1Path + "/ExternalUserSessions"
	externalUserSessionV1Path     = externalUserSessionListV1Path + "/%s"

	// Healthz.
	healthPath = "%s" + "/healthz"
)
