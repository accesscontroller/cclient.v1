package cclient

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	// Errors.

	ErrEmptyExternalUsersGroupsSourceNamePathParam = errors.New(
		"empty value given for ExternalUsersGroupsSourceNamePathParam")
	ErrEmptyAccessGroupNamePathParam            = errors.New("empty value given for AccessGroupNamePathParam")
	ErrEmptyExternalUserNamePathParam           = errors.New("empty value given for ExternalUserNamePathParam")
	ErrEmptyExternalGroupNamePathParam          = errors.New("empty value given for ExternalGroupNamePathParam")
	ErrEmptyExternalUserSessionNamePathParam    = errors.New("empty value given for ExternalUserSessionNamePathParam")
	ErrEmptyWebResourceCategoryNamePathParam    = errors.New("empty value given for WebResourceCategoryNamePathParam")
	ErrEmptyWebResourceNamePathParam            = errors.New("empty value given for WebResourceNamePathParam")
	ErrEmptySystemAccountNamePathParam          = errors.New("empty value given for SystemAccountNamePathParam")
	ErrEmptyExternalSessionsSourceNamePathParam = errors.New(
		"empty value given for ExternalSessionsSourceNamePathParam")
	ErrEmptySystemAccountSessionNamePathParam = errors.New("empty value given for SystemAccountSessionNamePathParam")

	ErrResourceNotFound                = errors.New("requested resource not found")
	ErrEmptyExternalGroupV1sSliceGiven = errors.New("empty ExternalGroupV1s slice given")
	ErrEmptyExternalUserV1sSliceGiven  = errors.New("empty ExternalUserV1s slice given")
	ErrUnsupportedResourceType         = errors.New("given resource type is not supported")
)

// ErrServerResponseStatus error means that server returned unexpected status.
type ErrServerResponseStatus struct {
	GotStatusCode      int
	ExpectedStatusCode int
	Message            string
	ServerError        error
}

func (e *ErrServerResponseStatus) Error() string {
	if e.ServerError == nil {
		return fmt.Sprintf("unexpected server response status %d, expected %d. Error Message: %s",
			e.GotStatusCode, e.ExpectedStatusCode, e.Message)
	}

	return fmt.Sprintf("unexpected server response status %d, expected %d. Error Message: %s. ServerError: %s",
		e.GotStatusCode, e.ExpectedStatusCode, e.Message, e.ServerError)
}

// GetGotStatusCode: returns HTTP status code which  was received from server.
func (e *ErrServerResponseStatus) GetGotStatusCode() int {
	return e.GotStatusCode
}

// GetExpectedStatusCode returns expected HTTP server response code.
func (e *ErrServerResponseStatus) GetExpectedStatusCode() int {
	return e.ExpectedStatusCode
}

// IsResourceNotFound returns true if error represents any resource not found error.
func (e *ErrServerResponseStatus) IsResourceNotFound() bool {
	return e.GotStatusCode == http.StatusNotFound
}

// ErrServerContentType means that server content-type in response was not expected.
type ErrServerContentType struct {
	got, expected string
}

func (e *ErrServerContentType) Error() string {
	return fmt.Sprintf("unexpected content-type %q, expected %q", e.got, e.expected)
}
