package cclient

import "fmt"

// Healthz performs simple ping to server and expects HTTP success code 2XX.
func (c *CClient) Healthz() error {
	if _, err := c.getRead(fmt.Sprintf(healthPath, c.urlPrefix), nil); err != nil {
		return fmt.Errorf("server health check failed: %w", err)
	}

	return nil
}
