package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetWebResourceCategories returns filtered Web Resource Categories.
func (c *CClient) GetWebResourceCategories(filter []storage.WebResourceCategoryFilterV1) (
	[]storage.WebResourceCategoryV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryListV1Path, c.urlPrefix)

	response, err := c.getRead(_url, filter)
	if err != nil {
		return nil, err
	}

	var result []storage.WebResourceCategoryV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// checkWebResourceCategoryV1sFound checks that for all the names there are WebResourceCategoryV1s.
func checkWebResourceCategoryV1sFound(
	names []storage.ResourceNameT, categories []storage.WebResourceCategoryV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range categories {
			if n == categories[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: WebResourceCategoryV1s %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetWebResourceCategories returns Web Resource Categories using their names.
func (c *CClient) GetWebResourceCategoriesByNames(names []storage.ResourceNameT) ([]storage.WebResourceCategoryV1, error) {
	// Filter.
	var filter = make([]storage.WebResourceCategoryFilterV1, 0, len(names))

	for _, n := range names {
		filter = append(filter, storage.WebResourceCategoryFilterV1{
			Name: n,
		})
	}

	// Load.
	categories, err := c.GetWebResourceCategories(filter)
	if err != nil {
		return nil, err
	}

	if err := checkWebResourceCategoryV1sFound(names, categories); err != nil {
		return nil, err
	}

	return categories, nil
}

// GetWebResourceCategory loads and returns one Web Resource Category by name.
func (c *CClient) GetWebResourceCategory(name storage.ResourceNameT) (
	*storage.WebResourceCategoryV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryV1Path, c.urlPrefix, name)

	response, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	var result storage.WebResourceCategoryV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// CreateWebResourceCategories creates and returns Web Resource Categories.
func (c *CClient) CreateWebResourceCategories(categories []storage.WebResourceCategoryV1, loadCreated bool) (
	[]storage.WebResourceCategoryV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryListV1Path+"?get_operation_result=%t", c.urlPrefix, loadCreated)

	response, err := c.postRead(_url, categories)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result []storage.WebResourceCategoryV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateWebResourceCategory creates and returns one Web Resource Category.
func (c *CClient) CreateWebResourceCategory(category *storage.WebResourceCategoryV1, loadCreated bool) (
	*storage.WebResourceCategoryV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryV1Path+"?get_operation_result=%t",
		c.urlPrefix, category.Metadata.Name, loadCreated)

	response, err := c.postRead(_url, category)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result storage.WebResourceCategoryV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// UpdateWebResourceCategory updates and returns one Web Resource Category.
func (c *CClient) UpdateWebResourceCategory(oldName storage.ResourceNameT, // nolint:dupl
	category *storage.WebResourceCategoryV1, loadCreated bool) (
	*storage.WebResourceCategoryV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryV1Path+"?get_operation_result=%t",
		c.urlPrefix, oldName, loadCreated)

	response, err := c.patchRead(_url, category)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result storage.WebResourceCategoryV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteWebResourceCategory deletes one Web Resource Category.
func (c *CClient) DeleteWebResourceCategory(category *storage.WebResourceCategoryV1) error {
	var _url = fmt.Sprintf(webResourceCategoryV1Path,
		c.urlPrefix, category.Metadata.Name)

	if err := c.delete(_url, category); err != nil {
		return err
	}

	return nil
}
