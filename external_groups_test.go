package cclient

import (
	"math/rand"
	"net/http"
	"sort"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalGroupSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestExternalGroupSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestExternalGroupSuite) TestGetGroupSuccess() {
	var tests = []struct {
		name   storage.ResourceNameT
		source storage.ResourceNameT

		group storage.ExternalGroupV1
	}{
		{
			name:   "external group 1 at source 2",
			source: "source 2",
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       21,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					ExternalResource: true,
					Kind:             storage.ExternalGroupKind,
					Name:             "external group 1 at source 2",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "",
				},
			},
		},
		{
			name:   "external group 2 at source 2",
			source: "source 2",
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       22,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					ExternalResource: true,
					Kind:             storage.ExternalGroupKind,
					Name:             "external group 2 at source 2",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 3",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetExternalGroup(test.name, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.group, *got, "Unexpected result")
	}
}

func (ts *TestExternalGroupSuite) TestNotFound() {
	var tests = []struct {
		name   storage.ResourceNameT
		source storage.ResourceNameT

		expectedError error
	}{
		{
			name:   "not found external group 1 at source 1",
			source: "source 1",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found external group 1 at source 1\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"source 1\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetExternalGroup(test.name, test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalGroupSuite) TestGetGroupsSuccess() {
	var tests = []struct {
		source storage.ResourceNameT
		names  []storage.ResourceNameT

		expectedGroups []storage.ExternalGroupV1
	}{
		{
			source: "source 2",
			names:  []storage.ResourceNameT{}, // Empty - must return all the groups.
			expectedGroups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             21,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 2",
					},
					Data: storage.ExternalGroupDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             22,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 2 at source 2",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 3",
					},
				},
			},
		},
		{
			source: "source 1",
			names: []storage.ResourceNameT{
				"external group 1 at source 1",
			},
			expectedGroups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             11,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 1",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetExternalGroups(test.names, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(test.expectedGroups, func(i, j int) bool {
			return test.expectedGroups[i].Metadata.Name < test.expectedGroups[j].Metadata.Name
		})

		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		ts.Equal(test.expectedGroups, got, "Unexpected returned result")
	}
}

func (ts *TestExternalGroupSuite) TestCreateGroupsSuccess() {
	var tests = []struct {
		loadCreated bool
		source      storage.ResourceNameT

		groups []storage.ExternalGroupV1
	}{
		{
			loadCreated: true,
			source:      "for groups creation source 3",
		},
		{
			loadCreated: false,
			source:      "for groups creation source 4",
		},
	}

	// Create Data.
	for i := range tests {
		test := &tests[i]

		// Fill Faker.
		if err := faker.FakeData(&test.groups); err != nil {
			ts.FailNow("Can not FakeData", err)
		}

		// Set Mandatory fields.
		for j := range test.groups {
			g := &test.groups[j]

			g.Metadata.APIVersion = storage.APIVersionV1Value
			g.Metadata.ExternalSource = &storage.ExternalSourceInfo{
				SourceName: test.source,
				Kind:       storage.ExternalUsersGroupsSourceKind,
			}
			g.Metadata.Kind = storage.ExternalGroupKind
			g.Metadata.ExternalResource = true

			g.Data.AccessGroupName = func() storage.ResourceNameT {
				var accessGroups = map[int]storage.ResourceNameT{
					0: "",
					1: "access group 1",
					2: "access group 2",
					3: "access group 3",
					4: "access group 4",
				}

				return accessGroups[rand.Intn(5)] // nolint:gosec
			}()
		}
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalGroups(test.groups, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Try to load groups from API Server to compare them.
		for j := range test.groups {
			g := &test.groups[j]

			apiGroup, err := ts.client.GetExternalGroup(g.Metadata.Name, g.Metadata.ExternalSource.SourceName)
			if err != nil {
				ts.FailNow("Can not load an ExternalGroup", "Name: %s, Source: %s, err: %s",
					g.Metadata.Name, g.Metadata.ExternalSource.SourceName, err)
			}

			ts.Equal(g, apiGroup, "Unexpected created group")
		}

		if !test.loadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })
		sort.Slice(test.groups, func(i, j int) bool { return test.groups[i].Metadata.Name < test.groups[j].Metadata.Name })

		ts.Equal(test.groups, got, "Unexpected result value")
	}
}

func (ts *TestExternalGroupSuite) TestCreateGroupsConflict() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             11,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at source 1", // Conflict.
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             11,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 10 at source 1", // No conflict.
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"external group 1 at source 1\",\"ETag\":11,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"source 1\",\"Kind\":\"ExternalUsersGroupsSource\"}},\"Data\":{\"AccessGroupName\":\"\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalGroups(test.groups, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.JSONEq(marshalJSONPanic(test.expectedError),
			marshalJSONPanic(gotErr), "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestCreateGroupsSourceNotFound() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             11,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not found source 1", // Not found source.
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "external group 1 at not found source 1",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found source 1\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalGroups(test.groups, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestCreateGroupSuccess() {
	var tests = []struct {
		loadCreated bool
		source      storage.ResourceNameT

		group storage.ExternalGroupV1
	}{
		{
			loadCreated: true,
			source:      "for groups creation source 3",
		},
		{
			loadCreated: false,
			source:      "for groups creation source 4",
		},
	}

	// Create Data.
	for i := range tests {
		test := &tests[i]

		// Fill Faker.
		if err := faker.FakeData(&test.group); err != nil {
			ts.FailNow("Can not FakeData", err)
		}

		// Set Mandatory fields.
		g := &test.group

		g.Metadata.APIVersion = storage.APIVersionV1Value
		g.Metadata.ExternalSource = &storage.ExternalSourceInfo{
			SourceName: test.source,
			Kind:       storage.ExternalUsersGroupsSourceKind,
		}
		g.Metadata.Kind = storage.ExternalGroupKind
		g.Metadata.ExternalResource = true

		g.Data.AccessGroupName = func() storage.ResourceNameT {
			var accessGroups = map[int]storage.ResourceNameT{
				0: "",
				1: "access group 1",
				2: "access group 2",
				3: "access group 3",
				4: "access group 4",
			}

			return accessGroups[rand.Intn(5)] // nolint:gosec
		}()
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalGroup(&test.group, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Try to load a group from API Server to compare them.
		g := &test.group

		apiGroup, err := ts.client.GetExternalGroup(g.Metadata.Name, g.Metadata.ExternalSource.SourceName)
		if err != nil {
			ts.FailNow("Can not load an ExternalGroup", "Name: %s, Source: %s, err: %s",
				g.Metadata.Name, g.Metadata.ExternalSource.SourceName, err)
		}

		ts.Equal(g, apiGroup, "Unexpected created group")

		if !test.loadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.group, *got, "Unexpected returned value")
	}
}

func (ts *TestExternalGroupSuite) TestCreateGroupConflict() {
	var tests = []struct {
		group storage.ExternalGroupV1

		expectedError error
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             11,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at source 1", // Conflict.
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"external group 1 at source 1\",\"ETag\":11,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"source 1\",\"Kind\":\"ExternalUsersGroupsSource\"}},\"Data\":{\"AccessGroupName\":\"\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalGroup(&test.group, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.JSONEq(marshalJSONPanic(test.expectedError),
			marshalJSONPanic(gotErr),
			"Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestCreateGroupSourceNotFound() {
	var tests = []struct {
		group storage.ExternalGroupV1

		expectedError error
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             11,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found source 1", // Not found source.
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "external group 1 at not found source 1",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found source 1\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalGroup(&test.group, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestUpdateGroupsSuccess() {
	var tests = []struct {
		loadUpdated bool

		patch []storage.ExternalGroupV1
	}{
		{
			loadUpdated: true,
			patch: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             13,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 3 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 2",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             14,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 4 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "",
					},
				},
			},
		},
		{
			loadUpdated: false,
			patch: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             13,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 5 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 2",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             14,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 6 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "",
					},
				},
			},
		},
		{
			loadUpdated: false,
			patch:       []storage.ExternalGroupV1{},
		},
		{
			loadUpdated: true,
			patch:       []storage.ExternalGroupV1{},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateExternalGroups(test.patch, test.loadUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Increase ETag.
		for j := range test.patch {
			test.patch[j].Metadata.ETag++
		}

		// Load Updated groups from API to compare.
		for j := range test.patch {
			g := &test.patch[j]

			apiGroup, err := ts.client.GetExternalGroup(g.Metadata.Name, g.Metadata.ExternalSource.SourceName)
			if err != nil {
				ts.FailNowf("Can not load an updated ExternalGroup from DB",
					"Name: %s, Source: %s, error: %s", g.Metadata.Name, g.Metadata.ExternalSource.SourceName, err)
			}

			ts.Equal(g, apiGroup, "Unexpected ExternalGroup is loaded from DB")
		}

		if !test.loadUpdated {
			continue
		}

		// Sort.
		sort.Slice(test.patch, func(i, j int) bool { return test.patch[i].Metadata.Name < test.patch[j].Metadata.Name })
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		// Compare.
		ts.Equal(test.patch, got, "Unexpected result after update")
	}
}

func (ts *TestExternalGroupSuite) TestUpdateGroupsNotFound() {
	var tests = []struct {
		patch []storage.ExternalGroupV1

		expectedError error
	}{
		{
			patch: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             13,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "not found for update external group 3 at source 5", // Not found group.
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 2",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             14,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 4 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found for update external group 3 at source 5\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for groups update source 5\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateExternalGroups(test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestUpdateGroupsETagConflict() {
	var tests = []struct {
		patch []storage.ExternalGroupV1

		expectedError error
	}{
		{
			patch: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1311, // Incorrect ETag.
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 3 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "access group 2",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             14,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups update source 5",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for update external group 4 at source 5",
					},
					Data: storage.ExternalGroupDataV1{
						AccessGroupName: "",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":13,\"given\":1311}\n",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateExternalGroups(test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestUpdateGroupSuccess() {
	var tests = []struct {
		loadUpdated bool
		oldName     storage.ResourceNameT
		patch       storage.ExternalGroupV1
	}{
		{
			loadUpdated: true,
			oldName:     "for update external group 7 at source 5",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             13,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for groups update source 5",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed for update external group 7 at source 5",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
		},
		{
			loadUpdated: false,
			oldName:     "for update external group 8 at source 5",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             14,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for groups update source 5",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed for update external group 8 at source 5",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateExternalGroup(test.oldName, &test.patch, test.loadUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Correct ETag.
		test.patch.Metadata.ETag++

		// Load Updated groups from API to compare.
		g := &test.patch

		apiGroup, err := ts.client.GetExternalGroup(g.Metadata.Name, g.Metadata.ExternalSource.SourceName)
		if err != nil {
			ts.FailNowf("Can not load an updated ExternalGroup from DB",
				"Name: %s, Source: %s, error: %s", g.Metadata.Name, g.Metadata.ExternalSource.SourceName, err)
		}

		ts.Equal(g, apiGroup, "Unexpected ExternalGroup is loaded from DB")

		if !test.loadUpdated {
			continue
		}

		// Compare.
		ts.Equal(test.patch, *got, "Unexpected result after update")
	}
}

func (ts *TestExternalGroupSuite) TestUpdateGroupNotFound() {
	var tests = []struct {
		oldName storage.ResourceNameT
		patch   storage.ExternalGroupV1

		expectedError error
	}{
		{
			oldName: "not found for update external group 3 at source 5", // Not found group.,
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             13,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for groups update source 5",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed not found for update external group 3 at source 5",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found for update external group 3 at source 5\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for groups update source 5\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateExternalGroup(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestUpdateGroupETagConflict() {
	var tests = []struct {
		oldName storage.ResourceNameT
		patch   storage.ExternalGroupV1

		expectedError error
	}{
		{
			oldName: "for update external group 9 at source 5",
			patch: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1311, // Incorrect ETag.
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for groups update source 5",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "renamed for update external group 9 at source 5",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":14,\"given\":1311}\n",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateExternalGroup(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestDeleteGroupsSuccess() {
	var tests = []struct {
		groups []storage.ExternalGroupV1
	}{
		{ // Empty list case.
			groups: []storage.ExternalGroupV1{},
		},
		{
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             13,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups deletion source 7",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for delete external group 10 at source 7",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             14,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups deletion source 7",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for delete external group 11 at source 7",
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteExternalGroups(test.groups)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Groups were deleted.
		for j := range test.groups {
			apiGroup, err := ts.client.GetExternalGroup(
				test.groups[j].Metadata.Name, test.groups[j].Metadata.ExternalSource.SourceName)

			if !ts.Error(err, "Must return error") {
				ts.FailNowf("Got no error - group was not deleted", "%+v", test.groups[j])
			}

			ts.Nilf(apiGroup, "Must return nil because the group %+v was deleted", test.groups[j].Metadata)
		}
	}
}

func (ts *TestExternalGroupSuite) TestDeleteGroupSuccess() {
	var tests = []struct {
		group storage.ExternalGroupV1
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             14,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for groups deletion source 7",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "for delete external group 12 at source 7",
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteExternalGroup(&test.group)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Group was deleted.

		apiGroup, err := ts.client.GetExternalGroup(
			test.group.Metadata.Name, test.group.Metadata.ExternalSource.SourceName)

		if !ts.Error(err, "Must return error") {
			ts.FailNowf("Got no error - group was not deleted", "%+v", test.group)
		}

		ts.Nilf(apiGroup, "Must return nil because the group %+v was deleted", test.group.Metadata)
	}
}

func (ts *TestExternalGroupSuite) TestDeleteGroupsNotFound() {
	var tests = []struct {
		groups []storage.ExternalGroupV1

		expectedError error
	}{
		{
			groups: []storage.ExternalGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups deletion source 7",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "not found group",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups deletion source 7",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "not found group 2",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             10,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for groups deletion source 7",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalGroupKind,
						Name: "for delete external group 12 at source 7",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found group\",\"ETag\":10,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for groups deletion source 7\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteExternalGroups(test.groups)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroupSuite) TestDeleteGroupNotFound() {
	var tests = []struct {
		group storage.ExternalGroupV1

		expectedError error
	}{
		{
			group: storage.ExternalGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             10,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for groups deletion source 7",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalGroupKind,
					Name: "not found group",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found group\",\"ETag\":10,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for groups deletion source 7\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteExternalGroup(&test.group)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalGroup(t *testing.T) {
	suite.Run(t, &TestExternalGroupSuite{})
}
