package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalUserSession loads one ExternalUserSessionV1 by its name.
func (c *CClient) GetExternalUserSession(name, source storage.ResourceNameT) (*storage.ExternalUserSessionV1, error) {
	// Load.
	raw, err := c.getRead(fmt.Sprintf(externalUserSessionV1Path, c.urlPrefix, source, name), nil)
	if err != nil {
		return nil, err
	}

	// Parse.
	var session storage.ExternalUserSessionV1

	if err := json.Unmarshal(raw, &session); err != nil {
		return nil, err
	}

	return &session, nil
}

// CloseExternalUserSession closes given ExternalUserSessionV1.
func (c *CClient) CloseExternalUserSession(s *storage.ExternalUserSessionV1) error {
	// Close
	if err := c.delete(fmt.Sprintf(externalUserSessionV1Path,
		c.urlPrefix, s.Metadata.ExternalSource.SourceName, s.Metadata.Name), s); err != nil {
		return err
	}

	return nil
}

// OpenExternalUserSessions opens given sessions. All the sessions must be from the same source.
func (c *CClient) OpenExternalUserSessions(
	ss []storage.ExternalUserSessionV1, loadOpened bool) ([]storage.ExternalUserSessionV1, error) {
	// Check for empty value.
	if len(ss) == 0 {
		return nil, nil
	}

	// Open.
	var _url = fmt.Sprintf(externalUserSessionListV1Path+"?get_operation_result=%t",
		c.urlPrefix, ss[0].Metadata.ExternalSource.SourceName, loadOpened)

	response, err := c.postRead(_url, ss)
	if err != nil {
		return nil, err
	}

	if !loadOpened {
		return nil, nil
	}

	// Load result.
	var sessions []storage.ExternalUserSessionV1

	if err := json.Unmarshal(response, &sessions); err != nil {
		return nil, err
	}

	return sessions, nil
}

// OpenExternalUserSession opens one ExternalUserSessionV1.
func (c *CClient) OpenExternalUserSession(s *storage.ExternalUserSessionV1, loadOpened bool, // nolint:dupl
) (*storage.ExternalUserSessionV1, error) {
	// Open.
	response, err := c.postRead(fmt.Sprintf(externalUserSessionV1Path+"?get_operation_result=%t",
		c.urlPrefix, s.Metadata.ExternalSource.SourceName, s.Metadata.Name, loadOpened), s)
	if err != nil {
		return nil, err
	}

	if !loadOpened {
		return nil, nil
	}

	var session storage.ExternalUserSessionV1

	if err := json.Unmarshal(response, &session); err != nil {
		return nil, err
	}

	return &session, nil
}

// GetExternalUserSessions returns filtered ExternalUserSessionV1s from given source.
func (c *CClient) GetExternalUserSessions(source storage.ResourceNameT,
	filter []storage.ExternalUserSessionFilterV1) ([]storage.ExternalUserSessionV1, error) {
	var _url = fmt.Sprintf(externalUserSessionListV1Path, c.urlPrefix, source)

	response, err := c.getRead(_url, filter)
	if err != nil {
		return nil, err
	}

	var result []storage.ExternalUserSessionV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// checkExternalUserSessionV1sFound checks that for all the names there are ExternalUserSessionV1s.
func checkExternalUserSessionV1sFound(
	names []storage.ResourceNameT, source storage.ResourceNameT, sessions []storage.ExternalUserSessionV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range sessions {
			if n == sessions[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: ExternalUserSessionV1s %q in source %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "), source)
	}

	return nil
}

// GetExternalUserSessionsByNames returns filtered ExternalUserSessionV1s from given source.
func (c *CClient) GetExternalUserSessionsByNames(source storage.ResourceNameT,
	names []storage.ResourceNameT) ([]storage.ExternalUserSessionV1, error) {
	// Filter.
	var filter = make([]storage.ExternalUserSessionFilterV1, 0, len(names))

	for _, n := range names {
		filter = append(filter, storage.ExternalUserSessionFilterV1{Name: n})
	}

	// Request.
	sessions, err := c.GetExternalUserSessions(source, filter)
	if err != nil {
		return nil, err
	}

	// Check that all sessions are found.
	if err := checkExternalUserSessionV1sFound(names, source, sessions); err != nil {
		return nil, err
	}

	return sessions, nil
}
