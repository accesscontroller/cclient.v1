package cclient

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	exchangeContentType = "application/json; charset=UTF-8"

	// DefaultClientTimeoutSec defines default request timeout.
	DefaultClientTimeoutSec = 10

	APIKeyHeader string = "Authentication"
)

// CClient implements API client to Access Controller server.
type CClient struct {
	client    http.Client
	urlPrefix string
	apiKey    string
}

// ServerConfig Access Controller server connection configuration.
type ServerConfig struct {
	URL     *url.URL
	TLS     *tls.Config
	Timeout time.Duration
	APIKey  string
}

// NewDefault creates a new CClient instance with default settings.
func NewDefault(server *url.URL, ca *x509.CertPool, client *tls.Certificate) (*CClient, error) {
	// Make TLS config.
	tlsConf := &tls.Config{
		Certificates: []tls.Certificate{*client},
		RootCAs:      ca,
		MinVersion:   tls.VersionTLS12,
	}

	// Make Server Config
	sc := ServerConfig{
		URL:     server,
		TLS:     tlsConf,
		Timeout: time.Second * DefaultClientTimeoutSec,
	}

	return NewCustomConfig(sc)
}

// NewCustomConfig creates a new CClient instance with user configuration.
func NewCustomConfig(sc ServerConfig) (*CClient, error) {
	httpConfig := http.Client{
		Timeout: sc.Timeout,
		Transport: &http.Transport{
			TLSClientConfig: sc.TLS,
		},
	}

	// Strip trailing slash.
	sc.URL.Path = strings.TrimRight(sc.URL.Path, "/")

	cl := &CClient{
		client:    httpConfig,
		urlPrefix: sc.URL.String(),
		apiKey:    sc.APIKey,
	}

	// Ping test.
	if err := cl.Healthz(); err != nil {
		return nil, fmt.Errorf("can not create New Client: %w", err)
	}

	return cl, nil
}
