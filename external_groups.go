package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalGroup loads an external group from server.
func (c *CClient) GetExternalGroup(name, source storage.ResourceNameT) (*storage.ExternalGroupV1, error) {
	// Get.
	_url := fmt.Sprintf(externalGroupV1Path, c.urlPrefix, source, name)

	body, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	// Parse.
	var gr storage.ExternalGroupV1

	if err := json.Unmarshal(body, &gr); err != nil {
		return nil, err
	}

	return &gr, nil
}

// checkExternalGroupV1sFound checks that for all the names there are ExternalGroupV1s.
func checkExternalGroupV1sFound(
	names []storage.ResourceNameT, source storage.ResourceNameT, groups []storage.ExternalGroupV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range groups {
			if n == groups[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: ExternalGroupV1s %q in source %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "), source)
	}

	return nil
}

// GetExternalGroups loads groups by names.
// If names are not given (nil) loads all groups.
func (c *CClient) GetExternalGroups(names []storage.ResourceNameT, source storage.ResourceNameT) ([]storage.ExternalGroupV1, error) {
	var flt = make([]storage.ExternalGroupFilterV1, len(names))

	for i := range names {
		flt[i] = storage.ExternalGroupFilterV1{
			Name: names[i],
		}
	}

	// Get.
	resp, err := c.getRead(fmt.Sprintf(externalGroupListV1Path, c.urlPrefix, source), flt)
	if err != nil {
		return nil, err
	}

	// Deserialize response.
	var result []storage.ExternalGroupV1

	if err := json.Unmarshal(resp, &result); err != nil {
		return nil, err
	}

	if err := checkExternalGroupV1sFound(names, source, result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateExternalGroups creates all given ExternalGroupV1s on API Server.
// All the groups must be from the same ExternalUsersGroupsSourceV1.
func (c *CClient) CreateExternalGroups(groups []storage.ExternalGroupV1, loadCreated bool) ([]storage.ExternalGroupV1, error) {
	if len(groups) == 0 {
		return []storage.ExternalGroupV1{}, nil
	}

	// Send.
	response, err := c.postRead(fmt.Sprintf(
		externalGroupListV1Path+"?get_operation_result=%t",
		c.urlPrefix, groups[0].Metadata.ExternalSource.SourceName, loadCreated),
		groups)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result []storage.ExternalGroupV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateExternalGroup creates a new ExternalGroup on server.
func (c *CClient) CreateExternalGroup(group *storage.ExternalGroupV1, loadCreated bool) (*storage.ExternalGroupV1, error) { // nolint:dupl
	// Send.
	response, err := c.postRead(fmt.Sprintf(
		externalGroupV1Path+"?get_operation_result=%t",
		c.urlPrefix, group.Metadata.ExternalSource.SourceName, group.Metadata.Name, loadCreated),
		group)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result storage.ExternalGroupV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// UpdateExternalGroups updates all the given ExternalGroupV1s.
// Can not rename given groups.
func (c *CClient) UpdateExternalGroups(groups []storage.ExternalGroupV1, // nolint:dupl
	loadUpdated bool) ([]storage.ExternalGroupV1, error) {
	if len(groups) == 0 {
		if loadUpdated {
			return []storage.ExternalGroupV1{}, nil
		}

		return nil, nil
	}

	// Send.
	response, err := c.patchRead(fmt.Sprintf(externalGroupListV1Path+"?get_operation_result=%t",
		c.urlPrefix, groups[0].Metadata.ExternalSource.SourceName, loadUpdated), groups)
	if err != nil {
		return nil, err
	}

	if !loadUpdated {
		return nil, nil
	}

	var result []storage.ExternalGroupV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateExternalGroup updates an existing ExternalGroup.
func (c *CClient) UpdateExternalGroup(oldName storage.ResourceNameT, // nolint:dupl
	patch *storage.ExternalGroupV1, loadUpdated bool) (*storage.ExternalGroupV1, error) {
	// Send.
	response, err := c.patchRead(fmt.Sprintf(externalGroupV1Path+"?get_operation_result=%t",
		c.urlPrefix, patch.Metadata.ExternalSource.SourceName, oldName, loadUpdated), patch)
	if err != nil {
		return nil, err
	}

	if !loadUpdated {
		return nil, nil
	}

	var result storage.ExternalGroupV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteExternalGroups deletes ExternalGroupV1s from API Server.
func (c *CClient) DeleteExternalGroups(groups []storage.ExternalGroupV1) error {
	if len(groups) == 0 {
		return nil
	}

	// Send.
	if err := c.delete(fmt.Sprintf(externalGroupListV1Path,
		c.urlPrefix, groups[0].Metadata.ExternalSource.SourceName), groups); err != nil {
		return err
	}

	return nil
}

// DeleteExternalGroup deletes one existing ExternalGroup.
func (c *CClient) DeleteExternalGroup(group *storage.ExternalGroupV1) error {
	// Send.
	if err := c.delete(fmt.Sprintf(externalGroupV1Path,
		c.urlPrefix, group.Metadata.ExternalSource.SourceName, group.Metadata.Name), group); err != nil {
		return err
	}

	return nil
}
