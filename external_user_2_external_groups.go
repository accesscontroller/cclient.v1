package cclient

import (
	"encoding/json"
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalUser2ExternalGroupListV1Relations loads and returns all Relations
// for given ExternalUser Name and Source.
func (c *CClient) GetExternalUser2ExternalGroupListV1Relations(
	name, source storage.ResourceNameT) (*storage.ExternalUser2ExternalGroupListRelationsV1, error) {
	var _url = fmt.Sprintf(externalUserV12ExternalGroupsPath, c.urlPrefix, source, name)

	response, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	// Read response.
	var result storage.ExternalUser2ExternalGroupListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// CreateExternalUser2ExternalGroupListRelationsV1 creates given Relationships on
// API Server.
func (c *CClient) CreateExternalUser2ExternalGroupListV1Relations( // nolint:dupl
	relations *storage.ExternalUser2ExternalGroupListRelationsV1,
	loadCreated bool) (*storage.ExternalUser2ExternalGroupListRelationsV1, error) {
	var _url = fmt.Sprintf(externalUserV12ExternalGroupsPath+"?get_operation_result=%t",
		c.urlPrefix, relations.Data.ExternalUsersGroupsSource,
		relations.Data.ExternalUser, loadCreated)

	response, err := c.putRead(_url, relations)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	// Read response.
	var result storage.ExternalUser2ExternalGroupListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteExternalUser2ExternalGroupListV1Relations deletes given relationships on API Server.
func (c *CClient) DeleteExternalUser2ExternalGroupListV1Relations(
	relations *storage.ExternalUser2ExternalGroupListRelationsV1) error {
	var _url = fmt.Sprintf(externalGroupV12ExternalUsersPath,
		c.urlPrefix, relations.Data.ExternalUsersGroupsSource, relations.Data.ExternalUser)

	if err := c.delete(_url, relations); err != nil {
		return err
	}

	return nil
}
