package cclient

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"net"
	"net/url"
	"os"
	"reflect"
	"time"

	"github.com/bxcodec/faker/v3"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

const testAPIServerURLEnvVar = "CCLIENT_API_SERVER_TEST_URL"

var (
	testIsFakerConfigured bool // nolint:gochecknoglobals
)

func initClientTest() (*CClient, error) {
	rawU := os.Getenv(testAPIServerURLEnvVar)
	if rawU == "" {
		return nil, errors.New("no cclient API Server URL is given for testing in os.Env CCLIENT_API_SERVER_TEST_URL") // nolint:goerr113
	}

	u, err := url.Parse(rawU)
	if err != nil {
		return nil, err
	}

	client, err := NewCustomConfig(ServerConfig{
		URL:     u,
		Timeout: time.Minute,
	})
	if err != nil {
		return nil, fmt.Errorf("can not init cclient: %w", err)
	}

	return client, nil
}

func fakerCustomGenerator() error { // nolint:gocognit,gocyclo
	if testIsFakerConfigured {
		return nil
	}

	faker.SetGenerateUniqueValues(true)

	const mapSliceMaxLength = 10

	if err := faker.SetRandomMapAndSliceSize(mapSliceMaxLength); err != nil {
		return err
	}

	if err := faker.SetRandomNumberBoundaries(1, 10000000); err != nil {
		return err
	}

	if err := faker.AddProvider("api_version", func(v reflect.Value) (interface{}, error) {
		return "v1", nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("e_tag", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(2048))
		if err != nil {
			return nil, err
		}

		return storage.ETagT(val.Uint64()), nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("default_access_policy", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(2))
		if err != nil {
			return nil, err
		}

		var res = storage.DefaultAccessPolicyT(val.Uint64())

		res++ // DefaultAccessPolicyT has value either 1 or 2.

		return res, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_domain_names", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(mapSliceMaxLength))
		if err != nil {
			return nil, err
		}

		var iVal = val.Int64()

		var domains = make([]storage.WebResourceDomainT, iVal)

		for i := range domains {
			domains[i] = storage.WebResourceDomainT(faker.DomainName())
		}

		return domains, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_urls", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(mapSliceMaxLength))
		if err != nil {
			return nil, err
		}

		var iVal = val.Int64()

		var urls = make([]url.URL, iVal)

		for i := range urls {
			us := faker.URL()

			u, err := url.Parse(us)
			if err != nil {
				return nil, err
			}

			urls[i] = *u
		}

		return urls, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_ips", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(mapSliceMaxLength))
		if err != nil {
			return nil, err
		}

		var iVal = val.Int64()

		var ips = make([]net.IP, iVal)

		for i := range ips {
			ipVer, err := rand.Int(rand.Reader, big.NewInt(2)) // 0 - IPv6, 1 - IPv4.
			if err != nil {
				return nil, err
			}

			var ipS string

			if ipVer.Int64() == 1 {
				ipS = faker.IPv4()
			} else {
				ipS = faker.IPv6()
			}

			ips[i] = net.ParseIP(ipS)
		}

		return ips, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_web_resource_category", func(v reflect.Value) (interface{}, error) {
		// One Of.
		var possibleCategories = map[int64]string{
			0: "for web resources create web resource category 117",
			1: "for web resources create web resource category 118",
			2: "for web resources create web resource category 119",
		}

		wResCatID, err := rand.Int(rand.Reader, big.NewInt(3))
		if err != nil {
			return nil, err
		}

		return possibleCategories[wResCatID.Int64()], nil
	}); err != nil {
		return err
	}

	testIsFakerConfigured = true

	return nil
}

func parseTimeRFC3339Panic(s string) *time.Time {
	tm, err := time.Parse(time.RFC3339, s)
	if err != nil {
		panic(err)
	}

	return &tm
}

func parseURLPanic(u string) url.URL {
	p, err := url.Parse(u)
	if err != nil {
		panic(err)
	}

	return *p
}

func marshalJSONPanic(v interface{}) string {
	bts, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		panic(err)
	}

	return string(bts)
}
