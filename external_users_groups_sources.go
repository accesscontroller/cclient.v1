package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalUsersGroupsSource loads and returns one storage.ExternalUsersGroupsSourceV1.
func (c *CClient) GetExternalUsersGroupsSource(usersGroupsSourceName storage.ResourceNameT) (
	*storage.ExternalUsersGroupsSourceV1, error) {
	url := fmt.Sprintf(externalUsersGroupsSourceV1Path, c.urlPrefix, usersGroupsSourceName)

	// Load.
	pl, err := c.getRead(url, nil)
	if err != nil {
		return nil, err
	}

	// Parse.
	var result storage.ExternalUsersGroupsSourceV1

	if err := json.Unmarshal(pl, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// GetExternalUsersGroupsSources loads and returns filtered ExternalUsersGroupsSourceV1s.
func (c *CClient) GetExternalUsersGroupsSources(filters []storage.ExternalUsersGroupsSourceFilterV1) (
	[]storage.ExternalUsersGroupsSourceV1, error) {
	var url = fmt.Sprintf(externalUsersGroupsSourceListV1Path, c.urlPrefix)

	response, err := c.getRead(url, filters)
	if err != nil {
		return nil, err
	}

	var result []storage.ExternalUsersGroupsSourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// checkExternalUsersGroupsSourceV1sFound checks that for all the names there are ExternalUsersGroupsSourceV1s.
func checkExternalUsersGroupsSourceV1sFound(
	names []storage.ResourceNameT, sources []storage.ExternalUsersGroupsSourceV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range sources {
			if n == sources[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: ExternalUsersGroupsSourceV1s %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetExternalUsersGroupsSourcesByNames loads and returns filtered ExternalUsersGroupsSourceV1s.
func (c *CClient) GetExternalUsersGroupsSourcesByNames(names []storage.ResourceNameT) (
	[]storage.ExternalUsersGroupsSourceV1, error) {
	// Filter.
	var filter = make([]storage.ExternalUsersGroupsSourceFilterV1, 0, len(names))

	for _, n := range names {
		filter = append(filter, storage.ExternalUsersGroupsSourceFilterV1{
			Name: n,
		})
	}

	// Load.
	sources, err := c.GetExternalUsersGroupsSources(filter)
	if err != nil {
		return nil, err
	}

	// Check that all resources are found.
	if err := checkExternalUsersGroupsSourceV1sFound(names, sources); err != nil {
		return nil, err
	}

	return sources, nil
}

// CreateExternalUsersGroupsSource creates one new ExternalUsersGroupsSourceV1.
func (c *CClient) CreateExternalUsersGroupsSource(
	source *storage.ExternalUsersGroupsSourceV1, loadCreated bool) (
	*storage.ExternalUsersGroupsSourceV1, error) {
	var _url = fmt.Sprintf(externalUsersGroupsSourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, source.Metadata.Name, loadCreated)

	response, err := c.postRead(_url, source)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result storage.ExternalUsersGroupsSourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// CreateExternalUsersGroupsSource creates all given ExternalUsersGroupsSourceV1s.
func (c *CClient) CreateExternalUsersGroupsSources(
	sources []storage.ExternalUsersGroupsSourceV1, loadCreated bool) (
	[]storage.ExternalUsersGroupsSourceV1, error) {
	var _url = fmt.Sprintf(externalUsersGroupsSourceListV1Path+"?get_operation_result=%t",
		c.urlPrefix, loadCreated)

	response, err := c.postRead(_url, sources)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result []storage.ExternalUsersGroupsSourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateExternalUsersGroupsSource updates an existing ExternalUsersGroupsSourceV1.
func (c *CClient) UpdateExternalUsersGroupsSource( // nolint:dupl
	oldName storage.ResourceNameT, patch *storage.ExternalUsersGroupsSourceV1,
	loadUpdated bool) (*storage.ExternalUsersGroupsSourceV1, error) {
	var _url = fmt.Sprintf(externalUsersGroupsSourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, oldName, loadUpdated)

	response, err := c.patchRead(_url, patch)
	if err != nil {
		return nil, err
	}

	if !loadUpdated {
		return nil, nil
	}

	var result storage.ExternalUsersGroupsSourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteExternalUsersGroupsSource deletes a given ExternalUsersGroupsSourceV1.
func (c *CClient) DeleteExternalUsersGroupsSource(source *storage.ExternalUsersGroupsSourceV1) error {
	var _url = fmt.Sprintf(externalUsersGroupsSourceV1Path, c.urlPrefix, source.Metadata.Name)

	if err := c.delete(_url, source); err != nil {
		return err
	}

	return nil
}
