package cclient

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// DeleteResourcesV1 deletes given resources from API Server.
func (c *CClient) DeleteResourcesV1(v ...interface{}) error { // nolint:gocyclo
	for i := range v {
		var reqErr error

		// Type Check.
		switch tpd := v[i].(type) {
		case storage.AccessGroup2ExternalGroupListRelationsV1:
			reqErr = c.DeleteAccessGroup2ExternalGroupListV1Relations(&tpd)
		case *storage.AccessGroup2ExternalGroupListRelationsV1:
			reqErr = c.DeleteAccessGroup2ExternalGroupListV1Relations(tpd)

		case storage.AccessGroup2WebResourceCategoryListRelationsV1:
			reqErr = c.DeleteAccessGroup2WebResourceCategoryListRelationsV1(&tpd)
		case *storage.AccessGroup2WebResourceCategoryListRelationsV1:
			reqErr = c.DeleteAccessGroup2WebResourceCategoryListRelationsV1(tpd)

		case storage.AccessGroupV1:
			reqErr = c.DeleteAccessGroup(&tpd)
		case *storage.AccessGroupV1:
			reqErr = c.DeleteAccessGroup(tpd)

		case storage.ExternalGroup2ExternalUserListRelationsV1:
			reqErr = c.DeleteExternalGroup2ExternalUserListV1Relations(&tpd)
		case *storage.ExternalGroup2ExternalUserListRelationsV1:
			reqErr = c.DeleteExternalGroup2ExternalUserListV1Relations(tpd)

		case storage.ExternalUser2ExternalGroupListRelationsV1:
			reqErr = c.DeleteExternalUser2ExternalGroupListV1Relations(&tpd)
		case *storage.ExternalUser2ExternalGroupListRelationsV1:
			reqErr = c.DeleteExternalUser2ExternalGroupListV1Relations(tpd)

		case storage.ExternalGroupV1:
			reqErr = c.DeleteExternalGroup(&tpd)
		case *storage.ExternalGroupV1:
			reqErr = c.DeleteExternalGroup(tpd)

		case storage.ExternalSessionsSourceV1:
			reqErr = c.DeleteExternalSessionsSource(&tpd)
		case *storage.ExternalSessionsSourceV1:
			reqErr = c.DeleteExternalSessionsSource(tpd)

		case storage.ExternalUserSessionV1:
			reqErr = c.CloseExternalUserSession(&tpd)
		case *storage.ExternalUserSessionV1:
			reqErr = c.CloseExternalUserSession(tpd)

		case storage.ExternalUsersGroupsSourceV1:
			reqErr = c.DeleteExternalUsersGroupsSource(&tpd)
		case *storage.ExternalUsersGroupsSourceV1:
			reqErr = c.DeleteExternalUsersGroupsSource(tpd)

		case storage.ExternalUserV1:
			reqErr = c.DeleteExternalUser(&tpd)
		case *storage.ExternalUserV1:
			reqErr = c.DeleteExternalUser(tpd)

		case storage.WebResourceCategoryV1:
			reqErr = c.DeleteWebResourceCategory(&tpd)
		case *storage.WebResourceCategoryV1:
			reqErr = c.DeleteWebResourceCategory(tpd)

		case storage.WebResourceV1:
			reqErr = c.DeleteWebResource(&tpd)
		case *storage.WebResourceV1:
			reqErr = c.DeleteWebResource(tpd)

		default:
			return fmt.Errorf("%w: %+v", ErrUnsupportedResourceType, tpd)
		}

		if reqErr != nil {
			return reqErr
		}
	}

	return nil
}
