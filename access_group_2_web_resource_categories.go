package cclient // nolint:dupl

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// CreateAccessGroup2WebResourceCategoryListRelationsV1 creates Relations for a given AccessGroupV1.
func (c *CClient) CreateAccessGroup2WebResourceCategoryListRelationsV1(
	relations *storage.AccessGroup2WebResourceCategoryListRelationsV1,
	loadCreated bool) (*storage.AccessGroup2WebResourceCategoryListRelationsV1, error) {
	var _url = fmt.Sprintf(accessGroupV12WebResourceCategoriesPath+"?get_operation_result=%t",
		c.urlPrefix, relations.Data.AccessGroup, loadCreated)

	response, err := c.putRead(_url, relations)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	// Load Result.
	var result storage.AccessGroup2WebResourceCategoryListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// GetAccessGroup2WebResourceCategoryListRelationsV1 loads Relations for a given AccessGroupV1.
func (c *CClient) GetAccessGroup2WebResourceCategoryListRelationsV1(
	accessGroup storage.ResourceNameT) (*storage.AccessGroup2WebResourceCategoryListRelationsV1, error) {
	var _url = fmt.Sprintf(accessGroupV12WebResourceCategoriesPath,
		c.urlPrefix, accessGroup)

	response, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	// Load Result.
	var result storage.AccessGroup2WebResourceCategoryListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// checkAccessGroup2WebResourceCategoryListRelationsV1sFound checks that all the names are in groups.
func checkAccessGroup2WebResourceCategoryListRelationsV1sFound(
	names []storage.ResourceNameT,
	groups []storage.AccessGroup2WebResourceCategoryListRelationsV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range groups {
			if n == groups[i].Data.AccessGroup {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf("%w: AccessGroup2WebResourceCategoryListRelationsV1 %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetAccessGroup2WebResourceCategoryListRelationsV1s loads Relations for given AccessGroupV1s.
func (c *CClient) GetAccessGroup2WebResourceCategoryListRelationsV1s(
	accessGroups []storage.ResourceNameT) ([]storage.AccessGroup2WebResourceCategoryListRelationsV1, error) {
	var result = make([]storage.AccessGroup2WebResourceCategoryListRelationsV1, 0, len(accessGroups))

	for _, n := range accessGroups {
		r, err := c.GetAccessGroup2WebResourceCategoryListRelationsV1(n)
		if err != nil {
			return nil, err
		}

		result = append(result, *r)
	}

	if err := checkAccessGroup2WebResourceCategoryListRelationsV1sFound(accessGroups, result); err != nil {
		return nil, err
	}

	return result, nil
}

// DeleteAccessGroup2WebResourceCategoryListRelationsV1 deletes Relations for given AccessGroupV1.
func (c *CClient) DeleteAccessGroup2WebResourceCategoryListRelationsV1(
	relations *storage.AccessGroup2WebResourceCategoryListRelationsV1) error {
	var _url = fmt.Sprintf(accessGroupV12WebResourceCategoriesPath,
		c.urlPrefix, relations.Data.AccessGroup)

	if err := c.delete(_url, relations); err != nil {
		return err
	}

	return nil
}
