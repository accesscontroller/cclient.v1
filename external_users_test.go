package cclient

import (
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUserSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestExternalUserSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestExternalUserSuite) TestGetUserSuccess() {
	var tests = []struct {
		name   storage.ResourceNameT
		source storage.ResourceNameT

		user storage.ExternalUserV1
	}{
		{
			name:   "external user 1 at source 1",
			source: "source 1",
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "external user 1 at source 1",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetExternalUser(test.name, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.user, *got, "Unexpected User")
	}
}

func (ts *TestExternalUserSuite) TestGetUserNotFound() {
	var tests = []struct {
		name   storage.ResourceNameT
		source storage.ResourceNameT

		expectedError error
	}{
		{
			name:   "not found external user 1 at source 1",
			source: "source 1",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"not found external user 1 at source 1\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"source 1\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetExternalUser(test.name, test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSuite) TestGetUsersSuccess() {
	var tests = []struct {
		source storage.ResourceNameT

		filter []storage.ExternalUserFilterV1

		expectedUsers []storage.ExternalUserV1
	}{
		{
			source: "source 1",
			filter: []storage.ExternalUserFilterV1{
				{
					NameSubstringMatch: "user 1",
				},
			},
			expectedUsers: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "external user 1 at source 1",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
		{
			source: "source 2",
			filter: []storage.ExternalUserFilterV1{},
			expectedUsers: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             103,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "external user 1 at source 2",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             104,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 2",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "external user 2 at source 2",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetExternalUsers(test.filter, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must return result") {
			ts.FailNow("Got nil result - can not continue")
		}

		if len(got) != len(test.expectedUsers) {
			ts.FailNowf("Unexpected returned slice length", "Expected length %d, got %d",
				len(test.expectedUsers), len(got))
		}

		// Sort.
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })
		sort.Slice(test.expectedUsers, func(i, j int) bool { return test.expectedUsers[i].Metadata.Name < test.expectedUsers[j].Metadata.Name })

		ts.Equal(test.expectedUsers, got, "Unexpected returned result")
	}
}

func (ts *TestExternalUserSuite) TestCreateUsersSuccess() {
	var tests = []struct {
		getCreated bool

		users []storage.ExternalUserV1
	}{
		{
			// Empty list case.
			getCreated: true,
			users:      []storage.ExternalUserV1{},
		},
		{
			// Empty list case.
			getCreated: false,
			users:      []storage.ExternalUserV1{},
		},
		{
			getCreated: true,
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users creation source 8",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 1 at source 8",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users creation source 8",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 2 at source 8",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
		{
			getCreated: false,
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users creation source 8",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 3 at source 8",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users creation source 8",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 4 at source 8",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalUsers(test.users, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load every user to check.
		for j := range test.users {
			expU := &test.users[j]

			apiUser, err := ts.client.GetExternalUser(expU.Metadata.Name, expU.Metadata.ExternalSource.SourceName)
			if err != nil {
				ts.FailNowf("Can not load an ExternalUserV1 from API server for check",
					"User %q, Source %q, Error: %s",
					expU.Metadata.Name, expU.Metadata.ExternalSource.SourceName, err)
			}

			ts.Equal(expU, apiUser, "Unexpected created ExternalUserV1")
		}

		if !test.getCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })
		sort.Slice(test.users, func(i, j int) bool { return test.users[i].Metadata.Name < test.users[j].Metadata.Name })

		ts.Equal(test.users, got, "Unexpected result")
	}
}

func (ts *TestExternalUserSuite) TestCreateUsersSourceNotFound() {
	var tests = []struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not found for users creation source 8", // Not found.
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 1 at source 8",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not found for users creation source 8", // Not found.
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 2 at source 8",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found for users creation source 8\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalUsers(test.users, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestCreateUsersConflict() {
	var tests = []struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "external user 1 at source 1", // Conflict
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "source 1",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "created user 42 at source 1",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"external user 1 at source 1\",\"ETag\":1101,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"source 1\",\"Kind\":\"ExternalUsersGroupsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalUsers(test.users, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestCreateUserSuccess() {
	var tests = []struct {
		getCreated bool

		user storage.ExternalUserV1
	}{
		{
			getCreated: true,
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users creation source 8",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "created user 5 at source 8",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
		{
			getCreated: false,
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users creation source 8",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "created user 6 at source 8",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalUser(&test.user, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load every user to check.
		expU := &test.user

		apiUser, err := ts.client.GetExternalUser(expU.Metadata.Name, expU.Metadata.ExternalSource.SourceName)
		if err != nil {
			ts.FailNowf("Can not load an ExternalUserV1 from API server for check",
				"User %q, Source %q, Error: %s",
				expU.Metadata.Name, expU.Metadata.ExternalSource.SourceName, err)
		}

		ts.Equal(expU, apiUser, "Unexpected created ExternalUserV1")

		if !test.getCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.user, *got, "Unexpected result")
	}
}

func (ts *TestExternalUserSuite) TestCreateUserSourceNotFound() {
	var tests = []struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found for users creation source 8", // Not found.
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "created user 1 at source 8",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found for users creation source 8\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalUser(&test.user, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestCreateUserConflict() {
	var tests = []struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "external user 2 at source 1", // Conflict.
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"external user 2 at source 1\",\"ETag\":1101,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"source 1\",\"Kind\":\"ExternalUsersGroupsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateExternalUser(&test.user, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUserSuccess() {
	var tests = []*struct {
		loadUpdated bool

		oldName storage.ResourceNameT

		patch storage.ExternalUserV1
	}{
		{
			oldName: "for update external user 1 at source 17",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             105,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users update source 17",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed for update external user 1 at source 17",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
		{
			loadUpdated: true,
			oldName:     "for update external user 2 at source 17",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             106,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users update source 17",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed for update external user 2 at source 17",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUser(test.oldName, &test.patch, test.loadUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Correct ETag increment.
		test.patch.Metadata.ETag++

		// Load a User from API for a check.
		apiUser, err := ts.client.GetExternalUser(test.patch.Metadata.Name, test.patch.Metadata.ExternalSource.SourceName)
		if err != nil {
			ts.FailNowf("Can not load an updated ExternalUserV1 from API Server",
				"User: %s, Source: %s, Error: %s",
				test.patch.Metadata.Name,
				test.patch.Metadata.ExternalSource.SourceName,
				err)
		}

		ts.Equal(test.patch, *apiUser, "Unexpected updated ExternalUserV1 got from API Server")

		if !test.loadUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil error") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.patch, *got, "Unexpected result")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUserNotFound() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUserV1

		expectedError error
	}{
		{
			oldName: "not found user",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             105,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users update source 17",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed for update external user 1 at source 17",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"not found user\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for users update source 17\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
		{
			oldName: "for update external user 2 at source 17",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             106,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found source",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "renamed for update external user 2 at source 17",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"for update external user 2 at source 17\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"not found source\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUser(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must not return nil error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUserConflict() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUserV1

		expectedError error
	}{
		{
			oldName: "for update external user 3 at source 17",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             107,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users update source 17",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "for update external user 4 at source 17", // Conflict.
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"for update external user 4 at source 17\",\"ETag\":107,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for users update source 17\",\"Kind\":\"ExternalUsersGroupsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUser(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must not return nil error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUserETag() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUserV1

		expectedError error
	}{
		{
			oldName: "for update external user 3 at source 17",
			patch: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             107123, // ETag incorrect.
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users update source 17",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "for update external user 3 at source 17",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":107,\"given\":107123}\n",
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUser(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must not return nil error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUsersSuccess() {
	var tests = []*struct {
		loadUpdated bool

		patch []storage.ExternalUserV1
	}{
		{
			patch: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             123,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 15 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             124,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 16 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
		{
			loadUpdated: true,
			patch: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             125,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 17 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             126,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 18 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUsers(test.patch, test.loadUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check each User.
		for i := range test.patch {
			up := &test.patch[i]

			// Correct ETag Increment.
			up.Metadata.ETag++

			// Load a User from API for a check.
			apiUser, err := ts.client.GetExternalUser(up.Metadata.Name, up.Metadata.ExternalSource.SourceName)
			if err != nil {
				ts.FailNowf("Can not load an updated ExternalUserV1 from API Server",
					"User: %s, Source: %s, Error: %s",
					up.Metadata.Name,
					up.Metadata.ExternalSource.SourceName,
					err)
			}

			ts.Equal(up, apiUser, "Unexpected updated ExternalUserV1 got from API Server")
		}

		if !test.loadUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil error") {
			ts.FailNow("Got nil result - can not continue")
		}

		if !ts.Equal(len(test.patch), len(got), "Lengths must be equal") {
			ts.FailNow("Expected and got lengths are not equal")
		}

		// Sort.
		sort.Slice(test.patch, func(i, j int) bool {
			return test.patch[i].Metadata.Name < test.patch[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.patch, got, "Unexpected result")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUsersNotFound() {
	var tests = []*struct {
		patch []storage.ExternalUserV1

		expectedError error
	}{
		{
			patch: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             125,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "not found user at source 17", // Not Found.
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             126,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 18 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"not found user at source 17\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for users update source 17\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
		{
			patch: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             125,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not found source", // Not Found.
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 17 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             126,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 18 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"for update external user 17 at source 17\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"not found source\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUsers(test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must not return nil error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSuite) TestUpdateUsersETag() {
	var tests = []*struct {
		patch []storage.ExternalUserV1

		expectedError error
	}{
		{
			patch: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             107123, // ETag incorrect.
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users update source 17",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for update external user 3 at source 17",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":107,\"given\":107123}\n",
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUsers(test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must not return nil error")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSuite) TestDeleteUserSuccess() {
	var tests = []*struct {
		user storage.ExternalUserV1
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             109,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users delete source 18",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "for delete external user 1 at source 18",
				},
				Data: storage.ExternalUserDataV1{},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUser(&test.user)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that a User is deleted.
		_, err := ts.client.GetExternalUser(test.user.Metadata.Name, test.user.Metadata.ExternalSource.SourceName)

		ts.Error(err, "Must return error on Get deleted User")
	}
}

func (ts *TestExternalUserSuite) TestDeleteUserNotFound() {
	var tests = []*struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             109,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users delete source 18",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "not found user at source 18",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"not found user at source 18\",\"ETag\":109,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for users delete source 18\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             109,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found source 18",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "for delete external user 1 at source 18",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"for delete external user 1 at source 18\",\"ETag\":109,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"not found source 18\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUser(&test.user)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no  error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestDeleteUserETagError() {
	var tests = []*struct {
		user storage.ExternalUserV1

		expectedError error
	}{
		{
			user: storage.ExternalUserV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             12109,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for users delete source 18",
						Kind:       storage.ExternalUsersGroupsSourceKind,
					},
					Kind: storage.ExternalUserKind,
					Name: "for delete external user 2 at source 18",
				},
				Data: storage.ExternalUserDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":110,\"given\":12109}\n",
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUser(&test.user)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no  error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestDeleteUsersSuccess() {
	var tests = []*struct {
		users []storage.ExternalUserV1
	}{
		{
			// Empty list case.
			users: []storage.ExternalUserV1{},
		},
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             111,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users delete source 18",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for delete external user 3 at source 18",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUsers(test.users)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that Users is deleted.
		for i := range test.users {
			pu := &test.users[i]

			_, err := ts.client.GetExternalUser(pu.Metadata.Name, pu.Metadata.ExternalSource.SourceName)

			ts.Error(err, "Must return error on Get deleted User")
		}
	}
}

func (ts *TestExternalUserSuite) TestDeleteUsersNotFound() {
	var tests = []*struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             109,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users delete source 18",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "not found user at source 18",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"not found user at source 18\",\"ETag\":109,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for users delete source 18\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             109,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not found source 18",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for delete external user 1 at source 18",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"for delete external user 1 at source 18\",\"ETag\":109,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"not found source 18\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUsers(test.users)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no  error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func (ts *TestExternalUserSuite) TestDeleteUsersETagError() {
	var tests = []*struct {
		users []storage.ExternalUserV1

		expectedError error
	}{
		{
			users: []storage.ExternalUserV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             12109,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for users delete source 18",
							Kind:       storage.ExternalUsersGroupsSourceKind,
						},
						Kind: storage.ExternalUserKind,
						Name: "for delete external user 2 at source 18",
					},
					Data: storage.ExternalUserDataV1{},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":110,\"given\":12109}\n",
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUsers(test.users)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no  error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error")
	}
}

func TestExternalUser(t *testing.T) {
	suite.Run(t, &TestExternalUserSuite{})
}
