package cclient

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// CreateResourcesV1 creates given resources on API Server.
// Every item in v must be a valid "storage" resource, no nested slices supported.
func (c *CClient) CreateResourcesV1(loadCreated bool, v ...interface{}) ([]interface{}, error) { // nolint:gocyclo
	var result []interface{}

	if loadCreated {
		result = make([]interface{}, 0, len(v))
	}

	for i := range v {
		var (
			resp   interface{}
			reqErr error
		)

		// Type Check.
		switch tpd := v[i].(type) {
		case storage.AccessGroup2ExternalGroupListRelationsV1:
			resp, reqErr = c.CreateAccessGroup2ExternalGroupListV1Relations(&tpd, loadCreated)
		case *storage.AccessGroup2ExternalGroupListRelationsV1:
			resp, reqErr = c.CreateAccessGroup2ExternalGroupListV1Relations(tpd, loadCreated)

		case storage.AccessGroup2WebResourceCategoryListRelationsV1:
			resp, reqErr = c.CreateAccessGroup2WebResourceCategoryListRelationsV1(&tpd, loadCreated)
		case *storage.AccessGroup2WebResourceCategoryListRelationsV1:
			resp, reqErr = c.CreateAccessGroup2WebResourceCategoryListRelationsV1(tpd, loadCreated)

		case storage.AccessGroupV1:
			resp, reqErr = c.CreateAccessGroup(&tpd, loadCreated)
		case *storage.AccessGroupV1:
			resp, reqErr = c.CreateAccessGroup(tpd, loadCreated)

		case storage.ExternalGroup2ExternalUserListRelationsV1:
			resp, reqErr = c.CreateExternalGroup2ExternalUserListV1Relations(&tpd, loadCreated)
		case *storage.ExternalGroup2ExternalUserListRelationsV1:
			resp, reqErr = c.CreateExternalGroup2ExternalUserListV1Relations(tpd, loadCreated)

		case storage.ExternalUser2ExternalGroupListRelationsV1:
			resp, reqErr = c.CreateExternalUser2ExternalGroupListV1Relations(&tpd, loadCreated)
		case *storage.ExternalUser2ExternalGroupListRelationsV1:
			resp, reqErr = c.CreateExternalUser2ExternalGroupListV1Relations(tpd, loadCreated)

		case storage.ExternalGroupV1:
			resp, reqErr = c.CreateExternalGroup(&tpd, loadCreated)
		case *storage.ExternalGroupV1:
			resp, reqErr = c.CreateExternalGroup(tpd, loadCreated)

		case storage.ExternalSessionsSourceV1:
			resp, reqErr = c.CreateExternalSessionsSource(&tpd, loadCreated)
		case *storage.ExternalSessionsSourceV1:
			resp, reqErr = c.CreateExternalSessionsSource(tpd, loadCreated)

		case storage.ExternalUserSessionV1:
			resp, reqErr = c.OpenExternalUserSession(&tpd, loadCreated)
		case *storage.ExternalUserSessionV1:
			resp, reqErr = c.OpenExternalUserSession(tpd, loadCreated)

		case storage.ExternalUsersGroupsSourceV1:
			resp, reqErr = c.CreateExternalUsersGroupsSource(&tpd, loadCreated)
		case *storage.ExternalUsersGroupsSourceV1:
			resp, reqErr = c.CreateExternalUsersGroupsSource(tpd, loadCreated)

		case storage.ExternalUserV1:
			resp, reqErr = c.CreateExternalUser(&tpd, loadCreated)
		case *storage.ExternalUserV1:
			resp, reqErr = c.CreateExternalUser(tpd, loadCreated)

		case storage.WebResourceCategoryV1:
			resp, reqErr = c.CreateWebResourceCategory(&tpd, loadCreated)
		case *storage.WebResourceCategoryV1:
			resp, reqErr = c.CreateWebResourceCategory(tpd, loadCreated)

		case storage.WebResourceV1:
			resp, reqErr = c.CreateWebResource(&tpd, loadCreated)
		case *storage.WebResourceV1:
			resp, reqErr = c.CreateWebResource(tpd, loadCreated)

		default:
			return nil, fmt.Errorf("%w: %+v", ErrUnsupportedResourceType, tpd)
		}

		if reqErr != nil {
			return nil, reqErr
		}

		if loadCreated {
			result = append(result, resp)
		}
	}

	return result, nil
}
