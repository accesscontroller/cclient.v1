package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetWebResourceCategoryWebResource returns one WebResourceV1 in a WebResourceCategory by its name.
func (c *CClient) GetWebResourceCategoryWebResource(
	category, name storage.ResourceNameT) (*storage.WebResourceV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryWebResourceV1Path, c.urlPrefix, category, name)

	response, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	var result storage.WebResourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// GetWebResourceCategoryWebResources returns Web Resources from given WebResourceCategory and filters.
func (c *CClient) GetWebResourceCategoryWebResources(category storage.ResourceNameT,
	filters []storage.WebResourceFilterV1) ([]storage.WebResourceV1, error) {
	var _url = fmt.Sprintf(webResourceCategoryWebResourceListV1Path, c.urlPrefix, category)

	response, err := c.getRead(_url, filters)
	if err != nil {
		return nil, err
	}

	var result []storage.WebResourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// GetWebResources loads WebResourceV1s using given filters.
func (c *CClient) GetWebResources(filters []storage.WebResourceFilterV1) ([]storage.WebResourceV1, error) {
	var _url = fmt.Sprintf(webResourceListV1Path, c.urlPrefix)

	response, err := c.getRead(_url, filters)
	if err != nil {
		return nil, err
	}

	var result []storage.WebResourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// checkWebResourceV1sFound checks that for all the names there are WebResourceV1s.
func checkWebResourceV1sFound(
	names []storage.ResourceNameT, webResources []storage.WebResourceV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range webResources {
			if n == webResources[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: WebResourceV1s %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetWebResourcesByNames loads WeResourceV1s using their names.
func (c *CClient) GetWebResourcesByNames(names []storage.ResourceNameT) ([]storage.WebResourceV1, error) {
	// Filters.
	var filters = make([]storage.WebResourceFilterV1, 0, len(names))

	for _, n := range names {
		filters = append(filters, storage.WebResourceFilterV1{
			NameSubstringMatch: n,
		})
	}

	resources, err := c.GetWebResources(filters)
	if err != nil {
		return nil, err
	}

	if err := checkWebResourceV1sFound(names, resources); err != nil {
		return nil, err
	}

	return resources, nil
}

// CreateWebResources creates given Web Resources on API Server.
func (c *CClient) CreateWebResources(resources []storage.WebResourceV1,
	getCreated bool) ([]storage.WebResourceV1, error) {
	var _url = fmt.Sprintf(webResourceListV1Path+"?get_operation_result=%t", c.urlPrefix, getCreated)

	response, err := c.postRead(_url, resources)
	if err != nil {
		return nil, err
	}

	if !getCreated {
		return nil, nil
	}

	var result []storage.WebResourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateWebResource creates one Web Resource on API Server.
func (c *CClient) CreateWebResource(resource *storage.WebResourceV1,
	getCreated bool) (*storage.WebResourceV1, error) {
	var _url = fmt.Sprintf(webResourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, resource.Metadata.Name, getCreated)

	response, err := c.postRead(_url, resource)
	if err != nil {
		return nil, err
	}

	if !getCreated {
		return nil, nil
	}

	var result storage.WebResourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// UpdateWebResource updates one Web Resource on API Server.
func (c *CClient) UpdateWebResource(oldName storage.ResourceNameT, resource *storage.WebResourceV1, // nolint:dupl
	getUpdated bool) (*storage.WebResourceV1, error) {
	var _url = fmt.Sprintf(webResourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, oldName, getUpdated)

	response, err := c.patchRead(_url, resource)
	if err != nil {
		return nil, err
	}

	if !getUpdated {
		return nil, nil
	}

	var result storage.WebResourceV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteWebResource deletes one WebResource from API Server.
func (c *CClient) DeleteWebResource(resource *storage.WebResourceV1) error {
	var _url = fmt.Sprintf(webResourceV1Path+"?get_operation_result=%t",
		c.urlPrefix, resource.Metadata.Name)

	if err := c.delete(_url, resource); err != nil {
		return err
	}

	return nil
}
