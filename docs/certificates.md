# Client Certificate generation

cclient.v1 uses TLS authentication on API server.
Certificates must contain `OU=value` with role names defined on API server role model.

API server role definitions are described in API server docs.

Below are Cloufare CFSSL json configurations for issuing certificates (I will be glad if somebody helps me with OpenSSL configuration files):

CFSSL config:

```json
{
    "signing": {
        "default": {
            "expiry": "168h"
        },
        "profiles": {
            "www": {
                "expiry": "8760h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth"
                ]
            },
            "client": {
                "expiry": "8760h",
                "usages": [
                    "signing",
                    "key encipherment",
                    "client auth"
                ]
            }
        }
    }
}

```

CFSSL client certificate request configuration which gives minimal required permissions
for ExternalUserSessionV1s, ExternalUserV1s and ExternalGroupV1s management.

```json
{
    "CN": "sessions-users-groups-importer1", // Example name. It is used in logging.
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [{
            "C": "RU",
            "ST": "Krasnodar region",
            "L": "Krasnodar",
            "OU": "SessionsImporter" // Role 1 - SessionsImporter.
        },
        {
            "OU": "UsersGroupsImporter" // Role 2 - UsersGroupsImporter.
        }
    ]
}
```
