package cclient

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUsersGroupsSourceSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestExternalUsersGroupsSourceSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestGetSourceSuccess() {
	var tests = []*struct {
		name storage.ResourceNameT

		expectedSource storage.ExternalUsersGroupsSourceV1
	}{
		{
			name: "source 1",
			expectedSource: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 3600,
					GetMode:         storage.ExternalUsersGroupsGetModePoll,
					Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
					GroupsToMonitor: []string{"group1", "group2"},
					GroupsSourceSettings: map[string]string{
						"group sett key1": "group sett val1",
					},
					UsersSourceSettings: map[string]string{
						"key1": "value1",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalUsersGroupsSource(test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedSource, *got, "Unexpected result value")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestGetSourcesSuccess() {
	var tests = []*struct {
		filters []storage.ExternalUsersGroupsSourceFilterV1

		expectedSources []storage.ExternalUsersGroupsSourceV1
	}{
		{
			filters: []storage.ExternalUsersGroupsSourceFilterV1{
				{
					Name: "source 1",
				},
			},
			expectedSources: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 1",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 3600,
						GetMode:         storage.ExternalUsersGroupsGetModePoll,
						Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
						GroupsToMonitor: []string{"group1", "group2"},
						GroupsSourceSettings: map[string]string{
							"group sett key1": "group sett val1",
						},
						UsersSourceSettings: map[string]string{
							"key1": "value1",
						},
					},
				},
			},
		},
		{
			filters: []storage.ExternalUsersGroupsSourceFilterV1{
				{
					Name: "source 1",
				},
				{
					Name: "source 2",
				},
			},
			expectedSources: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 1",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 3600,
						GetMode:         storage.ExternalUsersGroupsGetModePoll,
						Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
						GroupsToMonitor: []string{"group1", "group2"},
						GroupsSourceSettings: map[string]string{
							"group sett key1": "group sett val1",
						},
						UsersSourceSettings: map[string]string{
							"key1": "value1",
						},
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       2,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 2",
					},
					Data: storage.ExternalUsersGroupsSourceDataV1{
						PollIntervalSec: 3600,
						GetMode:         storage.ExternalUsersGroupsGetModePoll,
						Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
						GroupsToMonitor: []string{"group1", "group2"},
						GroupsSourceSettings: map[string]string{
							"group sett key1": "group sett val1",
						},
						UsersSourceSettings: map[string]string{
							"key2": "value2",
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalUsersGroupsSources(test.filters)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.Len(got, len(test.expectedSources), "Unexpected result slice length") {
			ts.FailNow("Length check failed")
		}

		sort.Slice(test.expectedSources, func(i, j int) bool {
			return test.expectedSources[i].Metadata.Name < test.expectedSources[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		ts.Equal(test.expectedSources, got, "Unexpected result")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestCreateSourceSuccess() {
	var tests = []*struct {
		LoadCreated bool

		Source storage.ExternalUsersGroupsSourceV1
	}{}

	// Faker.
	// Set nil for empty slice. ToDo: Fix it, because without the setting here other tests can fail.
	faker.SetNilIfLenIsZero(true)
	defer faker.SetNilIfLenIsZero(false)

	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	for _, test := range tests {
		test.Source.Metadata.Kind = storage.ExternalUsersGroupsSourceKind
	}

	// Create.
	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalUsersGroupsSource(&test.Source, test.LoadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a Source from API to check.
		apiSource, err := ts.client.GetExternalUsersGroupsSource(test.Source.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load a created source from DB - can not continue",
				"Error: %s", err)
		}

		ts.Equal(test.Source, *apiSource, "Unexpected API Source value")

		if !test.LoadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.Source, *got, "Unexpected result")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestCreateSourcesSuccess() {
	type TestCase struct {
		LoadCreated bool

		Sources []storage.ExternalUsersGroupsSourceV1
	}

	var tests = make([]*TestCase, 0)

	// Set nil for empty slice. ToDo: Fix it, because without the setting here other tests can fail.
	faker.SetNilIfLenIsZero(true)
	defer faker.SetNilIfLenIsZero(false)

	// Faker.
	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	for _, test := range tests {
		for i := range test.Sources {
			test.Sources[i].Metadata.Kind = storage.ExternalUsersGroupsSourceKind
		}
	}

	// Create.
	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalUsersGroupsSources(test.Sources, test.LoadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			bts, _ := json.MarshalIndent(test.Sources, "", "    ")

			fmt.Println(string(bts))

			ts.FailNow("Got error - can not continue")
		}

		// Load a Source from API to check.
		for i := range test.Sources {
			apiSource, err := ts.client.GetExternalUsersGroupsSource(test.Sources[i].Metadata.Name)
			if err != nil {
				ts.FailNowf("Can not load a created source from DB - can not continue",
					"Error: %s", err)
			}

			ts.Equal(test.Sources[i], *apiSource, "Unexpected API Source value")
		}

		if !test.LoadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(test.Sources, func(i, j int) bool {
			return test.Sources[i].Metadata.Name < test.Sources[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		ts.Equal(test.Sources, got, "Unexpected result")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestCreateSourcesConflict() {
	type TestCase struct {
		Sources []storage.ExternalUsersGroupsSourceV1

		ExpectedError error
	}

	var tests = []*TestCase{
		{
			Sources: []storage.ExternalUsersGroupsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       110,
						Kind:       storage.ExternalUsersGroupsSourceKind,
						Name:       "source 1",
					},
				},
			},
			ExpectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"source 1\",\"ETag\":110,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	// Create.
	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalUsersGroupsSources(test.Sources, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		// Sort.
		ts.Equal(test.ExpectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestCreateSourceConflict() {
	type TestCase struct {
		Source storage.ExternalUsersGroupsSourceV1

		ExpectedError error
	}

	var tests = []*TestCase{
		{
			Source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       110,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "source 1",
				},
			},
			ExpectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"source 1\",\"ETag\":110,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	// Create.
	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalUsersGroupsSource(&test.Source, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		if !ts.Nil(got, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		// Sort.
		ts.Equal(test.ExpectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestUpdateSourceSuccess() {
	var tests = []*struct {
		loadUpdated bool
		oldName     storage.ResourceNameT

		patch storage.ExternalUsersGroupsSourceV1
	}{
		{
			loadUpdated: true,
			oldName:     "source 13",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       113,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "updated source 13",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 1313,
					GetMode:         storage.ExternalUsersGroupsGetModePoll,
					GroupsSourceSettings: map[string]string{
						"updated key 13": "updated value 13",
					},
					GroupsToMonitor: []string{"new group 13"},
					Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
					UsersSourceSettings: map[string]string{
						"updated user settings key 13": "value 13",
					},
				},
			},
		},
		{
			loadUpdated: false,
			oldName:     "source 14",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       114,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "updated source 14",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 1414,
					GetMode:         storage.ExternalUsersGroupsGetModePoll,
					GroupsSourceSettings: map[string]string{
						"updated key 14": "updated value 14",
					},
					GroupsToMonitor: []string{"new group 14"},
					Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
					UsersSourceSettings: map[string]string{
						"updated user settings key 14": "value 14",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUsersGroupsSource(test.oldName, &test.patch, test.loadUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Increment ETag for further comparison.
		test.patch.Metadata.ETag++

		// Load an updated Source from API.
		apiSource, err := ts.client.GetExternalUsersGroupsSource(test.patch.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not get Updated Source from API", "Source: %s, Error: %s",
				test.patch.Metadata.Name, err)
		}

		ts.Equal(test.patch, *apiSource, "Unexpected API Source value")

		if !test.loadUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.patch, *got, "Unexpected result value")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestUpdateSourceNotFoundError() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalUsersGroupsSourceV1

		expectedError error
	}{
		{
			oldName: "not found source",
			patch: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       113,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "updated source 13",
				},
				Data: storage.ExternalUsersGroupsSourceDataV1{
					PollIntervalSec: 1313,
					GetMode:         storage.ExternalUsersGroupsGetModePoll,
					GroupsSourceSettings: map[string]string{
						"updated key 13": "updated value 13",
					},
					GroupsToMonitor: []string{"new group 13"},
					Type:            storage.ExternalUsersGroupsSourceTypeLDAP,
					UsersSourceSettings: map[string]string{
						"updated user settings key 13": "value 13",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateExternalUsersGroupsSource(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestDeleteSourceSuccess() {
	var tests = []*struct {
		source storage.ExternalUsersGroupsSourceV1
	}{
		{
			source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       115,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "for deletion source 15",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUsersGroupsSource(&test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Try to load a Source from DB for a check.
		if apiSource, err := ts.client.GetExternalUsersGroupsSource(
			test.source.Metadata.Name); err == nil {
			ts.FailNowf("Got a Source which must be deleted", "Source: %+v", apiSource)
		}
	}
}

func (ts *TestExternalUsersGroupsSourceSuite) TestDeleteNotFoundError() {
	var tests = []*struct {
		source storage.ExternalUsersGroupsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalUsersGroupsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       115,
					Kind:       storage.ExternalUsersGroupsSourceKind,
					Name:       "not found source",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found source\",\"ETag\":115,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalUsersGroupsSource(&test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalUsersGroupsSource(t *testing.T) {
	suite.Run(t, &TestExternalUsersGroupsSourceSuite{})
}
