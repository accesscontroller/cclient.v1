package cclient

import (
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalSessionsSourceSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestExternalSessionsSourceSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestExternalSessionsSourceSuite) TestGetSessionsSourceSuccess() {
	var tests = []*struct {
		name storage.ResourceNameT

		expectedSource storage.ExternalSessionsSourceV1
	}{
		{
			name: "sessions source 1",
			expectedSource: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 1",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"key1": "value1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
		{
			name: "sessions source 2",
			expectedSource: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       102,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 2",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 2",
					Settings: map[string]string{
						"key2": "value2",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalSessionsSource(test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedSource, *got, "Unexpected result value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestGetSessionsSourceNotFound() {
	var tests = []*struct {
		name storage.ResourceNameT

		expectedError error
	}{
		{
			name: "not found sessions source",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"not found sessions source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalSessionsSource(test.name)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestCreateSessionsSourceSuccess() {
	var tests = []*struct {
		loadCreated bool

		source storage.ExternalSessionsSourceV1
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "new sessions source 10001",
					ETag:       10001,
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"key 10001": "value 10001",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
		{
			loadCreated: true,
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "new sessions source 10002",
					ETag:       10002,
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 2",
					Settings: map[string]string{
						"key 10002": "value 10002",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalSessionsSource(&test.source, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a created Source from API.
		apiSource, err := ts.client.GetExternalSessionsSource(test.source.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load Sessions Source from API", "Name: %s, error: %s",
				test.source.Metadata.Name, err)
		}

		// Compare.
		ts.Equal(test.source, *apiSource, "Unexpected API Source")

		if !test.loadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.source, *got, "Unexpected result")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestCreateSessionsSourceConflict() {
	var tests = []*struct {
		source storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "sessions source 1",
					ETag:       10001,
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"key 10001": "value 10001",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"sessions source 1\",\"ETag\":10001,\"ExternalResource\":false,\"ExternalSource\":null},\"Data\":{\"GetMode\":\"passive wait\",\"Type\":\"MSADEvents\",\"ExternalUsersSourceName\":\"source 1\",\"Settings\":{\"key 10001\":\"value 10001\"}}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalSessionsSource(&test.source, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestCreateSessionsSourceUsersGroupsSourceNotFound() {
	var tests = []*struct {
		source storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "new sessions source 20001",
					ETag:       10001,
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "not found users groups source",
					Settings: map[string]string{
						"key 10001": "value 10001",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found users groups source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalSessionsSource(&test.source, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestDeleteSuccess() {
	var tests = []*struct {
		source storage.ExternalSessionsSourceV1
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       103,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "for deletion sessions source 3",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalSessionsSource(&test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that there is no more the Source in API.
		_, err := ts.client.GetExternalSessionsSource(test.source.Metadata.Name)

		if err == nil {
			ts.FailNow("Got no error from API Server on Get Deleted Source request")
		}

		typedErr := err.(*ErrServerResponseStatus)

		ts.Equal(http.StatusNotFound, typedErr.GotStatusCode, "Unexpected check status")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestDeleteETagError() {
	var tests = []*struct {
		source storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       55104,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "for deletion sessions source 4",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":104,\"given\":55104}\n",
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalSessionsSource(&test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestDeleteNotFoundError() {
	var tests = []*struct {
		source storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			source: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       55104,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "not found source",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"not found source\",\"ETag\":55104,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalSessionsSource(&test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestUpdateSuccess() {
	var tests = []*struct {
		loadUpdated bool

		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1
	}{
		{
			oldName: "for update sessions source 5",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       105,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed for update sessions source 5",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"updated key 1": "updated value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
		{
			loadUpdated: true,
			oldName:     "for update sessions source 6",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       106,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed for update sessions source 6",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePassive,
					ExternalUsersSourceName: "source 2",
					Settings: map[string]string{
						"updated key 2": "updated value 2",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
		},
	}

	for _, test := range tests {
		got, goterr := ts.client.UpdateExternalSessionsSource(test.oldName, &test.patch, test.loadUpdated)

		if !ts.NoError(goterr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		apiSource, err := ts.client.GetExternalSessionsSource(test.patch.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load Sessions Source from API", "Name: %s, error: %s",
				test.patch.Metadata.Name, err)
		}

		// Compare.
		// Correct expected ETag.
		test.patch.Metadata.ETag++

		ts.Equal(test.patch, *apiSource, "Unexpected API Source")

		if !test.loadUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.patch, *got, "Unexpected result")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestUpdateNotFound() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			oldName: "not found sessions source",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       105,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed for update sessions source 5",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1",
					Settings: map[string]string{
						"updated key 1": "updated value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"not found sessions source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, goterr := ts.client.UpdateExternalSessionsSource(test.oldName, &test.patch, true)

		if !ts.Error(goterr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, goterr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestUpdateETagError() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			oldName: "for update sessions source 7",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111102, // Incorrect ETag.
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed for update sessions source 7",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 2",
					Settings: map[string]string{
						"updated key 1": "updated value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":107,\"given\":111102}\n",
			},
		},
	}

	for _, test := range tests {
		got, goterr := ts.client.UpdateExternalSessionsSource(test.oldName, &test.patch, true)

		if !ts.Error(goterr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, goterr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestUpdateForbiddenFieldError() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			oldName: "for update sessions source 8",
			patch: storage.ExternalSessionsSourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       108,
					Kind:       storage.ExternalSessionsSourceKind,
					Name:       "renamed for update sessions source 8",
				},
				Data: storage.ExternalSessionsSourceDataV1{
					GetMode:                 storage.ExternalSessionsGetModePoll,
					ExternalUsersSourceName: "source 1", // Forbidden update.
					Settings: map[string]string{
						"updated key 1": "updated value 1",
					},
					Type: storage.ExternalSessionsSourceTypeMSADEvents,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusBadRequest,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrForbiddenUpdateReadOnlyField\",\"forbidden_field\":\"Data#ExternalUsersSourceName\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"renamed for update sessions source 8\",\"ETag\":108,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, goterr := ts.client.UpdateExternalSessionsSource(test.oldName, &test.patch, true)

		if !ts.Error(goterr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, goterr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestCreateSessionsSourcesSuccess() {
	var tests = []*struct {
		loadCreated bool

		sources []storage.ExternalSessionsSourceV1
	}{
		{
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "new sessions source 20001",
						ETag:       20001,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "new sessions source 20002",
						ETag:       20002,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
		{
			loadCreated: true,
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "new sessions source 30001",
						ETag:       30001,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "new sessions source 30002",
						ETag:       30002,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalSessionsSources(test.sources, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a created Source from API.
		for j := range test.sources {
			expSrc := &test.sources[j]

			apiSource, err := ts.client.GetExternalSessionsSource(expSrc.Metadata.Name)
			if err != nil {
				ts.FailNowf("Can not load Sessions Source from API", "Name: %s, error: %s",
					expSrc.Metadata.Name, err)
			}

			// Compare.
			ts.Equal(expSrc, apiSource, "Unexpected API Source value")
		}

		if !test.loadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		if len(test.sources) != len(got) {
			ts.FailNow("Expected Sources slice length is not equal to got slice length")
		}

		// Sort.
		sort.Slice(test.sources, func(i, j int) bool { return test.sources[i].Metadata.Name < test.sources[j].Metadata.Name }) // nolint:scopelint
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		ts.Equal(test.sources, got, "Unexpected result")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestCreateSessionsSourcesConflict() {
	var tests = []*struct {
		sources []storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1", // Conflict.
						ETag:       10001,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 40001",
						ETag:       10001,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"sessions source 1\",\"ETag\":10001,\"ExternalResource\":false,\"ExternalSource\":null},\"Data\":{\"GetMode\":\"passive wait\",\"Type\":\"MSADEvents\",\"ExternalUsersSourceName\":\"source 1\",\"Settings\":{\"key 10001\":\"value 10001\"}}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalSessionsSources(test.sources, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestCreateSessionsSourcesUsersGroupsSourceNotFound() {
	var tests = []*struct {
		sources []storage.ExternalSessionsSourceV1

		expectedError error
	}{
		{
			sources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "new sessions source 20001",
						ETag:       10001,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "not found users groups source", // Not found.
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{ // Correct.
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 50001",
						ETag:       10001,
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key 10001": "value 10001",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUsersGroupsSource\",\"Name\":\"not found users groups source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalSessionsSources(test.sources, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalSessionsSourceSuite) TestGetSessionsSourcesSuccess() {
	var tests = []*struct {
		filter []storage.ExternalSessionsSourceFilterV1

		expectedSources []storage.ExternalSessionsSourceV1
	}{
		{
			filter: []storage.ExternalSessionsSourceFilterV1{
				{
					Name: "sessions source 1",
				},
			},
			expectedSources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       101,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key1": "value1",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
		{
			filter: []storage.ExternalSessionsSourceFilterV1{
				{
					Name: "sessions source 1",
				},
				{
					Name: "sessions source 2",
				},
			},
			expectedSources: []storage.ExternalSessionsSourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       101,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 1",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePoll,
						ExternalUsersSourceName: "source 1",
						Settings: map[string]string{
							"key1": "value1",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       102,
						Kind:       storage.ExternalSessionsSourceKind,
						Name:       "sessions source 2",
					},
					Data: storage.ExternalSessionsSourceDataV1{
						GetMode:                 storage.ExternalSessionsGetModePassive,
						ExternalUsersSourceName: "source 2",
						Settings: map[string]string{
							"key2": "value2",
						},
						Type: storage.ExternalSessionsSourceTypeMSADEvents,
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalSessionsSources(test.filter)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(test.expectedSources, func(i, j int) bool {
			return test.expectedSources[i].Metadata.Name < test.expectedSources[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		ts.Equal(test.expectedSources, got, "Unexpected result value")
	}
}

func TestExternalSessionsSourceV1(t *testing.T) {
	suite.Run(t, &TestExternalSessionsSourceSuite{})
}
