package cclient

import (
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroup2WebResourceCategoriesRelationsSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestGetSuccess() {
	var tests = []*struct {
		accessGroup storage.ResourceNameT

		expectedRelations storage.AccessGroup2WebResourceCategoryListRelationsV1
	}{
		{
			accessGroup: "for get web resource category relations access group 13",
			expectedRelations: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "for get web resource category relations access group 13",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for get web resource category relations access group 13",
					Categories: []storage.ResourceNameT{
						"web resource category 1 binded to for get web resource category relations access group 13",
						"web resource category 2 binded to for get web resource category relations access group 13",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetAccessGroup2WebResourceCategoryListRelationsV1(test.accessGroup)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(got.Data.Categories, func(i, j int) bool { return got.Data.Categories[i] < got.Data.Categories[j] })
		sort.Slice(test.expectedRelations.Data.Categories, func(i, j int) bool {
			return test.expectedRelations.Data.Categories[i] < test.expectedRelations.Data.Categories[j] // nolint:scopelint
		})

		ts.Equal(test.expectedRelations, *got, "Unexpected result")
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestCreateSuccess() {
	var tests = []*struct {
		loadCreated bool

		relationsToCreate storage.AccessGroup2WebResourceCategoryListRelationsV1
		expectedRelations storage.AccessGroup2WebResourceCategoryListRelationsV1
	}{
		{
			relationsToCreate: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "relations to create",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for bind web resource category relations access group 14",
					Categories: []storage.ResourceNameT{
						"for bind web resource category 3 to for get web resource category relations access group 14",
						"for bind web resource category 4 to for get web resource category relations access group 14",
					},
				},
			},
			expectedRelations: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "for bind web resource category relations access group 14",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for bind web resource category relations access group 14",
					Categories: []storage.ResourceNameT{
						"for bind web resource category 3 to for get web resource category relations access group 14",
						"for bind web resource category 4 to for get web resource category relations access group 14",
						"already binded web resource category 5 to for get web resource category relations access group 14",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateAccessGroup2WebResourceCategoryListRelationsV1(
			&test.relationsToCreate, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load existing relations.
		apiRels, err := ts.client.GetAccessGroup2WebResourceCategoryListRelationsV1(test.relationsToCreate.Data.AccessGroup)
		if err != nil {
			ts.FailNowf("Can not get AccessGroup to WebResourceCategories relations",
				"AccessGroup: %s, error: %s", test.relationsToCreate.Data.AccessGroup, err)
		}

		if apiRels == nil {
			ts.FailNow("Got nil API Relations, can not continue")
		}

		// Sort.
		sort.Slice(apiRels.Data.Categories, func(i, j int) bool { return apiRels.Data.Categories[i] < apiRels.Data.Categories[j] })
		sort.Slice(test.expectedRelations.Data.Categories, func(i, j int) bool {
			return test.expectedRelations.Data.Categories[i] < test.expectedRelations.Data.Categories[j] // nolint:scopelint
		})

		// Compare.
		ts.Equal(test.expectedRelations, *apiRels, "Unexpected relations value got from API Server")

		if !test.loadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(got.Data.Categories, func(i, j int) bool { return got.Data.Categories[i] < got.Data.Categories[j] })

		ts.Equal(test.expectedRelations, *got, "Unexpected result")
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestCreateAccessGroupNotFound() {
	var tests = []*struct {
		relationsToCreate storage.AccessGroup2WebResourceCategoryListRelationsV1

		expectedError error
	}{
		{
			relationsToCreate: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "relations to create",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "not found access group", // Not Found.
					Categories: []storage.ResourceNameT{
						"for bind web resource category 3 to for get web resource category relations access group 14",
						"for bind web resource category 4 to for get web resource category relations access group 14",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateAccessGroup2WebResourceCategoryListRelationsV1(
			&test.relationsToCreate, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestCreateWebResourceNotFound() {
	var tests = []*struct {
		relationsToCreate storage.AccessGroup2WebResourceCategoryListRelationsV1

		expectedError error
	}{
		{
			relationsToCreate: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "relations to create",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for bind web resource category relations access group 14",
					Categories: []storage.ResourceNameT{
						"for bind web resource category 3 to for get web resource category relations access group 14",
						"for bind web resource category 4 to for get web resource category relations access group 14",
						"not found web resource category",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"api_version\":\"v1\",\"error_kind\":\"ErrRelatedResourcesNotFound\",\"kind\":\"WebResourceCategory\",\"names\":[\"not found web resource category\"]}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateAccessGroup2WebResourceCategoryListRelationsV1(
			&test.relationsToCreate, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestDeleteSuccess() {
	var tests = []*struct {
		loadCreated bool

		relationsToDelete storage.AccessGroup2WebResourceCategoryListRelationsV1
		expectedRelations storage.AccessGroup2WebResourceCategoryListRelationsV1
	}{
		{
			relationsToDelete: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "relations to delete",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for delete bind web resource category relations access group 15",
					Categories: []storage.ResourceNameT{
						"for delete web resource category 6 to for get web resource category relations access group 15",
						"for delete web resource category 7 to for get web resource category relations access group 15",
					},
				},
			},
			expectedRelations: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "for delete bind web resource category relations access group 15",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for delete bind web resource category relations access group 15",
					Categories: []storage.ResourceNameT{
						"already binded web resource category 8 to for get web resource category relations access group 15",
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteAccessGroup2WebResourceCategoryListRelationsV1(
			&test.relationsToDelete)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load existing relations.
		apiRels, err := ts.client.GetAccessGroup2WebResourceCategoryListRelationsV1(test.relationsToDelete.Data.AccessGroup)
		if err != nil {
			ts.FailNowf("Can not get AccessGroup to WebResourceCategories relations",
				"AccessGroup: %s, error: %s", test.relationsToDelete.Data.AccessGroup, err)
		}

		if apiRels == nil {
			ts.FailNow("Got nil API Relations, can not continue")
		}

		// Sort.
		sort.Slice(apiRels.Data.Categories, func(i, j int) bool { return apiRels.Data.Categories[i] < apiRels.Data.Categories[j] })
		sort.Slice(test.expectedRelations.Data.Categories, func(i, j int) bool {
			return test.expectedRelations.Data.Categories[i] < test.expectedRelations.Data.Categories[j] // nolint:scopelint
		})

		// Compare.
		ts.Equal(test.expectedRelations, *apiRels, "Unexpected relations value got from API Server")
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestDeleteAccessGroupNotFound() {
	var tests = []*struct {
		relationsToCreate storage.AccessGroup2WebResourceCategoryListRelationsV1

		expectedError error
	}{
		{
			relationsToCreate: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "relations to create",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "not found access group", // Not Found.
					Categories: []storage.ResourceNameT{
						"for bind web resource category 3 to for get web resource category relations access group 14",
						"for bind web resource category 4 to for get web resource category relations access group 14",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteAccessGroup2WebResourceCategoryListRelationsV1(
			&test.relationsToCreate)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroup2WebResourceCategoriesRelationsSuite) TestDeleteWebResourceCategoryNotFound() {
	var tests = []*struct {
		relationsToCreate storage.AccessGroup2WebResourceCategoryListRelationsV1

		expectedError error
	}{
		{
			relationsToCreate: storage.AccessGroup2WebResourceCategoryListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
					Name:       "relations to create",
				},
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "for bind web resource category relations access group 14",
					Categories: []storage.ResourceNameT{
						"for bind web resource category 3 to for get web resource category relations access group 14",
						"for bind web resource category 4 to for get web resource category relations access group 14",
						"not found web resource category",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"api_version\":\"v1\",\"error_kind\":\"ErrRelatedResourcesNotFound\",\"kind\":\"WebResourceCategory\",\"names\":[\"not found web resource category\"]}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteAccessGroup2WebResourceCategoryListRelationsV1(
			&test.relationsToCreate)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestAccessGroup2WebResourceCategoriesRelations(t *testing.T) {
	suite.Run(t, &TestAccessGroup2WebResourceCategoriesRelationsSuite{})
}
