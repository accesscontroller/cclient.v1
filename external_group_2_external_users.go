package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalGroup2ExternalUserListRelationsV1 loads and returns all Relations
// for given ExternalGroup Name and Source.
func (c *CClient) GetExternalGroup2ExternalUserListRelationsV1(
	name, source storage.ResourceNameT) (*storage.ExternalGroup2ExternalUserListRelationsV1, error) {
	var _url = fmt.Sprintf(externalGroupV12ExternalUsersPath, c.urlPrefix, source, name)

	response, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	// Read response.
	var result storage.ExternalGroup2ExternalUserListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// checkExternalGroup2ExternalUserListRelationsV1sFound checks that for all
// ExternalGroupV1 names there are related relations.
func checkExternalGroup2ExternalUserListRelationsV1sFound( //nolint:dupl
	names []storage.ResourceNameT, source storage.ResourceNameT,
	rels []storage.ExternalGroup2ExternalUserListRelationsV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range rels {
			if n == rels[i].Data.ExternalGroup {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: ExternalGroup2ExternalUserListRelationsV1s for groups %q in source %s not found",
			ErrResourceNotFound, strings.Join(notFound, ", "), source)
	}

	return nil
}

// GetExternalGroup2ExternalUserListRelationsV1s loads and returns all Relations
// for given ExternalGroup Names and Source.
func (c *CClient) GetExternalGroup2ExternalUserListRelationsV1s(
	names []storage.ResourceNameT, source storage.ResourceNameT) ([]storage.ExternalGroup2ExternalUserListRelationsV1, error) {
	var result = make([]storage.ExternalGroup2ExternalUserListRelationsV1, 0, len(names))

	for _, n := range names {
		r, err := c.GetExternalGroup2ExternalUserListRelationsV1(n, source)
		if err != nil {
			return nil, err
		}

		result = append(result, *r)
	}

	if err := checkExternalGroup2ExternalUserListRelationsV1sFound(names, source, result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateExternalGroup2ExternalUserListV1Relations creates given Relationships on
// API Server.
func (c *CClient) CreateExternalGroup2ExternalUserListV1Relations( // nolint:dupl
	relations *storage.ExternalGroup2ExternalUserListRelationsV1,
	loadCreated bool) (*storage.ExternalGroup2ExternalUserListRelationsV1, error) {
	var _url = fmt.Sprintf(externalGroupV12ExternalUsersPath+"?get_operation_result=%t",
		c.urlPrefix, relations.Data.ExternalUsersGroupsSource,
		relations.Data.ExternalGroup, loadCreated)

	response, err := c.putRead(_url, relations)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	// Read response.
	var result storage.ExternalGroup2ExternalUserListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteExternalGroup2ExternalUserListV1Relations deletes given relationships on API Server.
func (c *CClient) DeleteExternalGroup2ExternalUserListV1Relations(
	relations *storage.ExternalGroup2ExternalUserListRelationsV1) error {
	var _url = fmt.Sprintf(externalGroupV12ExternalUsersPath,
		c.urlPrefix, relations.Data.ExternalUsersGroupsSource, relations.Data.ExternalGroup)

	if err := c.delete(_url, relations); err != nil {
		return err
	}

	return nil
}
