package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// checkAccessGroupV1sFound checks that for every name there is an AccessGroupV1.
func checkAccessGroupV1sFound(names []storage.ResourceNameT, groups []storage.AccessGroupV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range groups {
			if n == groups[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf("%w: AccessGroupV1 %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetAccessGroups returns AccessGroupV1s by names. If names is empty then returns all groups.
func (c *CClient) GetAccessGroups(names []storage.ResourceNameT) ([]storage.AccessGroupV1, error) {
	var flt = make([]storage.AccessGroupFilterV1, 0, len(names))

	for i := range names {
		flt = append(flt, storage.AccessGroupFilterV1{Name: names[i]})
	}

	_url := fmt.Sprintf(accessGroupListV1Path, c.urlPrefix)

	bts, err := c.getRead(_url, flt)
	if err != nil {
		return nil, err
	}

	var res []storage.AccessGroupV1

	if err := json.Unmarshal(bts, &res); err != nil {
		return nil, err
	}

	if err := checkAccessGroupV1sFound(names, res); err != nil {
		return nil, err
	}

	return res, nil
}

// GetAccessGroup returns one AccessGroupV1 by name.
func (c *CClient) GetAccessGroup(name storage.ResourceNameT) (*storage.AccessGroupV1, error) {
	_url := fmt.Sprintf(accessGroupV1Path, c.urlPrefix, name)

	bts, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	var g storage.AccessGroupV1
	if err := json.Unmarshal(bts, &g); err != nil {
		return nil, err
	}

	return &g, nil
}

// CreateAccessGroups create AccessGroupV1s from given list.
func (c *CClient) CreateAccessGroups(groups []storage.AccessGroupV1, loadCreated bool) ([]storage.AccessGroupV1, error) {
	if len(groups) == 0 {
		return []storage.AccessGroupV1{}, nil
	}

	_url := fmt.Sprintf(accessGroupListV1Path+"?get_operation_result=%t", c.urlPrefix, loadCreated)

	bts, err := c.postRead(_url, groups)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var res []storage.AccessGroupV1

	if err := json.Unmarshal(bts, &res); err != nil {
		return nil, err
	}

	return res, nil
}

// CreateAccessGroup creates one AccessGroupV1.
func (c *CClient) CreateAccessGroup(group *storage.AccessGroupV1, loadCreated bool) (*storage.AccessGroupV1, error) {
	_url := fmt.Sprintf(accessGroupV1Path+"?get_operation_result=%t", c.urlPrefix, group.Metadata.Name, loadCreated)

	bts, err := c.postRead(_url, group)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var res storage.AccessGroupV1

	if err := json.Unmarshal(bts, &res); err != nil {
		return nil, err
	}

	return &res, nil
}

// UpdateAccessGroup updates an existing AccessGroupV1 by its name and patch.
func (c *CClient) UpdateAccessGroup(name storage.ResourceNameT, patch *storage.AccessGroupV1, loadUpdated bool) (
	*storage.AccessGroupV1, error) {
	_url := fmt.Sprintf(accessGroupV1Path+"?get_operation_result=%t",
		c.urlPrefix, name, loadUpdated)

	bts, err := c.patchRead(_url, patch)
	if err != nil {
		return nil, err
	}

	if !loadUpdated {
		return nil, nil
	}

	var res storage.AccessGroupV1
	if err := json.Unmarshal(bts, &res); err != nil {
		return nil, err
	}

	return &res, nil
}

// DeleteAccessGroup deletes one AccessGroupV1.
func (c *CClient) DeleteAccessGroup(group *storage.AccessGroupV1) error {
	_url := fmt.Sprintf(accessGroupV1Path, c.urlPrefix, group.Metadata.Name)

	if err := c.delete(_url, group); err != nil {
		return err
	}

	return nil
}
