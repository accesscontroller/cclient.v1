module gitlab.com/accesscontroller/cclient.v1

go 1.14

require (
	github.com/bxcodec/faker/v3 v3.5.1-0.20200626122352-b0be7860cae4
	github.com/stretchr/testify v1.6.1
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20201030165051-959181aa057a
)

replace gitlab.com/accesscontroller/accesscontroller/controller/storage => ../controller/storage
