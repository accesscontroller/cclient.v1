package cclient

import (
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroup2ExternalGroupListRelationsSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestGetSuccess() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		relations storage.AccessGroup2ExternalGroupListRelationsV1
	}{
		{
			accessGroup: "for get external group relations access group 8",
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for get external group relations access group 8",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for get external group relations access group 8",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						// Sorted names in slices are required for comparison.
						"for access groups to external groups relations source 9": {
							"group 15 at source 9 binded to access group 8",
							"group 16 at source 9 binded to access group 8",
						},
						"for access groups to external groups relations source 10": {
							"group 17 at source 10 binded to access group 8",
							"group 18 at source 10 binded to access group 8",
						},
					},
				},
			},
		},
		{
			accessGroup: "for get empty external group relations access group 10",
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for get empty external group relations access group 10",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for get empty external group relations access group 10",
					Relations:   make(map[storage.ResourceNameT][]storage.ResourceNameT),
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetAccessGroup2ExternalGroupListRelationsV1(test.accessGroup)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort Relations.
		for _, v := range got.Data.Relations {
			sort.Slice(v, func(i, j int) bool { return v[i] < v[j] }) // nolint:scopelint
		}

		ts.Equal(test.relations, *got, "Unexpected value")
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestGetNotFound() {
	var tests = []struct {
		accessGroup storage.ResourceNameT

		expectedError error
	}{
		{
			accessGroup: "not found access group",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetAccessGroup2ExternalGroupListRelationsV1(test.accessGroup)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestDeleteSuccess() {
	var tests = []struct {
		relationsToDelete  storage.AccessGroup2ExternalGroupListRelationsV1
		relationsRemaining storage.AccessGroup2ExternalGroupListRelationsV1
	}{
		{
			relationsToDelete: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "delete request",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for delete external group relations access group 9",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 9": {
							"for binding delete group 21 at source 9 binded to access group 9",
						},
						"for access groups to external groups relations source 10": {
							"for binding delete group 19 at source 10 binded to access group 9",
						},
					},
				},
			},
			relationsRemaining: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for delete external group relations access group 9",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for delete external group relations access group 9",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 10": {
							"for binding delete group 20 at source 10 binded to access group 9",
						},
						"for access groups to external groups relations source 9": {
							"for binding delete group 22 at source 9 binded to access group 9",
						},
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteAccessGroup2ExternalGroupListV1Relations(&test.relationsToDelete)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check remaining relationships.
		apiRels, err := ts.client.GetAccessGroup2ExternalGroupListRelationsV1(test.relationsToDelete.Data.AccessGroup)
		if err != nil {
			ts.FailNowf("Can not get AccessGroup2ExternalGroupListV1Relations from API", "AccessGroup: %s, error: %s",
				test.relationsToDelete.Data.AccessGroup, err)
		}

		if apiRels == nil {
			ts.FailNow("nil got from API server")
		}

		if apiRels != nil {
			ts.Equal(test.relationsRemaining, *apiRels, "Unexpected remaining relations")
		}
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestDeleteAccessGroupNotFound() {
	var tests = []struct {
		relationsToDelete storage.AccessGroup2ExternalGroupListRelationsV1

		expectedError error
	}{
		{
			relationsToDelete: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "delete request",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "not found access group",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 9": {
							"for binding delete group 21 at source 9 binded to access group 9",
						},
						"for access groups to external groups relations source 10": {
							"for binding delete group 19 at source 10 binded to access group 9",
						},
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteAccessGroup2ExternalGroupListV1Relations(&test.relationsToDelete)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestCreateSuccess() {
	var tests = []struct {
		getCreated bool

		relations         storage.AccessGroup2ExternalGroupListRelationsV1
		expectedRelations storage.AccessGroup2ExternalGroupListRelationsV1
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for put external group relations access group 11",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for put external group relations access group 11",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 9": {
							"for binding group 23 at source 9 to access group 11",
						},
						"for access groups to external groups relations source 10": {
							"for binding group 24 at source 10 to access group 11",
						},
					},
				},
			},
			expectedRelations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for put external group relations access group 11",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for put external group relations access group 11",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 9": {
							"already binded group 25 at source 9 to access group 11",
							"for binding group 23 at source 9 to access group 11",
						},
						"for access groups to external groups relations source 10": {
							"for binding group 24 at source 10 to access group 11",
						},
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroup2ExternalGroupListV1Relations(&test.relations, test.getCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load Relations from API.
		apiRels, err := ts.client.GetAccessGroup2ExternalGroupListRelationsV1(test.relations.Data.AccessGroup)
		if err != nil {
			ts.FailNowf("Can not get AccessGroup relations", "AccessGroup: %s, error: %s",
				test.relations.Data.AccessGroup, err)
		}

		// Sort Relations.
		for _, v := range test.relations.Data.Relations {
			sort.Slice(v, func(i, j int) bool { return v[i] < v[j] }) // nolint:scopelint
		}

		for _, v := range apiRels.Data.Relations {
			sort.Slice(v, func(i, j int) bool { return v[i] < v[j] }) // nolint:scopelint
		}

		ts.Equal(test.expectedRelations, *apiRels, "Unexpected relations after update")

		if !test.getCreated {
			continue
		}

		ts.Equal(test.expectedRelations, *got, "Unexpected returned result")
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestCreateAccessGroupNotFound() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		expectedError error
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for put external group relations access group 11",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "not found access group", // No such an AccessGroup.
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 9": {
							"for binding group 23 at source 9 to access group 11",
						},
						"for access groups to external groups relations source 10": {
							"for binding group 24 at source 10 to access group 11",
						},
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroup2ExternalGroupListV1Relations(&test.relations, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroup2ExternalGroupListRelationsSuite) TestCreateExternalGroupNotFound() {
	var tests = []struct {
		relations storage.AccessGroup2ExternalGroupListRelationsV1

		expectedError error
	}{
		{
			relations: storage.AccessGroup2ExternalGroupListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroup2ExternalGroupListV1RelationsKind,
					Name:       "for put external group relations access group 11",
				},
				Data: storage.AccessGroup2ExternalGroupListRelationsDataV1{
					AccessGroup: "for put external group relations access group 11",
					Relations: map[storage.ResourceNameT][]storage.ResourceNameT{
						"for access groups to external groups relations source 9": {
							"not found external group", // Not Found.
							"for binding group 23 at source 9 to access group 11",
						},
						"for access groups to external groups relations source 10": {
							"for binding group 24 at source 10 to access group 11",
						},
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"api_version\":\"v1\",\"error_kind\":\"ErrRelatedResourcesNotFound\",\"kind\":\"ExternalGroup\",\"names\":[\"not found external group\"]}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroup2ExternalGroupListV1Relations(&test.relations, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestAccessGroup2ExternalGroupListRelations(t *testing.T) {
	suite.Run(t, &TestAccessGroup2ExternalGroupListRelationsSuite{})
}
