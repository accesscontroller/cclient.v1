package cclient

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetExternalUser loads an existing ExternalUserV1 from API server.
func (c *CClient) GetExternalUser(name, source storage.ResourceNameT) (*storage.ExternalUserV1, error) {
	// Get.
	url := fmt.Sprintf(externalUserV1Path, c.urlPrefix, source, name)

	body, err := c.getRead(url, nil)
	if err != nil {
		return nil, err
	}

	// Parse.
	var us storage.ExternalUserV1

	if err := json.Unmarshal(body, &us); err != nil {
		return nil, err
	}

	return &us, nil
}

// GetExternalUsers loads ExternalUserV1s from API Server filtered by given Filter.
func (c *CClient) GetExternalUsers(flt []storage.ExternalUserFilterV1,
	usersGroupsSource storage.ResourceNameT) ([]storage.ExternalUserV1, error) {
	// Request.
	resp, err := c.getRead(fmt.Sprintf(externalUserListV1Path, c.urlPrefix, usersGroupsSource), flt)
	if err != nil {
		return nil, err
	}

	// Parse.
	var users []storage.ExternalUserV1

	if err := json.Unmarshal(resp, &users); err != nil {
		return nil, err
	}

	return users, nil
}

// checkExternalUserV1sFound checks that for all the names there are ExternalUserV1s.
func checkExternalUserV1sFound(
	names []storage.ResourceNameT, source storage.ResourceNameT, users []storage.ExternalUserV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range users {
			if n == users[i].Metadata.Name {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf(
			"%w: ExternalUserV1s %q in source %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "), source)
	}

	return nil
}

// GetExternalUsersByNames loads ExternalUserV1s using their names.
func (c *CClient) GetExternalUsersByNames(names []storage.ResourceNameT, source storage.ResourceNameT) ([]storage.ExternalUserV1, error) {
	// Filters.
	var filters = make([]storage.ExternalUserFilterV1, 0, len(names))

	for _, n := range names {
		filters = append(filters, storage.ExternalUserFilterV1{
			Name: n,
		})
	}

	// Load.
	users, err := c.GetExternalUsers(filters, source)
	if err != nil {
		return nil, err
	}

	// Check that all Users are found.
	if err := checkExternalUserV1sFound(names, source, users); err != nil {
		return nil, err
	}

	return users, nil
}

// CreateExternalUsers creates a new ExternalUserV1s from given slice.
// All given users must be from the same ExternalUsersGroupsSourceV1.
func (c *CClient) CreateExternalUsers(users []storage.ExternalUserV1, loadCreated bool) ([]storage.ExternalUserV1, error) { // nolint:dupl
	if len(users) == 0 {
		if loadCreated {
			return []storage.ExternalUserV1{}, nil
		}

		return nil, nil
	}

	// Send.
	response, err := c.postRead(fmt.Sprintf(externalUserListV1Path+"?get_operation_result=%t", c.urlPrefix,
		users[0].Metadata.ExternalSource.SourceName, loadCreated), users)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result []storage.ExternalUserV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateExternalUser creates a new ExternalUserV1.
func (c *CClient) CreateExternalUser(us *storage.ExternalUserV1, loadCreated bool) (*storage.ExternalUserV1, error) { // nolint:dupl
	// Send.
	response, err := c.postRead(fmt.Sprintf(externalUserV1Path+"?get_operation_result=%t", c.urlPrefix,
		us.Metadata.ExternalSource.SourceName, us.Metadata.Name, loadCreated), us)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	var result storage.ExternalUserV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// UpdateExternalUser updates an existing ExternalUserV1.
func (c *CClient) UpdateExternalUser(oldName storage.ResourceNameT, // nolint:dupl
	patch *storage.ExternalUserV1, loadUpdated bool) (*storage.ExternalUserV1, error) {
	// Send.
	response, err := c.patchRead(fmt.Sprintf(externalUserV1Path+"?get_operation_result=%t", c.urlPrefix,
		patch.Metadata.ExternalSource.SourceName, oldName, loadUpdated), patch)
	if err != nil {
		return nil, err
	}

	if !loadUpdated {
		return nil, nil
	}

	var result storage.ExternalUserV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// UpdateExternalUsers updates all given ExternalUserV1s.
func (c *CClient) UpdateExternalUsers(users []storage.ExternalUserV1,
	loadUpdated bool) ([]storage.ExternalUserV1, error) {
	var result []storage.ExternalUserV1

	if loadUpdated {
		result = make([]storage.ExternalUserV1, 0, len(users))
	}

	for i := range users {
		pu := &users[i]

		updU, err := c.UpdateExternalUser(pu.Metadata.Name, pu, loadUpdated)
		if err != nil {
			return nil, err
		}

		if loadUpdated {
			result = append(result, *updU)
		}
	}

	return result, nil
}

// DeleteExternalUsers deletes given ExternalUserV1s from API Server.
// All given users must be from the same ExternalUsersGroupsSourceV1.
func (c *CClient) DeleteExternalUsers(users []storage.ExternalUserV1) error {
	if len(users) == 0 {
		return nil
	}

	// Send.
	if err := c.delete(fmt.Sprintf(externalUserListV1Path,
		c.urlPrefix, users[0].Metadata.ExternalSource.SourceName), users); err != nil {
		return err
	}

	return nil
}

// DeleteExternalUser deletes an existing ExternalUserV1 from API Server.
func (c *CClient) DeleteExternalUser(u *storage.ExternalUserV1) error {
	if err := c.delete(fmt.Sprintf(
		externalUserV1Path, c.urlPrefix, u.Metadata.ExternalSource.SourceName, u.Metadata.Name), u); err != nil {
		return err
	}

	return nil
}
