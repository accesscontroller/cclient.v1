package cclient // nolint:dupl

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// CreateAccessGroup2ExternalGroupListV1Relations creates given relations on API Server.
func (c *CClient) CreateAccessGroup2ExternalGroupListV1Relations(
	relations *storage.AccessGroup2ExternalGroupListRelationsV1,
	loadCreated bool) (*storage.AccessGroup2ExternalGroupListRelationsV1, error) {
	var _url = fmt.Sprintf(accessGroupV12ExternalGroupsPath+"?get_operation_result=%t",
		c.urlPrefix, relations.Data.AccessGroup, loadCreated)

	response, err := c.putRead(_url, relations)
	if err != nil {
		return nil, err
	}

	if !loadCreated {
		return nil, nil
	}

	// Load Result.
	var result storage.AccessGroup2ExternalGroupListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// GetAccessGroup2ExternalGroupListRelationsV1 loads relations from API Server for given AccessGroupV1.
func (c *CClient) GetAccessGroup2ExternalGroupListRelationsV1(
	accessGroup storage.ResourceNameT) (*storage.AccessGroup2ExternalGroupListRelationsV1, error) {
	var _url = fmt.Sprintf(accessGroupV12ExternalGroupsPath,
		c.urlPrefix, accessGroup)

	response, err := c.getRead(_url, nil)
	if err != nil {
		return nil, err
	}

	// Load Result.
	var result storage.AccessGroup2ExternalGroupListRelationsV1

	if err := json.Unmarshal(response, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

// checkAccessGroup2ExternalGroupListRelationsV1sFound checks that all the names are in groups.
func checkAccessGroup2ExternalGroupListRelationsV1sFound(
	names []storage.ResourceNameT,
	groups []storage.AccessGroup2ExternalGroupListRelationsV1) error {
	var notFound = make([]string, 0)

	for _, n := range names {
		var found bool

		for i := range groups {
			if n == groups[i].Data.AccessGroup {
				found = true

				break
			}
		}

		if !found {
			notFound = append(notFound, string(n))
		}
	}

	if len(notFound) != 0 {
		return fmt.Errorf("%w: AccessGroup2ExternalGroupListRelationsV1 %q not found",
			ErrResourceNotFound, strings.Join(notFound, ", "))
	}

	return nil
}

// GetAccessGroup2ExternalGroupListRelationsV1s loads relations for all given AccessGroupV1s.
func (c *CClient) GetAccessGroup2ExternalGroupListRelationsV1s(
	accessGroups []storage.ResourceNameT) ([]storage.AccessGroup2ExternalGroupListRelationsV1, error) {
	var result = make([]storage.AccessGroup2ExternalGroupListRelationsV1, 0, len(accessGroups))

	for _, n := range accessGroups {
		r, err := c.GetAccessGroup2ExternalGroupListRelationsV1(n)
		if err != nil {
			return nil, err
		}

		result = append(result, *r)
	}

	if err := checkAccessGroup2ExternalGroupListRelationsV1sFound(accessGroups, result); err != nil {
		return nil, err
	}

	return result, nil
}

// DeleteAccessGroup2ExternalGroupListV1Relations deletes relations for given AccessGroupV1.
func (c *CClient) DeleteAccessGroup2ExternalGroupListV1Relations(
	relations *storage.AccessGroup2ExternalGroupListRelationsV1) error {
	var _url = fmt.Sprintf(accessGroupV12ExternalGroupsPath,
		c.urlPrefix, relations.Data.AccessGroup)

	if err := c.delete(_url, relations); err != nil {
		return err
	}

	return nil
}
