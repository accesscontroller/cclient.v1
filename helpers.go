package cclient

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

const (
	serverErrResourceNotFound         string = "ErrResourceNotFound"
	serverErrResourceAlreadyExists    string = "ErrResourceAlreadyExists"
	serverErrRelatedResourcesNotFound string = "ErrRelatedResourcesNotFound"
	serverErrETagDoesNotMatch         string = "ErrETagDoesNotMatch"
)

type serverError struct {
	// ErrorKind stores error_kind server response field.
	ErrorKind string `json:"error_kind,omitempty" yaml:"error_kind,omitempty"`

	// Metadata stores storage.MetadataV1 server response field (used in storage.ErrResourceNotFound).
	Metadata storage.MetadataV1 `json:"metadata,omitempty" yaml:"metadata,omitempty"`

	// Conflicts stores conflicting resources (used in storage.ErrResourceAlreadyExists).
	Conflicts []interface{} `json:"conflicts,omitempty" yaml:"conflicts,omitempty"`

	// ErrRelatedResourcesNotFound fields.
	// Kind contains resource kind.
	Kind storage.ResourceKindT `json:"kind,omitempty" yaml:"kind,omitempty"`
	// Names contains names of the resources.
	Names []storage.ResourceNameT `json:"names,omitempty" yaml:"names,omitempty"`
	// APIVersion contains API Version.
	APIVersion storage.APIVersionT `json:"api_version,omitempty" yaml:"api_version,omitempty"`
	// Resources contains full resources representation.
	Resources []interface{} `json:"resources,omitempty" yaml:"resources,omitempty"`

	// ErrETagDoesNotMatch fields.
	// Expected contains expected ETag.
	Expected storage.ETagT `json:"expected,omitempty" yaml:"expected,omitempty"`

	// Given contains given ETag.
	Given storage.ETagT `json:"given,omitempty" yaml:"given,omitempty"`
}

func setAPIKeyHeader(req *http.Request, key string) *http.Request {
	if key == "" {
		return req
	}

	req.Header.Set(APIKeyHeader, "Bearer "+key)

	return req
}

// ParseStorageError tries to parse server Error message to storage.Err* error.
func ParseStorageError(e *ErrServerResponseStatus, payload []byte) *ErrServerResponseStatus {
	var errData serverError

	if err := json.Unmarshal(payload, &errData); err != nil {
		return e
	}

	switch errData.ErrorKind {
	case serverErrResourceNotFound:
		e.ServerError = storage.NewErrResourceNotFound(&errData.Metadata)
	case serverErrResourceAlreadyExists:
		e.ServerError = storage.NewErrResourceAlreadyExists(errData.Conflicts)
	case serverErrRelatedResourcesNotFound:
		e.ServerError = storage.NewErrRelatedResourcesNotFound(
			errData.APIVersion,
			errData.Kind,
			errData.Names,
			errData.Resources,
		)
	case serverErrETagDoesNotMatch:
		e.ServerError = storage.NewErrETagDoesNotMatch(errData.Expected, errData.Given)
	}

	return e
}

// getRead loads all data from given URL from server.
func (c *CClient) getRead(_url string, v interface{}) ([]byte, error) {
	var reqBody []byte
	if v == nil {
		reqBody = nil
	} else {
		var err error
		reqBody, err = json.Marshal(v)
		if err != nil {
			return nil, err
		}
	}

	// Make request.
	req, err := http.NewRequest(http.MethodGet, _url, bytes.NewReader(reqBody))
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", exchangeContentType)

	// Add APIKey.
	req = setAPIKeyHeader(req, c.apiKey)

	// Load.
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	// Read.
	pl, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Check.
	if resp.StatusCode != http.StatusOK {
		return nil, &ErrServerResponseStatus{
			GotStatusCode:      resp.StatusCode,
			ExpectedStatusCode: http.StatusOK,
			Message:            string(pl),
		}
	}

	// Check Mime type.
	if resp.Header.Get("content-type") != exchangeContentType {
		return nil, &ErrServerContentType{got: resp.Header.Get("content-type"), expected: exchangeContentType}
	}

	return pl, nil
}

func (c *CClient) postRead(_url string, v interface{}) ([]byte, error) { // nolint:dupl
	// Serialize
	bts, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	// Request
	req, err := http.NewRequest(http.MethodPost, _url, bytes.NewReader(bts))
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", exchangeContentType)

	// Add APIKey.
	req = setAPIKeyHeader(req, c.apiKey)

	// Send
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	// Read response.
	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Check status
	if resp.StatusCode != http.StatusCreated {
		return nil, &ErrServerResponseStatus{
			GotStatusCode:      resp.StatusCode,
			ExpectedStatusCode: http.StatusCreated,
			Message:            string(res),
		}
	}

	// Check content type
	if resp.Header.Get("content-type") != exchangeContentType {
		return nil, &ErrServerContentType{expected: exchangeContentType, got: resp.Header.Get("content-type")}
	}

	return res, nil
}

func (c *CClient) patchRead(_url string, v interface{}) ([]byte, error) { // nolint:dupl
	// Serialize
	bts, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	// Request
	req, err := http.NewRequest(http.MethodPatch, _url, bytes.NewReader(bts))
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", exchangeContentType)

	// Add APIKey.
	req = setAPIKeyHeader(req, c.apiKey)

	// Send
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Read response
	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Check status
	if resp.StatusCode != http.StatusOK {
		return nil, &ErrServerResponseStatus{
			GotStatusCode:      resp.StatusCode,
			ExpectedStatusCode: http.StatusOK,
			Message:            string(res),
		}
	}

	// Check content type
	if resp.Header.Get("content-type") != exchangeContentType {
		return nil, &ErrServerContentType{expected: exchangeContentType, got: resp.Header.Get("content-type")}
	}

	return res, nil
}

func (c *CClient) delete(_url string, v interface{}) error {
	// Serialize.
	bts, err := json.Marshal(v)
	if err != nil {
		return err
	}

	// Request.
	req, err := http.NewRequest(http.MethodDelete, _url, bytes.NewReader(bts))
	if err != nil {
		return err
	}

	req.Header.Set("content-type", exchangeContentType)

	// Add APIKey.
	req = setAPIKeyHeader(req, c.apiKey)

	// Send.
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	respB, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// Status check.
	// Success.
	if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusNoContent {
		return nil
	}

	return &ErrServerResponseStatus{
		GotStatusCode:      resp.StatusCode,
		ExpectedStatusCode: http.StatusNoContent,
		Message:            string(respB),
	}
}

func (c *CClient) putRead(_url string, v interface{}) ([]byte, error) {
	// Serialize.
	bts, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	// Request.
	req, err := http.NewRequest(http.MethodPut, _url, bytes.NewReader(bts))
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", exchangeContentType)

	// Add APIKey.
	req = setAPIKeyHeader(req, c.apiKey)

	// Send.
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	// Read response.
	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Check status.
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		return nil, &ErrServerResponseStatus{
			GotStatusCode:      resp.StatusCode,
			ExpectedStatusCode: http.StatusOK,
			Message:            string(res),
		}
	}

	// Check content type.
	if resp.Header.Get("content-type") != exchangeContentType {
		return nil, &ErrServerContentType{expected: exchangeContentType, got: resp.Header.Get("content-type")}
	}

	return res, nil
}
