-- Access Groups
TRUNCATE access_groups RESTART IDENTITY CASCADE;

INSERT INTO
    access_groups (
        id,
        name,
        e_tag,
        default_policy,
        order_in_access_rules_list,
        total_group_speed_limit_bps,
        user_group_speed_limit_bps,
        extra_access_deny_message
    )
VALUES
    (
        101,
        'access group 1',
        1,
        1,
        101,
        100000001,
        1000001,
        'access group 1 access denied'
    ),
    (
        102,
        'access group 2',
        2,
        2,
        102,
        100000002,
        1000002,
        'access group 2 access denied'
    ),
    (
        103,
        'access group 3',
        3,
        2,
        103,
        100000003,
        1000003,
        'access group 3 access denied'
    ),
    (
        104,
        'access group 4',
        4,
        1,
        104,
        100000004,
        1000004,
        'access group 4 access denied'
    ),
    (
        105,
        'for delete access group 5',
        3,
        2,
        105,
        100000003,
        1000003,
        'access group 5 access denied'
    ),
    (
        106,
        'for update access group 6',
        6,
        1,
        106,
        100000004,
        1000004,
        'access group 6 access denied'
    ),
    (
        176,
        'for update access group 7',
        7,
        1,
        107,
        100000004,
        1000004,
        'access group 7 access denied'
    ),
    (
        177,
        'for get external group relations access group 8',
        8,
        1,
        108,
        100000004,
        1000004,
        'access group 8 access denied'
    ),
    (
        179,
        'for delete external group relations access group 9',
        9,
        1,
        110,
        100000004,
        1000004,
        'access group 9 access denied'
    ),
    (
        180,
        'for get empty external group relations access group 10',
        8,
        1,
        111,
        100000004,
        1000004,
        'access group 10 access denied'
    ),
    (
        181,
        'for put external group relations access group 11',
        9,
        1,
        112,
        100000004,
        1000004,
        'access group 11 access denied'
    ),
    (
        182,
        'for put external group relations access group 12',
        9,
        1,
        113,
        100000004,
        1000004,
        'access group 12 access denied'
    ),
    (
        183,
        'for get web resource category relations access group 13',
        9,
        1,
        183,
        100000004,
        1000004,
        'access group 13 access denied'
    ),
    (
        184,
        'for bind web resource category relations access group 14',
        9,
        1,
        184,
        100000004,
        1000004,
        'access group 14 access denied'
    ),
    (
        185,
        'for delete bind web resource category relations access group 15',
        9,
        1,
        185,
        100000004,
        1000004,
        'access group 15 access denied'
    );

-- Web Resource Categories.
TRUNCATE web_resource_categories RESTART IDENTITY CASCADE;

INSERT INTO
    web_resource_categories (
        id,
        name,
        e_tag,
        description
    )
VALUES
    (
        101,
        'web resource category 1 binded to for get web resource category relations access group 13',
        101,
        'description for web resource category 1'
    ),
    (
        102,
        'web resource category 2 binded to for get web resource category relations access group 13',
        102,
        'description for web resource category 2'
    ),
    (
        103,
        'for bind web resource category 3 to for get web resource category relations access group 14',
        103,
        'description for web resource category 3'
    ),
    (
        104,
        'for bind web resource category 4 to for get web resource category relations access group 14',
        104,
        'description for web resource category 4'
    ),
    (
        105,
        'already binded web resource category 5 to for get web resource category relations access group 14',
        105,
        'description for web resource category 5'
    ),
    (
        106,
        'for delete web resource category 6 to for get web resource category relations access group 15',
        106,
        'description for web resource category 6'
    ),
    (
        107,
        'for delete web resource category 7 to for get web resource category relations access group 15',
        107,
        'description for web resource category 7'
    ),
    (
        108,
        'already binded web resource category 8 to for get web resource category relations access group 15',
        108,
        'description for web resource category 8'
    ),
    (
        109,
        'web resource category 109',
        109,
        'web resource category 109 description'
    ),
    (
        110,
        'web resource category 110',
        110,
        'web resource category 110 description'
    ),
    (
        111,
        'web resource category 111',
        111,
        'web resource category 111 description'
    ),
    (
        112,
        'for update web resource category 112',
        112,
        'web resource category 112 description'
    ),
    (
        113,
        'for update web resource category 113',
        113,
        'web resource category 113 description'
    ),
    (
        114,
        'for delete web resource category 114',
        114,
        'web resource category 114 description'
    ),
    (
        115,
        'web resource category 115',
        115,
        'web resource category 115 description'
    ),
    (
        116,
        'web resource category 116',
        116,
        'web resource category 116 description'
    ),
    (
        117,
        'for web resources create web resource category 117',
        117,
        'web resource category 117 description'
    ),
    (
        118,
        'for web resources create web resource category 118',
        118,
        'web resource category 118 description'
    ),
    (
        119,
        'for web resources create web resource category 119',
        119,
        'web resource category 119 description'
    ),
    (
        120,
        'for web resources update web resource category 120',
        120,
        'web resource category 120 description'
    ),
    (
        121,
        'for web resources delete web resource category 121',
        121,
        'web resource category 121 description'
    );

-- Web Resource Categories to Access Groups.
TRUNCATE web_resource_categories_to_access_groups RESTART IDENTITY CASCADE;

INSERT INTO
    web_resource_categories_to_access_groups (
        web_resource_category_id,
        access_group_id
    )
VALUES
    (101, 183),
    (102, 183),
    (105, 184),
    (106, 185),
    (107, 185),
    (108, 185);

-- External Sources.
TRUNCATE external_users_groups_sources RESTART IDENTITY CASCADE;

INSERT INTO
    external_users_groups_sources (
        id,
        name,
        e_tag,
        get_mode,
        type,
        users_get_settings,
        groups_get_settings,
        poll_interval,
        groups_to_load
    )
VALUES
    (
        101,
        'source 1',
        1,
        'poll',
        'LDAP',
        '{"key1":"value1"}',
        '{"group sett key1":"group sett val1"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        102,
        'source 2',
        2,
        'poll',
        'LDAP',
        '{"key2":"value2"}',
        '{"group sett key1":"group sett val1"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        103,
        'for groups creation source 3',
        3,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        104,
        'for groups creation source 4',
        4,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        105,
        'for groups update source 5',
        3,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        106,
        'for groups creation source 6',
        4,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        107,
        'for groups deletion source 7',
        4,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        108,
        'for users creation source 8',
        4,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        109,
        'for access groups to external groups relations source 9',
        4,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        110,
        'for access groups to external groups relations source 10',
        4,
        'poll',
        'MSAD',
        '{"key2":"value2"}',
        '{"group sett key2":"group sett val2"}',
        3600,
        '{"group1", "group2"}'
    ),
    (
        111,
        'source 11',
        111,
        'poll',
        'MSAD',
        '{"key11":"value11"}',
        '{"group sett key11":"group sett val11"}',
        3611,
        '{"group11", "group22"}'
    ),
    (
        112,
        'source 12',
        112,
        'passive wait',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3612,
        '{"group12", "group12"}'
    ),
    (
        113,
        'source 13',
        113,
        'passive wait',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3613,
        '{"group12", "group12"}'
    ),
    (
        114,
        'source 14',
        114,
        'poll',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3614,
        '{"group12", "group12"}'
    ),
    (
        115,
        'for deletion source 15',
        115,
        'poll',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3615,
        '{"group12", "group12"}'
    ),
    (
        116,
        'for group to users binding source 16',
        116,
        'poll',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3616,
        '{"group12", "group12"}'
    ),
    (
        117,
        'for users update source 17',
        117,
        'poll',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3617,
        '{"group12", "group12"}'
    ),
    (
        118,
        'for users delete source 18',
        118,
        'poll',
        'MSAD',
        '{"key12":"value12"}',
        '{"group sett key12":"group sett val12"}',
        3617,
        '{"group12", "group12"}'
    );

-- External Groups
TRUNCATE external_groups RESTART IDENTITY CASCADE;

INSERT INTO
    external_groups (
        id,
        name,
        e_tag,
        access_group_id,
        external_users_groups_source_id
    )
VALUES
    (
        101,
        'external group 1 at source 1',
        11,
        101,
        101
    ),
    (
        102,
        'external group 2 at source 1',
        12,
        NULL,
        101
    ),
    (
        103,
        'external group 1 at source 2',
        21,
        NULL,
        102
    ),
    (
        104,
        'external group 2 at source 2',
        22,
        103,
        102
    ),
    (
        105,
        'for update external group 3 at source 5',
        13,
        NULL,
        105
    ),
    (
        106,
        'for update external group 4 at source 5',
        14,
        105,
        105
    ),
    (
        108,
        'for update external group 5 at source 5',
        13,
        NULL,
        105
    ),
    (
        109,
        'for update external group 6 at source 5',
        14,
        105,
        105
    ),
    (
        110,
        'for update external group 7 at source 5',
        13,
        NULL,
        105
    ),
    (
        111,
        'for update external group 8 at source 5',
        14,
        105,
        105
    ),
    (
        112,
        'for update external group 9 at source 5',
        14,
        105,
        105
    ),
    (
        113,
        'for delete external group 10 at source 7',
        13,
        105,
        107
    ),
    (
        114,
        'for delete external group 11 at source 7',
        14,
        105,
        107
    ),
    (
        115,
        'for delete external group 12 at source 7',
        14,
        105,
        107
    ),
    (
        116,
        'for delete external group 13 at source 7',
        14,
        105,
        107
    ),
    (
        117,
        'for delete external group 14 at source 7',
        14,
        105,
        107
    ),
    (
        118,
        'for delete external group 15 at source 7',
        14,
        105,
        107
    ),
    (
        119,
        'group 15 at source 9 binded to access group 8',
        14,
        177,
        109
    ),
    (
        120,
        'group 16 at source 9 binded to access group 8',
        14,
        177,
        109
    ),
    (
        121,
        'group 17 at source 10 binded to access group 8',
        14,
        177,
        110
    ),
    (
        122,
        'group 18 at source 10 binded to access group 8',
        14,
        177,
        110
    ),
    (
        123,
        'for binding delete group 19 at source 10 binded to access group 9',
        14,
        179,
        110
    ),
    (
        124,
        'for binding delete group 20 at source 10 binded to access group 9',
        14,
        179,
        110
    ),
    (
        125,
        'for binding delete group 21 at source 9 binded to access group 9',
        14,
        179,
        109
    ),
    (
        126,
        'for binding delete group 22 at source 9 binded to access group 9',
        14,
        179,
        109
    ),
    (
        127,
        'for binding group 23 at source 9 to access group 11',
        14,
        NULL,
        109
    ),
    (
        128,
        'for binding group 24 at source 10 to access group 11',
        14,
        NULL,
        110
    ),
    (
        129,
        'already binded group 25 at source 9 to access group 11',
        14,
        181,
        109
    ),
    (
        130,
        'for binding group 26 at source 10 to access group 12',
        14,
        NULL,
        110
    ),
    (
        131,
        'for binding group 27 at source 10 to access group 12',
        14,
        NULL,
        110
    ),
    (
        132,
        'already binded group 28 at source 10 to access group 12',
        14,
        182,
        110
    ),
    (
        133,
        'already binded group 29 at source 16 to users 5, 6, 7',
        29,
        182,
        116
    ),
    (
        134,
        'group 30 at source 16 without related users',
        30,
        182,
        116
    ),
    (
        135,
        'for creating group to users relations group 31 at source 16',
        31,
        182,
        116
    ),
    (
        136,
        'for creating group to users relations group 32 at source 16',
        32,
        182,
        116
    ),
    (
        137,
        'for deleting group to users relations group 33 at source 16',
        33,
        182,
        116
    );

-- External Users.
TRUNCATE external_users RESTART IDENTITY CASCADE;

INSERT INTO
    external_users (
        id,
        e_tag,
        name,
        external_users_groups_source_id
    )
VALUES
    (
        101,
        101,
        'external user 1 at source 1',
        101
    ),
    (
        102,
        102,
        'external user 2 at source 1',
        101
    ),
    (
        103,
        103,
        'external user 1 at source 2',
        102
    ),
    (
        104,
        104,
        'external user 2 at source 2',
        102
    ),
    (
        105,
        105,
        'for update external user 1 at source 17',
        117
    ),
    (
        106,
        106,
        'for update external user 2 at source 17',
        117
    ),
    (
        107,
        107,
        'for update external user 3 at source 17',
        117
    ),
    (
        108,
        108,
        'for update external user 4 at source 17',
        117
    ),
    (
        109,
        109,
        'for delete external user 1 at source 18',
        118
    ),
    (
        110,
        110,
        'for delete external user 2 at source 18',
        118
    ),
    (
        111,
        111,
        'for delete external user 3 at source 18',
        118
    ),
    (
        112,
        112,
        'for delete external user 4 at source 18',
        118
    ),
    (
        113,
        113,
        'user 5 binded to external group 29 at source 16',
        116
    ),
    (
        114,
        114,
        'user 6 binded to external group 29 at source 16',
        116
    ),
    (
        115,
        115,
        'user 7 binded to external group 29 at source 16',
        116
    ),
    (
        116,
        116,
        'for creation group to users relations user 8 at source 16',
        116
    ),
    (
        117,
        117,
        'for creation group to users relations user 9 at source 16',
        116
    ),
    (
        118,
        118,
        'for creation group to users relations user 10 at source 16',
        116
    ),
    (
        119,
        119,
        'for creation group to users relations user 11 binded to group 32 at source 16',
        116
    ),
    (
        120,
        120,
        'for deleting group to users relations user 12 binded to group 33 at source 16',
        116
    ),
    (
        121,
        121,
        'for deleting group to users relations user 13 binded to group 33 at source 16',
        116
    ),
    (
        122,
        122,
        'for deleting group to users relations user 14 binded to group 33 at source 16',
        116
    ),
    (
        123,
        123,
        'for update external user 15 at source 17',
        117
    ),
    (
        124,
        124,
        'for update external user 16 at source 17',
        117
    ),
    (
        125,
        125,
        'for update external user 17 at source 17',
        117
    ),
    (
        126,
        126,
        'for update external user 18 at source 17',
        117
    );

-- ExternalSesionsSources.
TRUNCATE external_sessions_sources RESTART IDENTITY CASCADE;

INSERT INTO
    external_sessions_sources (
        id,
        name,
        e_tag,
        get_mode,
        type,
        settings,
        external_users_groups_source_id
    )
VALUES
    (
        101,
        'sessions source 1',
        101,
        'poll',
        'MSADEvents',
        '{"key1":"value1"}',
        101
    ),
    (
        102,
        'sessions source 2',
        102,
        'passive wait',
        'MSADEvents',
        '{"key2":"value2"}',
        102
    ),
    (
        103,
        'for deletion sessions source 3',
        103,
        'passive wait',
        'MSADEvents',
        '{"key2":"value2"}',
        103
    ),
    (
        104,
        'for deletion sessions source 4',
        104,
        'passive wait',
        'MSADEvents',
        '{"key2":"value2"}',
        104
    ),
    (
        105,
        'for update sessions source 5',
        105,
        'passive wait',
        'MSADEvents',
        '{"key2":"value2"}',
        101
    ),
    (
        106,
        'for update sessions source 6',
        106,
        'poll',
        'MSADEvents',
        '{"key2":"value2"}',
        102
    ),
    (
        107,
        'for update sessions source 7',
        107,
        'poll',
        'MSADEvents',
        '{"key2":"value2"}',
        102
    ),
    (
        108,
        'for update sessions source 8',
        108,
        'poll',
        'MSADEvents',
        '{"key2":"value2"}',
        102
    ),
    (
        109,
        'for open sessions source 9',
        109,
        'poll',
        'MSADEvents',
        '{"key2":"value2"}',
        101
    ),
    (
        110,
        'for open sessions source 10',
        110,
        'poll',
        'MSADEvents',
        '{"key2":"value2"}',
        101
    );

-- External User Sessions
TRUNCATE external_user_sessions RESTART IDENTITY CASCADE;

INSERT INTO
    external_user_sessions (
        id,
        name,
        e_tag,
        closed,
        open_timestamp,
        close_timestamp,
        host_name,
        ip_address,
        external_user_id,
        external_sessions_source_id
    )
VALUES
    (
        101,
        'external user 1 at source 1 session 1 at source 1',
        101,
        false,
        '2020-06-21T09:00:00Z',
        NULL,
        'hostname-1',
        '10.8.101.1',
        101,
        101
    ),
    (
        102,
        'external user 1 at source 1 session 2 at source 1',
        102,
        false,
        '2020-06-21T10:00:00Z',
        NULL,
        'hostname-1',
        '10.8.101.2',
        101,
        101
    ),
    (
        103,
        'closed external user 1 at source 1 session 3 at source 1',
        103,
        true,
        '2020-06-20T09:00:00Z',
        '2020-06-21T07:00:00Z',
        'hostname-1',
        '10.8.101.2',
        101,
        101
    ),
    (
        104,
        'external user 2 at source 1 session 2 at source 1',
        104,
        false,
        '2020-06-21T10:00:00Z',
        NULL,
        'hostname-2',
        '10.8.102.1',
        102,
        101
    ),
    (
        105,
        'for close external user 2 at source 1 session 2 at source 1',
        105,
        false,
        '2020-06-21T10:00:00Z',
        NULL,
        'hostname-3',
        '10.8.102.3',
        102,
        101
    ),
    (
        106,
        'for etag error external user 2 at source 1 session 3 at source 1',
        106,
        false,
        '2020-06-21T10:00:00Z',
        NULL,
        'hostname-2',
        '10.8.102.4',
        102,
        101
    ),
    (
        107,
        'for close external user 2 at source 1 session 2 at source 2',
        107,
        false,
        '2020-06-21T10:00:00Z',
        NULL,
        'hostname-3',
        '10.8.122.3',
        102,
        102
    ),
    (
        108,
        'for close external user 2 at source 1 session 3 at source 2',
        108,
        false,
        '2020-06-21T10:00:00Z',
        NULL,
        'hostname-3',
        '10.8.122.4',
        102,
        102
    );

TRUNCATE external_users_to_external_groups RESTART IDENTITY CASCADE;

INSERT INTO
    external_users_to_external_groups (
        external_user_id,
        external_group_id,
        created_at
    )
VALUES
    (
        113,
        133,
        '2020-06-24 08:01'
    ),
    (
        114,
        133,
        '2020-06-24 09:01'
    ),
    (
        115,
        133,
        '2020-06-24 10:01'
    ),
    (
        119,
        136,
        '2020-06-23 11:12'
    ),
    (
        120,
        137,
        '2020-06-23 11:12'
    ),
    (
        121,
        137,
        '2020-06-23 11:12'
    ),
    (
        122,
        137,
        '2020-06-23 11:12'
    );

-- Web Resources.
TRUNCATE web_resources RESTART IDENTITY CASCADE;

INSERT INTO
    web_resources (
        id,
        name,
        e_tag,
        description,
        web_resource_category_id
    )
VALUES
    (
        101,
        'web resource 101',
        101,
        'web resource description 101',
        115
    ),
    (
        102,
        'web resource 102',
        102,
        'web resource description 102',
        116
    ),
    (
        103,
        'for web resources update web resource 103',
        103,
        'web resource 103 description',
        120
    ),
    (
        104,
        'for web resources update web resource 104',
        104,
        'web resource 104 description',
        120
    ),
    (
        105,
        'for web resources delete web resource 105',
        105,
        'web resource 105 description',
        121
    );

TRUNCATE web_resource_ips RESTART IDENTITY CASCADE;

INSERT INTO
    web_resource_ips (
        id,
        ip,
        web_resource_id
    )
VALUES
    (
        101101,
        '10.8.101.1',
        101
    ),
    (
        101102,
        '10.8.101.2',
        101
    ),
    (
        102103,
        '10.8.102.1',
        102
    ),
    (
        101104,
        '10.8.102.2',
        102
    );

TRUNCATE web_resource_domains RESTART IDENTITY CASCADE;

INSERT INTO
    web_resource_domains (
        id,
        domain,
        web_resource_id
    )
VALUES
    (
        101101,
        'web-resource-101-domain-1.com',
        101
    ),
    (
        101102,
        'web-resource-101-domain-2.com',
        101
    ),
    (
        102101,
        'web-resource-102-domain-1.com',
        102
    );

TRUNCATE web_resource_urls RESTART IDENTITY CASCADE;

INSERT INTO
    web_resource_urls (
        id,
        url,
        web_resource_id
    )
VALUES
    (
        101101,
        'http://web-resource-101-url-1.com/url-1',
        101
    ),
    (
        101102,
        'http://web-resource-101-url-2.com/url-2',
        101
    );

-- SET sequences.
SELECT
    setval('public.external_groups_id_seq', 10001, true);

SELECT
    setval('public.access_groups_id_seq', 10001, true);

SELECT
    setval(
        'public.external_sessions_sources_id_seq',
        10001,
        true
    );

SELECT
    setval(
        'public.external_user_sessions_id_seq',
        10001,
        true
    );

SELECT
    setval(
        'public.external_users_groups_sources_id_seq',
        10001,
        true
    );

SELECT
    setval('public.external_users_id_seq', 10001, true);

SELECT
    setval(
        'public.external_users_to_external_groups_external_group_id_seq',
        10001,
        true
    );

SELECT
    setval(
        'public.external_users_to_external_groups_external_user_id_seq',
        10001,
        true
    );

SELECT
    setval(
        'public.web_resource_categories_id_seq',
        10001,
        true
    );

SELECT
    setval('public.web_resources_id_seq', 10001, true);

SELECT
    setval(
        'public.web_resource_domains_id_seq',
        10001,
        true
    );

SELECT
    setval('public.web_resource_ips_id_seq', 10001, true);

SELECT
    setval('public.web_resource_urls_id_seq', 10001, true);

SELECT
    setval(
        'public.web_resource_categories_to_access__web_resource_category_id_seq',
        10001,
        true
    );

SELECT
    setval(
        'public.web_resource_categories_to_access_groups_access_group_id_seq',
        10001,
        true
    );