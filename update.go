package cclient

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// UpdateResourcesV1 updates provided resources.
func (c *CClient) UpdateResourcesV1(loadUpdated bool, v ...interface{}) ([]interface{}, error) {
	var result []interface{}

	if loadUpdated {
		result = make([]interface{}, 0, len(v))
	}

	for i := range v {
		var (
			resp   interface{}
			reqErr error
		)

		// Type Check.
		switch tpd := v[i].(type) {
		case *storage.AccessGroupV1:
			resp, reqErr = c.UpdateAccessGroup(tpd.Metadata.Name, tpd, loadUpdated)

		case *storage.ExternalGroupV1:
			resp, reqErr = c.UpdateExternalGroup(tpd.Metadata.Name, tpd, loadUpdated)

		case *storage.ExternalSessionsSourceV1:
			resp, reqErr = c.UpdateExternalSessionsSource(tpd.Metadata.Name, tpd, loadUpdated)

		case *storage.ExternalUsersGroupsSourceV1:
			resp, reqErr = c.UpdateExternalUsersGroupsSource(tpd.Metadata.Name, tpd, loadUpdated)

		case *storage.ExternalUserV1:
			resp, reqErr = c.UpdateExternalUser(tpd.Metadata.Name, tpd, loadUpdated)

		case *storage.WebResourceCategoryV1:
			resp, reqErr = c.UpdateWebResourceCategory(tpd.Metadata.Name, tpd, loadUpdated)

		case *storage.WebResourceV1:
			resp, reqErr = c.UpdateWebResource(tpd.Metadata.Name, tpd, loadUpdated)

		default:
			return nil, fmt.Errorf("%w: %+v", ErrUnsupportedResourceType, tpd)
		}

		if reqErr != nil {
			return nil, reqErr
		}

		if loadUpdated {
			result = append(result, resp)
		}
	}

	return result, nil
}

// UpdateResourceV1 updates one resource with possible rename.
func (c *CClient) UpdateResourceV1(oldName storage.ResourceNameT, loadUpdated bool, v interface{}) (interface{}, error) {
	var (
		resp   interface{}
		reqErr error
	)

	// Type Check.
	switch tpd := v.(type) {
	case storage.AccessGroupV1:
		resp, reqErr = c.UpdateAccessGroup(oldName, &tpd, loadUpdated)
	case *storage.AccessGroupV1:
		resp, reqErr = c.UpdateAccessGroup(oldName, tpd, loadUpdated)

	case storage.ExternalGroupV1:
		resp, reqErr = c.UpdateExternalGroup(oldName, &tpd, loadUpdated)
	case *storage.ExternalGroupV1:
		resp, reqErr = c.UpdateExternalGroup(oldName, tpd, loadUpdated)

	case storage.ExternalSessionsSourceV1:
		resp, reqErr = c.UpdateExternalSessionsSource(oldName, &tpd, loadUpdated)
	case *storage.ExternalSessionsSourceV1:
		resp, reqErr = c.UpdateExternalSessionsSource(oldName, tpd, loadUpdated)

	case storage.ExternalUsersGroupsSourceV1:
		resp, reqErr = c.UpdateExternalUsersGroupsSource(oldName, &tpd, loadUpdated)
	case *storage.ExternalUsersGroupsSourceV1:
		resp, reqErr = c.UpdateExternalUsersGroupsSource(oldName, tpd, loadUpdated)

	case storage.ExternalUserV1:
		resp, reqErr = c.UpdateExternalUser(oldName, &tpd, loadUpdated)
	case *storage.ExternalUserV1:
		resp, reqErr = c.UpdateExternalUser(oldName, tpd, loadUpdated)

	case storage.WebResourceCategoryV1:
		resp, reqErr = c.UpdateWebResourceCategory(oldName, &tpd, loadUpdated)
	case *storage.WebResourceCategoryV1:
		resp, reqErr = c.UpdateWebResourceCategory(oldName, tpd, loadUpdated)

	case storage.WebResourceV1:
		resp, reqErr = c.UpdateWebResource(oldName, &tpd, loadUpdated)
	case *storage.WebResourceV1:
		resp, reqErr = c.UpdateWebResource(oldName, tpd, loadUpdated)

	default:
		return nil, fmt.Errorf("%w: %+v", ErrUnsupportedResourceType, tpd)
	}

	if reqErr != nil {
		return nil, reqErr
	}

	return resp, nil
}
