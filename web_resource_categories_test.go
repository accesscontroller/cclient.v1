package cclient

import (
	"net/http"
	"sort"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestWebResourceCategoriesSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestWebResourceCategoriesSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestWebResourceCategoriesSuite) TestGetWebResourceCategoriesSuccess() {
	var tests = []*struct {
		filter []storage.WebResourceCategoryFilterV1

		expectedCategories []storage.WebResourceCategoryV1
	}{
		{
			filter: []storage.WebResourceCategoryFilterV1{
				{
					Name: "web resource category 109",
				},
			},
			expectedCategories: []storage.WebResourceCategoryV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       109,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "web resource category 109",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "web resource category 109 description",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetWebResourceCategories(test.filter)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedCategories, got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestGetWebResourceCategorySuccess() {
	var tests = []*struct {
		name storage.ResourceNameT

		expectedCategory storage.WebResourceCategoryV1
	}{
		{
			name: "web resource category 109",

			expectedCategory: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       109,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "web resource category 109",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "web resource category 109 description",
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetWebResourceCategory(test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedCategory, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestGetWebResourceCategoryNotFound() {
	var tests = []*struct {
		name storage.ResourceNameT

		expectedError error
	}{
		{
			name: "not found web resource category",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetWebResourceCategory(test.name)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must not return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestCreateWebResourceCategoriesSuccess() {
	var tests = []*struct {
		GetCreated bool

		Categories []storage.WebResourceCategoryV1
	}{}

	// Faker.
	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not faker.FakeData", "Error: %s", err)
	}

	// Make correct Metadata.
	for _, test := range tests {
		for catI := range test.Categories {
			pc := &test.Categories[catI]

			pc.Metadata.Kind = storage.WebResourceCategoryKind
		}
	}

	// Create.
	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResourceCategories(test.Categories, test.GetCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load every created Category from API to check.
		for catI := range test.Categories {
			pc := &test.Categories[catI]

			apiCategory, err := ts.client.GetWebResourceCategory(pc.Metadata.Name)
			if err != nil {
				ts.FailNowf("Can not load a WebResourceCategory from API for check",
					"Name: %s, Error: %s",
					pc.Metadata.Name, err)
			}

			// Compare.
			ts.Equal(pc, apiCategory, "Unexpected created WebResourceCategory")
		}

		if !test.GetCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		sort.Slice(test.Categories, func(i, j int) bool {
			return test.Categories[i].Metadata.Name < test.Categories[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool {
			return got[i].Metadata.Name < got[j].Metadata.Name
		})

		ts.Equal(test.Categories, got, "Unexpected result")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestCreateWebResourceCategoriesConflict() {
	var tests = []*struct {
		Categories []storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			Categories: []storage.WebResourceCategoryV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       109,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "web resource category 109",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "web resource category 109 description",
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1102,
						Kind:       storage.WebResourceCategoryKind,
						Name:       "new web resource category 1111",
					},
					Data: storage.WebResourceCategoryDataV1{
						Description: "new web resource category 1111 description",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"web resource category 109\",\"ETag\":109,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResourceCategories(test.Categories, true)

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestCreateWebResourceCategorySuccess() {
	var tests = []*struct {
		GetCreated bool

		Category storage.WebResourceCategoryV1
	}{}

	// Faker.
	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not faker.FakeData", "Error: %s", err)
	}

	// Make correct Metadata.
	for _, test := range tests {
		test.Category.Metadata.Kind = storage.WebResourceCategoryKind
	}

	// Create.
	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResourceCategory(&test.Category, test.GetCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load created Category from API to check.
		apiCategory, err := ts.client.GetWebResourceCategory(test.Category.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load a WebResourceCategory from API for check",
				"Name: %s, Error: %s",
				test.Category.Metadata.Name, err)
		}

		// Compare.
		ts.Equal(test.Category, *apiCategory, "Unexpected created WebResourceCategory")

		if !test.GetCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Sort.
		ts.Equal(test.Category, *got, "Unexpected result")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestCreateWebResourceCategoryConflict() {
	var tests = []*struct {
		Category storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			Category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       109,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "web resource category 109",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "web resource category 109 description",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"web resource category 109\",\"ETag\":109,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResourceCategory(&test.Category, true)

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestUpdateSuccess() {
	var tests = []*struct {
		GetUpdated bool

		oldName storage.ResourceNameT

		patch storage.WebResourceCategoryV1
	}{
		{
			GetUpdated: true,
			oldName:    "for update web resource category 112",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       112,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "updated for update web resource category 112",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "updated description for update web resource category 112",
				},
			},
		},
		{
			GetUpdated: false,
			oldName:    "for update web resource category 113",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       113,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "updated for update web resource category 113",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "updated description for update web resource category 113",
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResourceCategory(test.oldName, &test.patch, test.GetUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Correct ETag increment.
		test.patch.Metadata.ETag++

		// Load a Category from API for a check.
		apiCategory, err := ts.client.GetWebResourceCategory(test.patch.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load a WebResourceCategory from API for a check",
				"Name: %s, Error: %s", test.patch.Metadata.Name, err)
		}

		ts.Equal(test.patch, *apiCategory, "Unexpected WebResourceCategory got from API")

		if !test.GetUpdated {
			continue
		}

		// Check Result.
		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.patch, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestUpdateErrorNotFound() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			oldName: "not found Web Resource Category",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       113,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "updated for update web resource category 113",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "updated description for update web resource category 113",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"not found Web Resource Category\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResourceCategory(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestUpdateErrorETag() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			oldName: "web resource category 111",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1113, // Incorrect ETag value.
					Kind:       storage.WebResourceCategoryKind,
					Name:       "updated for update web resource category 111",
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "updated description for update web resource category 111",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":111,\"given\":1113}\n",
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResourceCategory(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestUpdateErrorConflict() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			oldName: "web resource category 111",
			patch: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       111,
					Kind:       storage.WebResourceCategoryKind,
					Name:       "web resource category 110", // Name conflict
				},
				Data: storage.WebResourceCategoryDataV1{
					Description: "updated description for update web resource category 111",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"web resource category 110\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResourceCategory(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestDeleteSuccess() {
	var tests = []*struct {
		category storage.WebResourceCategoryV1
	}{
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Name:       "for delete web resource category 114",
					ETag:       114,
					Kind:       storage.WebResourceCategoryKind,
				},
				Data: storage.WebResourceCategoryDataV1{},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteWebResourceCategory(&test.category)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the Category was deleted.
		_, err := ts.client.GetWebResourceCategory(test.category.Metadata.Name)
		if err == nil {
			ts.FailNowf("Must return Web Resource Category Not Found error, got nil",
				"Name: %s, Error: %s", test.category.Metadata.Name, err)
		}
	}
}

func (ts *TestWebResourceCategoriesSuite) TestDeleteErrorNotFound() {
	var tests = []*struct {
		category storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Name:       "not found web resource category",
					ETag:       114,
					Kind:       storage.WebResourceCategoryKind,
				},
				Data: storage.WebResourceCategoryDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"not found web resource category\",\"ETag\":114,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteWebResourceCategory(&test.category)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourceCategoriesSuite) TestDeleteErrorETag() {
	var tests = []*struct {
		category storage.WebResourceCategoryV1

		expectedError error
	}{
		{
			category: storage.WebResourceCategoryV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Name:       "web resource category 111",
					ETag:       114, // Incorrect ETag.
					Kind:       storage.WebResourceCategoryKind,
				},
				Data: storage.WebResourceCategoryDataV1{},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":111,\"given\":114}\n",
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteWebResourceCategory(&test.category)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestWebResourceCategories(t *testing.T) {
	suite.Run(t, &TestWebResourceCategoriesSuite{})
}
