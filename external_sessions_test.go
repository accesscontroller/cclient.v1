package cclient

import (
	"net"
	"net/http"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalUserSessionSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestExternalUserSessionSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestExternalUserSessionSuite) TestGetSuccess() {
	var tests = []*struct {
		name storage.ResourceNameT

		source storage.ResourceNameT

		expected storage.ExternalUserSessionV1
	}{
		{
			name:   "external user 1 at source 1 session 1 at source 1",
			source: "sessions source 1",
			expected: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             101,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "external user 1 at source 1 session 1 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					Closed:                        false,
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-1",
					IPAddress:                     net.ParseIP("10.8.101.1"),
					OpenTimestamp:                 parseTimeRFC3339Panic("2020-06-21T09:00:00Z"),
				},
			},
		},
		{
			name:   "closed external user 1 at source 1 session 3 at source 1",
			source: "sessions source 1",
			expected: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             103,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "closed external user 1 at source 1 session 3 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					Closed:                        true,
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-1",
					IPAddress:                     net.ParseIP("10.8.101.2"),
					CloseTimestamp:                parseTimeRFC3339Panic("2020-06-21T07:00:00Z"),
					OpenTimestamp:                 parseTimeRFC3339Panic("2020-06-20T09:00:00Z"),
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalUserSession(test.name, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		testCompareExternalUserSessions(ts, &test.expected, got)
	}
}

func (ts *TestExternalUserSessionSuite) TestGetNotFound() {
	var tests = []*struct {
		name storage.ResourceNameT

		source storage.ResourceNameT

		expectedError error
	}{
		{
			name:   "not found session at source 1",
			source: "sessions source 1",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUserSession\",\"Name\":\"not found session at source 1\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"sessions source 1\",\"Kind\":\"ExternalSessionsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalUserSession(test.name, test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestCloseSuccess() {
	var tests = []*struct {
		session storage.ExternalUserSessionV1
	}{
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             105,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalUserSessionKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "for close external user 2 at source 1 session 2 at source 1",
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.CloseExternalUserSession(&test.session)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the session was closed.
		apiSession, err := ts.client.GetExternalUserSession(
			test.session.Metadata.Name, test.session.Metadata.ExternalSource.SourceName)
		if err != nil {
			ts.FailNow("Can not get a Session from API", "Name: %s, Source: %s, Error: %s",
				test.session.Metadata.Name, test.session.Metadata.ExternalSource.SourceName, err)
		}

		ts.True(apiSession.Data.Closed, "APISession must be Closed")
		ts.NotNil(apiSession.Data.CloseTimestamp, "APISession CloseTimestamp must be not nil")
	}
}

func (ts *TestExternalUserSessionSuite) TestCloseNotFound() {
	var tests = []*struct {
		session storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             105,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalUserSessionKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "not found session at source 1",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUserSession\",\"Name\":\"not found session at source 1\",\"ETag\":105,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"sessions source 1\",\"Kind\":\"ExternalUserSession\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.CloseExternalUserSession(&test.session)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestCloseETagError() {
	var tests = []*struct {
		session storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             105,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalUserSessionKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "for etag error external user 2 at source 1 session 3 at source 1",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":106,\"given\":105}\n",
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.CloseExternalUserSession(&test.session)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionsSuccess() {
	var tests = []*struct {
		loadOpened bool

		sessions []storage.ExternalUserSessionV1
	}{
		{
			loadOpened: true,
			sessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1001,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for open sessions source 9",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new external user session 1 at source 9 for user 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-9",
						IPAddress:                     net.ParseIP("10.8.101.9"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1002,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for open sessions source 9",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new external user session 2 at source 9 for user 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.8.101.10"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
			},
		},
		{
			loadOpened: false,
			sessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1001,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for open sessions source 9",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new external user session 3 at source 9 for user 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-11",
						IPAddress:                     net.ParseIP("10.8.101.11"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1002,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "for open sessions source 9",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new external user session 4 at source 9 for user 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-12",
						IPAddress:                     net.ParseIP("10.8.101.12"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSessions(test.sessions, test.loadOpened)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load each session to check for success.
		for i := range test.sessions {
			expectedSession := &test.sessions[i]

			apiSession, err := ts.client.GetExternalUserSession(
				expectedSession.Metadata.Name, expectedSession.Metadata.ExternalSource.SourceName)
			if err != nil {
				ts.FailNowf("Can not get a created Session from API",
					"Name: %s, Source: %s, Error: %s",
					expectedSession.Metadata.Name, expectedSession.Metadata.ExternalSource.SourceName, err)
			}

			testCompareExternalUserSessions(ts, expectedSession, apiSession)
		}

		if !test.loadOpened {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		if len(test.sessions) != len(got) {
			ts.FailNowf("Unequal expected and got Session slices lengths",
				"ExpectedStatusCode: %d, GotStatusCode: %d", len(test.sessions), len(got))
		}

		// Sort.
		sort.Slice(test.sessions, func(i, j int) bool {
			return test.sessions[i].Metadata.Name < test.sessions[j].Metadata.Name // nolint:scopelint
		})
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })

		// Compare.
		for i := range test.sessions {
			testCompareExternalUserSessions(ts, &test.sessions[i], &got[i])
		}
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionsConflict() {
	var tests = []*struct {
		sessions []storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			sessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1001,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 1 at source 1 session 1 at source 1", // Name conflict.
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-101",
						IPAddress:                     net.ParseIP("10.8.101.101"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1002,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new external user session 3 at source 9 for user 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.8.101.130"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUserSession\",\"Name\":\"external user 1 at source 1 session 1 at source 1\",\"ETag\":1001,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"sessions source 1\",\"Kind\":\"ExternalSessionsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
		{
			sessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1001,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 100 at source 1 session 1 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-101",
						IPAddress:                     net.ParseIP("10.8.101.1"), // IP Conflict.
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1002,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "new external user session 10001 at source 9 for user 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-10",
						IPAddress:                     net.ParseIP("10.8.101.131"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUserSession\",\"Name\":\"external user 100 at source 1 session 1 at source 1\",\"ETag\":1001,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"sessions source 1\",\"Kind\":\"ExternalSessionsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSessions(test.sessions, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.JSONEq(marshalJSONPanic(test.expectedError),
			marshalJSONPanic(gotErr),
			"Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionsRelatedResourceNotFound() {
	var tests = []*struct {
		sessions []storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			sessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1001,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "not found sessions source", // Not Found.
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 1002 at source 1 session 1 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-101",
						IPAddress:                     net.ParseIP("10.8.101.201"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"not found sessions source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
		{
			sessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             1001,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 1002 at source 1 session 1 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "not found user source", // Not found.
						Hostname:                      "hostname-101",
						IPAddress:                     net.ParseIP("10.8.101.201"),
						OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"external user 1 at source 1\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"not found user source\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSessions(test.sessions, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionsEmptyInput() {
	var tests = []*struct {
		sessions []storage.ExternalUserSessionV1
	}{
		{
			sessions: []storage.ExternalUserSessionV1{},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSessions(test.sessions, true)

		ts.Nil(got, "Must return nil result")
		ts.Nil(gotErr, "Must return nil error")
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionSuccess() {
	var tests = []*struct {
		loadOpened bool

		session storage.ExternalUserSessionV1
	}{
		{
			loadOpened: true,
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1001,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for open sessions source 10",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "new external user session 1 at source 10 for user 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-100",
					IPAddress:                     net.ParseIP("10.8.110.1"),
					OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
				},
			},
		},
		{
			loadOpened: false,
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1001,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "for open sessions source 10",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "new external user session 2 at source 10 for user 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-100",
					IPAddress:                     net.ParseIP("10.8.110.2"),
					OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSession(&test.session, test.loadOpened)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load each session to check for success.
		apiSession, err := ts.client.GetExternalUserSession(
			test.session.Metadata.Name, test.session.Metadata.ExternalSource.SourceName)
		if err != nil {
			ts.FailNowf("Can not get a created Session from API",
				"Name: %s, Source: %s, Error: %s",
				test.session.Metadata.Name, test.session.Metadata.ExternalSource.SourceName, err)
		}

		testCompareExternalUserSessions(ts, &test.session, apiSession)

		if !test.loadOpened {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Compare.
		testCompareExternalUserSessions(ts, &test.session, got)
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionConflict() {
	var tests = []*struct {
		session storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1001,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "external user 1 at source 1 session 1 at source 1", // Name conflict.
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-101",
					IPAddress:                     net.ParseIP("10.8.101.101"),
					OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUserSession\",\"Name\":\"external user 1 at source 1 session 1 at source 1\",\"ETag\":1001,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"sessions source 1\",\"Kind\":\"ExternalSessionsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1001,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "external user 100 at source 1 session 1 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-101",
					IPAddress:                     net.ParseIP("10.8.101.1"), // IP Conflict.
					OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUserSession\",\"Name\":\"external user 100 at source 1 session 1 at source 1\",\"ETag\":1001,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"sessions source 1\",\"Kind\":\"ExternalSessionsSource\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSession(&test.session, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.JSONEq(marshalJSONPanic(test.expectedError),
			marshalJSONPanic(gotErr),
			"Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestOpenSessionRelatedResourceNotFound() {
	var tests = []*struct {
		session storage.ExternalUserSessionV1

		expectedError error
	}{
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1001,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "not found sessions source", // Not Found.
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "external user 1002 at source 1 session 1 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "hostname-101",
					IPAddress:                     net.ParseIP("10.8.101.201"),
					OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalSessionsSource\",\"Name\":\"not found sessions source\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
		{
			session: storage.ExternalUserSessionV1{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1001,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "external user 1002 at source 1 session 1 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "external user 1 at source 1",
					ExternalUsersGroupsSourceName: "not found user source",
					Hostname:                      "hostname-101",
					IPAddress:                     net.ParseIP("10.8.101.201"),
					OpenTimestamp:                 func() *time.Time { var tm = time.Now(); return &tm }(),
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalUser\",\"Name\":\"external user 1 at source 1\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"not found user source\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.OpenExternalUserSession(&test.session, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalUserSessionSuite) TestGetSessionsSuccess() {
	var tests = []*struct {
		sessionsSource storage.ResourceNameT

		filter []storage.ExternalUserSessionFilterV1

		expecterSessions []storage.ExternalUserSessionV1
	}{
		{
			sessionsSource: "sessions source 1",
			filter: []storage.ExternalUserSessionFilterV1{
				{
					Name: "external user 1 at source 1 session 1 at source 1",
				},
			},
			expecterSessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 1 at source 1 session 1 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-1",
						IPAddress:                     net.ParseIP("10.8.101.1"),
						OpenTimestamp:                 parseTimeRFC3339Panic("2020-06-21T09:00:00Z"),
					},
				},
			},
		},
		{
			sessionsSource: "sessions source 1",
			filter: []storage.ExternalUserSessionFilterV1{
				{
					IPAddress: net.ParseIP("10.8.101.1"),
				},
				{
					Name: "external user 1 at source 1 session 2 at source 1",
				},
			},
			expecterSessions: []storage.ExternalUserSessionV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             101,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 1 at source 1 session 1 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-1",
						IPAddress:                     net.ParseIP("10.8.101.1"),
						OpenTimestamp:                 parseTimeRFC3339Panic("2020-06-21T09:00:00Z"),
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion:       storage.APIVersionV1Value,
						ETag:             102,
						ExternalResource: true,
						ExternalSource: &storage.ExternalSourceInfo{
							SourceName: "sessions source 1",
							Kind:       storage.ExternalSessionsSourceKind,
						},
						Kind: storage.ExternalUserSessionKind,
						Name: "external user 1 at source 1 session 2 at source 1",
					},
					Data: storage.ExternalUserSessionDataV1{
						ExternalUserName:              "external user 1 at source 1",
						ExternalUsersGroupsSourceName: "source 1",
						Hostname:                      "hostname-1",
						IPAddress:                     net.ParseIP("10.8.101.2"),
						OpenTimestamp:                 parseTimeRFC3339Panic("2020-06-21T10:00:00Z"),
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalUserSessions(test.sessionsSource, test.filter)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Compare.
		if !ts.Len(got, len(test.expecterSessions), "Unexpected got sessions length") {
			ts.FailNow("Unexpected got sessions length - can not continue")
		}

		// Sort.
		sort.Slice(got, func(i, j int) bool { return got[i].Metadata.Name < got[j].Metadata.Name })
		sort.Slice(test.expecterSessions, func(i, j int) bool {
			return test.expecterSessions[i].Metadata.Name < test.expecterSessions[j].Metadata.Name // nolint:scopelint
		})

		// Compare.
		for i := range test.expecterSessions {
			testCompareExternalUserSessions(ts, &test.expecterSessions[i], &got[i])
		}
	}
}

func TestExternalUserSession(t *testing.T) {
	suite.Run(t, &TestExternalUserSessionSuite{})
}

func testCompareExternalUserSessions(ts *TestExternalUserSessionSuite, expected, got *storage.ExternalUserSessionV1) {
	ts.Equal(expected.Metadata, got.Metadata, "Metadata has differences")
	ts.Equal(expected.Data.Closed, got.Data.Closed, "Closed must be equal")
	ts.Equal(expected.Data.ExternalUserName, got.Data.ExternalUserName, "ExternalUserName must be equal")
	ts.Equal(expected.Data.ExternalUsersGroupsSourceName,
		got.Data.ExternalUsersGroupsSourceName, "ExternalUsersGroupsSourceName must be equal")
	ts.Equal(expected.Data.Hostname, got.Data.Hostname, "Hostname must be equal")
	ts.Equal(expected.Data.IPAddress, got.Data.IPAddress, "IPAddress must be equal")

	if expected.Data.OpenTimestamp == nil {
		ts.Nil(got.Data.OpenTimestamp, "got OpenTimestamp must be nil")
	} else {
		if !ts.NotNil(got.Data.OpenTimestamp, "got OpenTimestamp must not be nil") {
			ts.FailNow("got OpenTimestamp is nil - can not continue")
		}

		ts.WithinDuration(*expected.Data.OpenTimestamp, *got.Data.OpenTimestamp, time.Second,
			"Unexpected OpenTimestamp value")
	}

	if expected.Data.CloseTimestamp == nil {
		ts.Nil(got.Data.CloseTimestamp, "got CloseTimestamp must be nil")
	} else {
		if !ts.NotNil(got.Data.CloseTimestamp, "got CloseTimestamp must not be nil") {
			ts.FailNow("got CloseTimestamp is nil - can not continue")
		}

		ts.WithinDuration(*expected.Data.CloseTimestamp, *got.Data.CloseTimestamp, time.Second,
			"Unexpected CloseTimestamp value")
	}
}
