package cclient

import (
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestExternalGroup2ExternalUserListRelationsSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestGetRelationsSuccess() {
	var tests = []*struct {
		name storage.ResourceNameT

		source storage.ResourceNameT

		expectedRelations storage.ExternalGroup2ExternalUserListRelationsV1
	}{
		{
			name:   "already binded group 29 at source 16 to users 5, 6, 7",
			source: "for group to users binding source 16",
			expectedRelations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "already binded group 29 at source 16 to users 5, 6, 7",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "already binded group 29 at source 16 to users 5, 6, 7",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"user 5 binded to external group 29 at source 16",
						"user 6 binded to external group 29 at source 16",
						"user 7 binded to external group 29 at source 16",
					},
				},
			},
		},
		{
			name:   "group 30 at source 16 without related users",
			source: "for group to users binding source 16",
			expectedRelations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "group 30 at source 16 without related users",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "group 30 at source 16 without related users",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers:             []storage.ResourceNameT{},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalGroup2ExternalUserListRelationsV1(test.name, test.source)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedRelations, *got, "Unexpected result value")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestGetRelationsGroupNotFound() {
	var tests = []*struct {
		name storage.ResourceNameT

		source storage.ResourceNameT

		expectedError error
	}{
		{
			name:   "not found group",
			source: "for group to users binding source 16",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found group\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for group to users binding source 16\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetExternalGroup2ExternalUserListRelationsV1(test.name, test.source)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestCreateRelationsSuccess() {
	var tests = []*struct {
		loadCreated bool

		relationsToCreate storage.ExternalGroup2ExternalUserListRelationsV1

		expectedRelations storage.ExternalGroup2ExternalUserListRelationsV1
	}{
		{
			loadCreated: false, // Loading result is not implemented on API server yet.
			relationsToCreate: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for creating group to users relations group 31 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for creating group to users relations group 31 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 9 at source 16",
					},
				},
			},
			expectedRelations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for creating group to users relations group 31 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for creating group to users relations group 31 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 9 at source 16",
					},
				},
			},
		},
		{
			loadCreated: false,
			relationsToCreate: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for creating group to users relations group 32 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for creating group to users relations group 32 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 10 at source 16",
					},
				},
			},
			expectedRelations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for creating group to users relations group 32 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for creating group to users relations group 32 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 10 at source 16",
						"for creation group to users relations user 11 binded to group 32 at source 16",
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalGroup2ExternalUserListV1Relations(&test.relationsToCreate, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a relations from API.
		apiRels, err := ts.client.GetExternalGroup2ExternalUserListRelationsV1(
			test.relationsToCreate.Data.ExternalGroup, test.relationsToCreate.Data.ExternalUsersGroupsSource)
		if err != nil {
			ts.FailNowf("Can not load API Relations", "Group: %s:%s, Error: %s",
				test.relationsToCreate.Data.ExternalUsersGroupsSource,
				test.relationsToCreate.Data.ExternalGroup, err)
		}

		// Sort.
		sort.Slice(test.expectedRelations.Data.ExternalUsers, func(i, j int) bool {
			return test.expectedRelations.Data.ExternalUsers[i] < test.expectedRelations.Data.ExternalUsers[j] // nolint:scopelint
		})
		sort.Slice(apiRels.Data.ExternalUsers, func(i, j int) bool {
			return apiRels.Data.ExternalUsers[i] < apiRels.Data.ExternalUsers[j]
		})

		ts.Equal(test.expectedRelations, *apiRels, "Unexpected result relation on API Server")

		if !test.loadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		sort.Slice(got.Data.ExternalUsers, func(i, j int) bool {
			return got.Data.ExternalUsers[i] < got.Data.ExternalUsers[j]
		})

		ts.Equal(test.expectedRelations, *got, "Unexpected result value")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestCreateRelationsGroupNotFound() {
	var tests = []*struct {
		relationsToCreate storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relationsToCreate: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "new relations",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "not found group at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 9 at source 16",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found group at source 16\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for group to users binding source 16\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalGroup2ExternalUserListV1Relations(&test.relationsToCreate, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestCreateRelationsUserNotFound() {
	var tests = []*struct {
		relationsToCreate storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relationsToCreate: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for creating group to users relations group 32 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for creating group to users relations group 32 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 9 at source 16",
						"not found user",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"api_version\":\"v1\",\"error_kind\":\"ErrRelatedResourcesNotFound\",\"kind\":\"ExternalUser\",\"names\":[\"not found user\"]}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateExternalGroup2ExternalUserListV1Relations(&test.relationsToCreate, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestDeleteRelationsSuccess() {
	var tests = []*struct {
		relationsToDelete storage.ExternalGroup2ExternalUserListRelationsV1

		expectedRelations storage.ExternalGroup2ExternalUserListRelationsV1
	}{
		{
			relationsToDelete: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for deleting group to users relations group 33 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for deleting group to users relations group 33 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for deleting group to users relations user 12 binded to group 33 at source 16",
						"for deleting group to users relations user 13 binded to group 33 at source 16",
					},
				},
			},
			expectedRelations: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for deleting group to users relations group 33 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for deleting group to users relations group 33 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for deleting group to users relations user 14 binded to group 33 at source 16",
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalGroup2ExternalUserListV1Relations(
			&test.relationsToDelete)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load a relations from API.
		apiRels, err := ts.client.GetExternalGroup2ExternalUserListRelationsV1(
			test.relationsToDelete.Data.ExternalGroup,
			test.relationsToDelete.Data.ExternalUsersGroupsSource)
		if err != nil {
			ts.FailNowf("Can not load API Relations", "Group: %s:%s, Error: %s",
				test.relationsToDelete.Data.ExternalUsersGroupsSource,
				test.relationsToDelete.Data.ExternalGroup, err)
		}

		// Sort.
		sort.Slice(test.expectedRelations.Data.ExternalUsers, func(i, j int) bool {
			return test.expectedRelations.Data.ExternalUsers[i] < test.expectedRelations.Data.ExternalUsers[j] // nolint:scopelint
		})
		sort.Slice(apiRels.Data.ExternalUsers, func(i, j int) bool {
			return apiRels.Data.ExternalUsers[i] < apiRels.Data.ExternalUsers[j]
		})

		ts.Equal(test.expectedRelations, *apiRels, "Unexpected result relation on API Server")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestDeleteRelationsGroupNotFound() {
	var tests = []*struct {
		relationsToDelete storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relationsToDelete: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "new relations",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "not found group at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 9 at source 16",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"ExternalGroup\",\"Name\":\"not found group at source 16\",\"ETag\":0,\"ExternalResource\":true,\"ExternalSource\":{\"SourceName\":\"for group to users binding source 16\",\"Kind\":\"ExternalUsersGroupsSource\"}}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalGroup2ExternalUserListV1Relations(&test.relationsToDelete)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestExternalGroup2ExternalUserListRelationsSuite) TestDeleteRelationsUserNotFound() {
	var tests = []*struct {
		relationsToDelete storage.ExternalGroup2ExternalUserListRelationsV1

		expectedError error
	}{
		{
			relationsToDelete: storage.ExternalGroup2ExternalUserListRelationsV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.ExternalGroup2ExternalUserListV1RelationsKind,
					Name:       "for creating group to users relations group 32 at source 16",
				},
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup:             "for creating group to users relations group 32 at source 16",
					ExternalUsersGroupsSource: "for group to users binding source 16",
					ExternalUsers: []storage.ResourceNameT{
						"for creation group to users relations user 8 at source 16",
						"for creation group to users relations user 9 at source 16",
						"not found user",
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"api_version\":\"v1\",\"error_kind\":\"ErrRelatedResourcesNotFound\",\"kind\":\"ExternalUser\",\"names\":[\"not found user\"]}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteExternalGroup2ExternalUserListV1Relations(&test.relationsToDelete)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestExternalGroup2ExternalUserListRelations(t *testing.T) {
	suite.Run(t, &TestExternalGroup2ExternalUserListRelationsSuite{})
}
