package cclient

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// GetResourcesV1 loads resources.
// Returns error if at least one resource from given names is not found.
func (c *CClient) GetResourcesV1(kind storage.ResourceKindT, // nolint:funlen,gocognit,gocyclo
	source storage.ResourceNameT, names ...storage.ResourceNameT) ([]interface{}, error) {
	var result = make([]interface{}, 0, len(names))

	switch kind {
	case storage.AccessGroup2ExternalGroupListV1RelationsKind:
		res, err := c.GetAccessGroup2ExternalGroupListRelationsV1s(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.AccessGroup2WebResourceCategoryListV1RelationsKind:
		res, err := c.GetAccessGroup2WebResourceCategoryListRelationsV1s(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.AccessGroupKind:
		res, err := c.GetAccessGroups(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalGroup2ExternalUserListV1RelationsKind:
		res, err := c.GetExternalGroup2ExternalUserListRelationsV1s(names, source)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalGroupKind:
		res, err := c.GetExternalGroups(names, source)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalSessionsSourceKind:
		res, err := c.GetExternalSessionsSourcesByNames(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalUser2ExternalGroupListV1RelationsKind:
		res, err := c.GetExternalGroup2ExternalUserListRelationsV1s(names, source)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalUserSessionKind:
		res, err := c.GetExternalUserSessionsByNames(source, names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalUsersGroupsSourceKind:
		res, err := c.GetExternalUsersGroupsSourcesByNames(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.ExternalUserKind:
		res, err := c.GetExternalUsersByNames(names, source)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.WebResourceCategoryKind:
		res, err := c.GetWebResourceCategoriesByNames(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	case storage.WebResourceKind:
		res, err := c.GetWebResourcesByNames(names)
		if err != nil {
			return nil, err
		}

		for i := range res {
			result = append(result, res[i])
		}

	default:
		return nil, fmt.Errorf("%w: %q", ErrUnsupportedResourceType, kind)
	}

	return result, nil
}
