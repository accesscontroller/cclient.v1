package cclient

import (
	"net"
	"net/http"
	"net/url"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestWebResourcesSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestWebResourcesSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestWebResourcesSuite) TestWebResourceCategoryGetWebResourceSuccess() {
	var tests = []*struct {
		category storage.ResourceNameT
		name     storage.ResourceNameT

		expectedWebResource storage.WebResourceV1
	}{
		{
			category: "web resource category 115",
			name:     "web resource 101",
			expectedWebResource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 101",
				},
				Data: storage.WebResourceDataV1{
					WebResourceCategoryName: "web resource category 115",
					Description:             "web resource description 101",
					Domains: []storage.WebResourceDomainT{
						"web-resource-101-domain-1.com",
						"web-resource-101-domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.101.1"),
						net.ParseIP("10.8.101.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://web-resource-101-url-1.com/url-1"),
						parseURLPanic("http://web-resource-101-url-2.com/url-2"),
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetWebResourceCategoryWebResource(test.category, test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedWebResource, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourcesSuite) TestGetWebResourceCategoryWebResourceNotFound() {
	var tests = []*struct {
		category storage.ResourceNameT
		name     storage.ResourceNameT

		expectedError error
	}{
		{
			category: "web resource category 115",
			name:     "not found web resource",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResource\",\"Name\":\"not found web resource\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
		{
			// No a Web Resource in a Category.
			category: "web resource category 116",
			name:     "web resource 101",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResource\",\"Name\":\"web resource 101\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetWebResourceCategoryWebResource(test.category, test.name)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestWebResourceCategoryGetWebResourcesSuccess() {
	var tests = []*struct {
		category storage.ResourceNameT
		filters  []storage.WebResourceFilterV1

		expectedWebResources []storage.WebResourceV1
	}{
		{
			category: "web resource category 115",
			filters: []storage.WebResourceFilterV1{
				{
					NameSubstringMatch: "101",
				},
			},
			expectedWebResources: []storage.WebResourceV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       101,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 101",
					},
					Data: storage.WebResourceDataV1{
						WebResourceCategoryName: "web resource category 115",
						Description:             "web resource description 101",
						Domains: []storage.WebResourceDomainT{
							"web-resource-101-domain-1.com",
							"web-resource-101-domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.101.1"),
							net.ParseIP("10.8.101.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://web-resource-101-url-1.com/url-1"),
							parseURLPanic("http://web-resource-101-url-2.com/url-2"),
						},
					},
				},
			},
		},
		{
			category: "web resource category 116", // Must not return any Web Resource with given filter.
			filters: []storage.WebResourceFilterV1{
				{
					NameSubstringMatch: "101",
				},
			},
			expectedWebResources: []storage.WebResourceV1{},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.GetWebResourceCategoryWebResources(test.category, test.filters)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedWebResources, got, "Unexpected result value")
	}
}

func (ts *TestWebResourcesSuite) TestCreateWebResourcesSuccess() {
	var tests []*struct {
		GetCreated bool

		WebResources []storage.WebResourceV1
	}

	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not faker.FakeData", "Error: %s", err)
	}

	for _, test := range tests {
		for i := range test.WebResources {
			test.WebResources[i].Metadata.Kind = storage.WebResourceKind
		}
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResources(test.WebResources, test.GetCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// For every resource perform a check.
		for i := range test.WebResources {
			r := &test.WebResources[i]

			apiResource, err := ts.client.GetWebResourceCategoryWebResource(r.Data.WebResourceCategoryName, r.Metadata.Name)
			if err != nil {
				ts.FailNowf("Can not load a WebResource from API for a check",
					"Category: %s, Name: %s, Error: %s",
					r.Data.WebResourceCategoryName, r.Metadata.Name, err)
			}

			ts.Equal(r, apiResource, "Unexpected WebResource value")
		}

		if !test.GetCreated {
			continue
		}

		// Check loaded.
		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.WebResources, got, "Unexpected returned WebResources result")
	}
}

func (ts *TestWebResourcesSuite) TestCreateWebResourcesNameConflict() {
	var tests = []*struct {
		WebResources []storage.WebResourceV1

		expectedError error
	}{
		{
			WebResources: []storage.WebResourceV1{
				{ // Conflict.
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       101,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 101",
					},
					Data: storage.WebResourceDataV1{
						WebResourceCategoryName: "web resource category 115",
						Description:             "web resource description 101",
						Domains: []storage.WebResourceDomainT{
							"web-resource-101-domain-1.com",
							"web-resource-101-domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.101.1"),
							net.ParseIP("10.8.101.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://web-resource-101-url-1.com/url-1"),
							parseURLPanic("http://web-resource-101-url-2.com/url-2"),
						},
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       10100,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 10100",
					},
					Data: storage.WebResourceDataV1{
						WebResourceCategoryName: "web resource category 115",
						Description:             "web resource description 10100",
						Domains: []storage.WebResourceDomainT{
							"web-resource-101-domain-100.com",
							"web-resource-101-domain-200.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.101.100"),
							net.ParseIP("10.8.101.200"),
						},
						URLs: []url.URL{
							parseURLPanic("http://web-resource-10100-url-1.com/url-1"),
							parseURLPanic("http://web-resource-10100-url-2.com/url-2"),
						},
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"WebResource\",\"Name\":\"web resource 101\",\"ETag\":101,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResources(test.WebResources, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestCreateWebResourcesCategoryNotFound() {
	var tests = []*struct {
		WebResources []storage.WebResourceV1

		expectedError error
	}{
		{
			WebResources: []storage.WebResourceV1{
				{ // Conflict.
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       101,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 101100",
					},
					Data: storage.WebResourceDataV1{
						WebResourceCategoryName: "not found web resource category",
						Description:             "web resource description 101",
						Domains: []storage.WebResourceDomainT{
							"web-resource-101-domain-1.com",
							"web-resource-101-domain-2.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.101.1"),
							net.ParseIP("10.8.101.2"),
						},
						URLs: []url.URL{
							parseURLPanic("http://web-resource-101-url-1.com/url-1"),
							parseURLPanic("http://web-resource-101-url-2.com/url-2"),
						},
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       10100,
						Kind:       storage.WebResourceKind,
						Name:       "web resource 10200",
					},
					Data: storage.WebResourceDataV1{
						WebResourceCategoryName: "web resource category 115",
						Description:             "web resource description 10100",
						Domains: []storage.WebResourceDomainT{
							"web-resource-101-domain-100.com",
							"web-resource-101-domain-200.com",
						},
						IPs: []net.IP{
							net.ParseIP("10.8.101.100"),
							net.ParseIP("10.8.101.200"),
						},
						URLs: []url.URL{
							parseURLPanic("http://web-resource-10100-url-1.com/url-1"),
							parseURLPanic("http://web-resource-10100-url-2.com/url-2"),
						},
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"not found web resource category\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResources(test.WebResources, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestCreateWebResourceSuccess() {
	var tests []*struct {
		GetCreated bool

		WebResource storage.WebResourceV1
	}

	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not faker.FakeData", "Error: %s", err)
	}

	for _, test := range tests {
		test.WebResource.Metadata.Kind = storage.WebResourceKind
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResource(&test.WebResource, test.GetCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// For a new resource perform a check.
		r := &test.WebResource

		apiResource, err := ts.client.GetWebResourceCategoryWebResource(r.Data.WebResourceCategoryName, r.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load a WebResource from API for a check",
				"Category: %s, Name: %s, Error: %s",
				r.Data.WebResourceCategoryName, r.Metadata.Name, err)
		}

		ts.Equal(r, apiResource, "Unexpected WebResource value")

		if !test.GetCreated {
			continue
		}

		// Check loaded.
		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.WebResource, *got, "Unexpected returned WebResources result")
	}
}

func (ts *TestWebResourcesSuite) TestCreateWebResourceNameConflict() {
	var tests = []*struct {
		WebResource storage.WebResourceV1

		expectedError error
	}{
		{
			WebResource: storage.WebResourceV1{
				// Conflict.
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 101",
				},
				Data: storage.WebResourceDataV1{
					WebResourceCategoryName: "web resource category 115",
					Description:             "web resource description 101",
					Domains: []storage.WebResourceDomainT{
						"web-resource-101-domain-1.com",
						"web-resource-101-domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.101.1"),
						net.ParseIP("10.8.101.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://web-resource-101-url-1.com/url-1"),
						parseURLPanic("http://web-resource-101-url-2.com/url-2"),
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"APIVersion\":\"v1\",\"Kind\":\"WebResource\",\"Name\":\"web resource 101\",\"ETag\":101,\"ExternalResource\":false,\"ExternalSource\":null}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResource(&test.WebResource, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestCreateWebResourceCategoryNotFound() {
	var tests = []*struct {
		WebResource storage.WebResourceV1

		expectedError error
	}{
		{
			WebResource: storage.WebResourceV1{
				// Conflict.
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       101311,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 101123",
				},
				Data: storage.WebResourceDataV1{
					WebResourceCategoryName: "not found web resource category",
					Description:             "web resource description 101",
					Domains: []storage.WebResourceDomainT{
						"web-resource-101-domain-1.com",
						"web-resource-101-domain-2.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.101.1"),
						net.ParseIP("10.8.101.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://web-resource-101-url-1.com/url-1"),
						parseURLPanic("http://web-resource-101-url-2.com/url-2"),
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResourceCategory\",\"Name\":\"not found web resource category\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.CreateWebResource(&test.WebResource, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestUpdateWebResourceSuccess() {
	var tests = []*struct {
		oldName    storage.ResourceNameT
		getUpdated bool

		patch storage.WebResourceV1
	}{
		{
			oldName:    "for web resources update web resource 103",
			getUpdated: true,
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       103,
					Kind:       storage.WebResourceKind,
					Name:       "renamed for web resources update web resource 103",
				},
				Data: storage.WebResourceDataV1{
					WebResourceCategoryName: "for web resources update web resource category 120",
					Description:             "new description for web resources update web resource 103",
					Domains: []storage.WebResourceDomainT{
						"103-web-resource.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.103.121"),
					},
					URLs: []url.URL{
						parseURLPanic("http://103.web-resource.org"),
					},
				},
			},
		},
		{
			oldName: "for web resources update web resource 104",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       104,
					Kind:       storage.WebResourceKind,
					Name:       "renamed for web resources update web resource 104",
				},
				Data: storage.WebResourceDataV1{
					WebResourceCategoryName: "for web resources update web resource category 120",
					Description:             "new description for web resources update web resource 104",
					Domains: []storage.WebResourceDomainT{
						"104-web-resource.com",
					},
					IPs: []net.IP{
						net.ParseIP("10.8.104.122"),
					},
					URLs: []url.URL{
						parseURLPanic("http://104.web-resource.org"),
					},
				},
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResource(test.oldName, &test.patch, test.getUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Correct ETag.
		test.patch.Metadata.ETag++

		// Check Updated.
		apiResource, err := ts.client.GetWebResourceCategoryWebResource(
			test.patch.Data.WebResourceCategoryName, test.patch.Metadata.Name)
		if err != nil {
			ts.FailNowf("Can not load a WebResource from API server",
				"Name: %s, Error: %s", test.patch.Metadata.Name, err)
		}

		ts.Equal(test.patch, *apiResource, "Unexpected API WebResource value")

		if !test.getUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.patch, *got, "Unexpected result value")
	}
}

func (ts *TestWebResourcesSuite) TestUpdateWebResourceNotFound() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceV1

		expectedError error
	}{
		{
			oldName: "not found web resource",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResource\",\"Name\":\"not found web resource\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", //nolint:lll
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResource(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestUpdateWebResourceETagError() {
	var tests = []*struct {
		oldName storage.ResourceNameT

		patch storage.WebResourceV1

		expectedError error
	}{
		{
			oldName: "web resource 102",
			patch: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       12312, // Incorrect.
					Name:       "web resource 102",
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":102,\"given\":12312}\n",
			},
		},
	}

	for _, test := range tests {
		got, gotErr := ts.client.UpdateWebResource(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestDeleteWebResourceSuccess() {
	var tests = []*struct {
		resource storage.WebResourceV1
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       "for web resources delete web resource 105",
					ETag:       105,
				},
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteWebResource(&test.resource)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Check that the resource was deleted.
		apiResource, err := ts.client.GetWebResourceCategoryWebResource(
			test.resource.Data.WebResourceCategoryName, test.resource.Metadata.Name)

		ts.Error(err, "Must return not found error on request deleted resource from API")
		ts.Nil(apiResource, "Must return nil API Resource")
	}
}

func (ts *TestWebResourcesSuite) TestDeleteWebResourceNotFound() {
	var tests = []*struct {
		resource storage.WebResourceV1

		expectedError error
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       "Not Found Resource",
					ETag:       105,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"WebResource\",\"Name\":\"Not Found Resource\",\"ETag\":105,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteWebResource(&test.resource)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestWebResourcesSuite) TestDeleteWebResourceETagError() {
	var tests = []*struct {
		resource storage.WebResourceV1

		expectedError error
	}{
		{
			resource: storage.WebResourceV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.WebResourceKind,
					Name:       "web resource 101",
					ETag:       101100, // Incorrect ETag.
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":101,\"given\":101100}\n",
			},
		},
	}

	for _, test := range tests {
		gotErr := ts.client.DeleteWebResource(&test.resource)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestWebResources(t *testing.T) {
	suite.Run(t, &TestWebResourcesSuite{})
}
