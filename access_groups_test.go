package cclient

import (
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestAccessGroupSuite struct {
	client *CClient

	suite.Suite
}

func (ts *TestAccessGroupSuite) SetupSuite() {
	client, err := initClientTest()
	if err != nil {
		ts.FailNow("Can not init cclient", err)
	}

	ts.client = client

	// Configure Faker.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNow("Can not configure Faker", err)
	}
}

func (ts *TestAccessGroupSuite) SetupTest() {

}

func (ts *TestAccessGroupSuite) TearDownTest() {

}

func (ts *TestAccessGroupSuite) TestGetAccessGroupSuccess() {
	var tests = []struct {
		name storage.ResourceNameT

		expectedGroup storage.AccessGroupV1
	}{
		{
			name: "access group 1",
			expectedGroup: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 1",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "access group 1 access denied",
					OrderInAccessRulesList:  101,
					TotalGroupSpeedLimitBps: 100000001,
					UserGroupSpeedLimitBps:  1000001,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		lGroup, gotErr := ts.client.GetAccessGroup(test.name)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(lGroup, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.expectedGroup, *lGroup, "Unexpected returned value")
	}
}

func (ts *TestAccessGroupSuite) TestGetAccessGroupNotFound() {
	var tests = []struct {
		name storage.ResourceNameT

		expectedError error
	}{
		{
			name: "not found access group 1",
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group 1\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		lGroup, gotErr := ts.client.GetAccessGroup(test.name)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.Nil(lGroup, "Must return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupSuite) TestGetAccessGroupsSuccess() {
	var tests = []struct {
		names []storage.ResourceNameT

		expectedGroups []storage.AccessGroupV1
	}{
		{
			names: []storage.ResourceNameT{"access group 1", "access group 2"},
			expectedGroups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 1",
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "access group 1 access denied",
						OrderInAccessRulesList:  101,
						TotalGroupSpeedLimitBps: 100000001,
						UserGroupSpeedLimitBps:  1000001,
					},
				},
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       2,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 2",
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyDeny,
						ExtraAccessDenyMessage:  "access group 2 access denied",
						OrderInAccessRulesList:  102,
						TotalGroupSpeedLimitBps: 100000002,
						UserGroupSpeedLimitBps:  1000002,
					},
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.GetAccessGroups(test.names)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got not nil result - can not continue")
		}

		ts.Equal(test.expectedGroups, got, "Unexpected result")
	}
}

func (ts *TestAccessGroupSuite) TestCreateAccessGroupsSuccess() {
	// Prepare random data so at not to get name conflict.
	var tests = make([]struct {
		LoadCreated bool

		Groups []storage.AccessGroupV1
	}, 5)

	// Fill data.
	for i := range tests {
		t := &tests[i]

		if err := faker.FakeData(&t.Groups); err != nil {
			ts.FailNow("Can not faker.FakeData", err)
		}

		if err := faker.FakeData(&t.LoadCreated); err != nil {
			ts.FailNow("Can not faker.FakeData", err)
		}

		for j := range t.Groups {
			gr := &t.Groups[j]

			gr.Metadata.Kind = storage.AccessGroupKind
			gr.Metadata.ExternalSource = nil
			gr.Metadata.ExternalResource = false
		}
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroups(test.Groups, test.LoadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load every AccessGroup to check it.
		for gInd := range test.Groups {
			gotGr, err := ts.client.GetAccessGroup(test.Groups[gInd].Metadata.Name)
			if err != nil {
				ts.FailNow("Can not load an AccessGroup for a check", err)
			}

			if !ts.NotNil(gotGr, "Must not return nil") {
				ts.FailNow("Can not load an AccessGroup for a check: nil result")
			}

			ts.Equal(test.Groups[gInd], *gotGr, "Unexpected group got from AccessController")
		}

		if !test.LoadCreated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.Groups, got, "Unexpected result")
	}
}

func (ts *TestAccessGroupSuite) TestCreateAccessGroupsConflict() {
	var tests = []struct {
		groups []storage.AccessGroupV1

		expectedError error
	}{
		{
			groups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 1", // Name Conflict
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "access group 1 access denied",
						OrderInAccessRulesList:  1010,
						TotalGroupSpeedLimitBps: 100000001,
						UserGroupSpeedLimitBps:  1000001,
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"access group 1\",\"ETag\":1,\"ExternalResource\":false,\"ExternalSource\":null},\"Data\":{\"DefaultPolicy\":\"allow\",\"OrderInAccessRulesList\":1010,\"TotalGroupSpeedLimitBps\":100000001,\"UserGroupSpeedLimitBps\":1000001,\"ExtraAccessDenyMessage\":\"access group 1 access denied\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
		{
			groups: []storage.AccessGroupV1{
				{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						ETag:       1,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 100",
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "access group 1 access denied",
						OrderInAccessRulesList:  101, // Order conflict.
						TotalGroupSpeedLimitBps: 100000001,
						UserGroupSpeedLimitBps:  1000001,
					},
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusConflict,
				ExpectedStatusCode: http.StatusCreated,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"access group 100\",\"ETag\":1,\"ExternalResource\":false,\"ExternalSource\":null},\"Data\":{\"DefaultPolicy\":\"allow\",\"OrderInAccessRulesList\":101,\"TotalGroupSpeedLimitBps\":100000001,\"UserGroupSpeedLimitBps\":1000001,\"ExtraAccessDenyMessage\":\"access group 1 access denied\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroups(test.groups, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		// Compare using JSON to make ints and floats look the same.
		ts.JSONEq(marshalJSONPanic(test.expectedError), marshalJSONPanic(gotErr), "Unexpected error value")
	}
}

func (ts *TestAccessGroupSuite) TestCreateAccessGroupSuccess() {
	// Prepare random data so at not to get name conflict.
	var tests = []struct {
		loadCreated bool

		group storage.AccessGroupV1
	}{
		{
			loadCreated: false,
		},
		{
			loadCreated: true,
		},
	}

	// Fill data.
	for i := range tests {
		t := &tests[i]

		if err := faker.FakeData(&t.group); err != nil {
			ts.FailNow("Can not faker.FakeData", err)
		}

		t.group.Metadata.Kind = storage.AccessGroupKind
		t.group.Metadata.ExternalSource = nil
		t.group.Metadata.ExternalResource = false
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroup(&test.group, test.loadCreated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Load the created AccessGroup to check it.
		gotGr, err := ts.client.GetAccessGroup(test.group.Metadata.Name)
		if err != nil {
			ts.FailNow("Can not load an AccessGroup for a check", err)
		}

		if !ts.NotNil(gotGr, "Must not return nil") {
			ts.FailNow("Can not load an AccessGroup for a check: nil result")
		}

		ts.Equal(test.group, *gotGr, "Unexpected group got from AccessController")

		if !test.loadCreated {
			continue
		}

		// Check returned result.
		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		ts.Equal(test.group, *got, "Unexpected result")
	}
}

func (ts *TestAccessGroupSuite) TestCreateAccessGroupConflict() {
	var tests = []struct {
		group storage.AccessGroupV1

		expectedError error
	}{
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 1", // Name Conflict
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "access group 1 access denied",
					OrderInAccessRulesList:  1010,
					TotalGroupSpeedLimitBps: 100000001,
					UserGroupSpeedLimitBps:  1000001,
				},
			},
			expectedError: &ErrServerResponseStatus{
				ExpectedStatusCode: http.StatusCreated,
				GotStatusCode:      http.StatusConflict,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"access group 1\",\"ETag\":1,\"ExternalResource\":false,\"ExternalSource\":null},\"Data\":{\"DefaultPolicy\":\"allow\",\"OrderInAccessRulesList\":1010,\"TotalGroupSpeedLimitBps\":100000001,\"UserGroupSpeedLimitBps\":1000001,\"ExtraAccessDenyMessage\":\"access group 1 access denied\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 100",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "access group 1 access denied",
					OrderInAccessRulesList:  101, // Order conflict.
					TotalGroupSpeedLimitBps: 100000001,
					UserGroupSpeedLimitBps:  1000001,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      409,
				ExpectedStatusCode: 201,
				Message:            "{\"conflicts\":[{\"Metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"access group 100\",\"ETag\":1,\"ExternalResource\":false,\"ExternalSource\":null},\"Data\":{\"DefaultPolicy\":\"allow\",\"OrderInAccessRulesList\":101,\"TotalGroupSpeedLimitBps\":100000001,\"UserGroupSpeedLimitBps\":1000001,\"ExtraAccessDenyMessage\":\"access group 1 access denied\"}}],\"error_kind\":\"ErrResourceAlreadyExists\"}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.CreateAccessGroup(&test.group, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		// Compare using JSON to make ints and floats look the same.
		ts.JSONEq(marshalJSONPanic(test.expectedError), marshalJSONPanic(gotErr), "Unexpected error value")
	}
}

func (ts *TestAccessGroupSuite) TestUpdateSuccess() {
	var tests = []struct {
		getUpdated bool

		oldName storage.ResourceNameT

		patch storage.AccessGroupV1
	}{
		{
			oldName: "for update access group 6",
			patch: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       6,
					Kind:       storage.AccessGroupKind,
					Name:       "renamed for update access group 6",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "updated access group 6 access denied",
					OrderInAccessRulesList:  91010,
					TotalGroupSpeedLimitBps: 200000001,
					UserGroupSpeedLimitBps:  3000001,
				},
			},
		},
		{
			getUpdated: true,
			oldName:    "for update access group 7",
			patch: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       7,
					Kind:       storage.AccessGroupKind,
					Name:       "renamed access group 7",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "updated access group 7 access denied",
					OrderInAccessRulesList:  91011,
					TotalGroupSpeedLimitBps: 200000002,
					UserGroupSpeedLimitBps:  3000002,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateAccessGroup(test.oldName, &test.patch, test.getUpdated)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Got error - can not continue")
		}

		// Try to load an updated group.
		apiGroup, err := ts.client.GetAccessGroup(test.patch.Metadata.Name)
		if err != nil {
			ts.FailNow("Can not load an updated group from DB", err)
		}

		// ETag update.
		apiGroup.Metadata.ETag--

		ts.Equal(test.patch, *apiGroup, "Unexpected API server AccessGroup state")

		if !test.getUpdated {
			continue
		}

		if !ts.NotNil(got, "Must not return nil result") {
			ts.FailNow("Got nil result - can not continue")
		}

		// Compare result.
		got.Metadata.ETag--

		ts.Equal(test.patch, *got, "Unexpected returned result")
	}
}

func (ts *TestAccessGroupSuite) TestUpdateNotFound() {
	var tests = []struct {
		oldName storage.ResourceNameT

		patch storage.AccessGroupV1

		expectedError error
	}{
		{
			oldName: "not found access group 1",
			patch: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
					Name:       "renamed access group 1",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "updated access group 1 access denied",
					OrderInAccessRulesList:  91010,
					TotalGroupSpeedLimitBps: 200000001,
					UserGroupSpeedLimitBps:  3000001,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusOK,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group 1\",\"ETag\":0,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		got, gotErr := ts.client.UpdateAccessGroup(test.oldName, &test.patch, true)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Nil(got, "Must return nil result")

		ts.Equal(test.expectedError, gotErr, "Unexpected returned error")
	}
}

func (ts *TestAccessGroupSuite) TestDeleteSuccess() {
	var tests = []struct {
		group storage.AccessGroupV1
	}{
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "for delete access group 5",
					ETag:       3,
				},
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteAccessGroup(&test.group)

		ts.NoError(gotErr, "Must not return error")
	}
}

func (ts *TestAccessGroupSuite) TestDeleteNotFound() {
	var tests = []struct {
		group storage.AccessGroupV1

		expectedError error
	}{
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "not found access group 3",
					ETag:       3,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusNotFound,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrResourceNotFound\",\"metadata\":{\"APIVersion\":\"v1\",\"Kind\":\"AccessGroup\",\"Name\":\"not found access group 3\",\"ETag\":3,\"ExternalResource\":false,\"ExternalSource\":null}}\n", // nolint:lll
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteAccessGroup(&test.group)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func (ts *TestAccessGroupSuite) TestDeleteETagError() {
	var tests = []struct {
		group storage.AccessGroupV1

		expectedError error
	}{
		{
			group: storage.AccessGroupV1{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					Kind:       storage.AccessGroupKind,
					Name:       "for delete access group 5",
					ETag:       111,
				},
			},
			expectedError: &ErrServerResponseStatus{
				GotStatusCode:      http.StatusGone,
				ExpectedStatusCode: http.StatusNoContent,
				Message:            "{\"error_kind\":\"ErrETagDoesNotMatch\",\"expected\":3,\"given\":111}\n",
			},
		},
	}

	for i := range tests {
		test := &tests[i]

		gotErr := ts.client.DeleteAccessGroup(&test.group)

		if !ts.Error(gotErr, "Must return error") {
			ts.FailNow("Got no error - can not continue")
		}

		ts.Equal(test.expectedError, gotErr, "Unexpected error value")
	}
}

func TestAccessGroup(t *testing.T) {
	suite.Run(t, &TestAccessGroupSuite{})
}
